-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 17 Feb 2014 pada 17.18
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `silkss_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `Id_Admin` int(4) NOT NULL AUTO_INCREMENT,
  `Nip_Admin` varchar(12) NOT NULL,
  `Nama_Admin` varchar(50) NOT NULL,
  `Id_Masuk` int(5) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Jenis_Kelamin` enum('L','P') DEFAULT 'L',
  `Level` varchar(10) NOT NULL,
  PRIMARY KEY (`Id_Admin`),
  KEY `Nama_Admin` (`Nama_Admin`),
  KEY `Id_Masuk` (`Id_Masuk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`Id_Admin`, `Nip_Admin`, `Nama_Admin`, `Id_Masuk`, `Email`, `Jenis_Kelamin`, `Level`) VALUES
(5, '65754564342', 'admin Yandri ', 239, 'yandri@gmai.com', 'L', ''),
(6, '32224242778', 'yandri', 240, 'yan@fds.sccs', 'L', ''),
(7, '62736237627', 'gfv', 241, 'yandri@gmai.com', 'L', ''),
(8, '62736237622', 'gfv', 243, 'yandri@gmai.com', 'L', ''),
(9, '62736237621', 'gfv', 244, 'yandri@gmai.com', 'L', ''),
(10, '62736237629', 'gfv', 245, 'yandri@gmai.com', 'L', ''),
(11, '28173424624', 'hgusfgfu', 246, 'yandri@gmai.com', 'P', ''),
(12, '84787887462', 'yhjg', 247, 'yandri@gmai.com', 'L', ''),
(13, '10012919238', 'w al fa', 248, 'yandri@gmai.com', 'L', ''),
(14, '10010310016', 'Jeki Oemanu', 254, 'jeki@gmail.com', 'L', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita_situs`
--

CREATE TABLE IF NOT EXISTS `berita_situs` (
  `Id_Berita` int(10) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(40) NOT NULL,
  `Isi_Berita` varchar(5000) NOT NULL,
  `Tanggal_Muat` datetime NOT NULL,
  `Judul` varchar(500) NOT NULL,
  PRIMARY KEY (`Id_Berita`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data untuk tabel `berita_situs`
--

INSERT INTO `berita_situs` (`Id_Berita`, `Nama`, `Isi_Berita`, `Tanggal_Muat`, `Judul`) VALUES
(23, 'Yandri Pono', 'Praesent vitae aliquam urna. Duis mattis at arcu nec accumsan. Suspendisse imperdiet magna libero, at viverra nunc tempus ac. Donec aliquam orci nec ligula consequat, sit amet gravida nisi consectetur. Etiam nibh justo, eleifend eget nulla quis, gravida commodo ligula. Phasellus luctus turpis consequat arcu elementum molestie. Nunc faucibus hendrerit lacus nec congue. Morbi dolor nisi, cursus vitae ligula id, auctor cursus tellus. Sed blandit nisl enim, vitae faucibus massa cursus ut. Vestibulum hendrerit nec odio ac semper. Sed interdum risus vitae pulvinar ultricies. Maecenas tempus fringilla est, eu ornare justo euismod a. Quisque ac mi ut augue varius viverra vitae vel lectus. Maecenas eu lectus ut nulla rutrum volutpat.\n\nNulla ut metus justo. Nullam blandit ut erat ut fermentum. Vivamus at nulla et erat bibendum ultricies et posuere lectus. Phasellus sollicitudin orci lacus, eget commodo dolor sagittis in. Nam interdum libero massa, eget porta purus commodo et. Nam scelerisque, nulla nec volutpat porta, tortor lectus pharetra felis, nec rhoncus elit nisi sit amet leo. Donec non mi vitae lacus iaculis consequat. Nullam felis libero, ullamcorper at urna vel, faucibus hendrerit nibh. Donec at eros et libero accumsan ultrices non at quam. Donec massa neque, fringilla sit amet faucibus eu, hendrerit id sapien. Vestibulum fermentum, diam eget sagittis feugiat, nisi massa porttitor orci, vel vehicula nisl ipsum hendrerit leo. Pellentesque quis posuere lorem. Ut laoreet, lorem et tincidunt tincidunt, tellus nibh tincidunt enim, sed molestie ipsum eros et eros. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas suscipit quam at risus venenatis cursus.', '2014-02-09 01:30:37', 'Pengumuman UAS JarKom'),
(28, 'Yandri Pono', 'makan', '2014-02-16 09:08:11', 'Pengumuman UAS JarKom'),
(29, 'jh', 'bjbjbjbjb', '2014-02-16 09:32:10', 'bjjbjbj');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku_tamu`
--

CREATE TABLE IF NOT EXISTS `buku_tamu` (
  `Id_Tamu` int(10) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Komentar` varchar(1000) NOT NULL,
  `Tanggal` datetime NOT NULL,
  PRIMARY KEY (`Id_Tamu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data untuk tabel `buku_tamu`
--

INSERT INTO `buku_tamu` (`Id_Tamu`, `Nama`, `Email`, `Komentar`, `Tanggal`) VALUES
(35, 'Yandri', 'yandripono@gmail.com', 'sayang sekali , kamu gagal					', '2014-02-08 21:49:41'),
(36, 'nn', 'nn', 'nn', '2014-02-13 14:08:00'),
(37, 'sjhsbj', 'h@jh.c', 'guy					', '2014-02-16 15:05:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data`
--

CREATE TABLE IF NOT EXISTS `data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `full_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `data`
--

INSERT INTO `data` (`id`, `from_date`, `to_date`, `full_name`, `email`, `city`) VALUES
(1, '2011-11-22', '2011-11-26', 'Aaron P. Holt', 'AaronPHolt@teleworm.com', 'London'),
(2, '2011-11-23', '2011-11-26', 'Calvin N. Harwood', 'CalvinNHarwood@teleworm.com', 'New York'),
(3, '2011-11-20', '2011-12-14', 'Richard J. Mayo', 'RichardJMayo@teleworm.com', 'London'),
(4, '2011-11-11', '2011-11-30', 'Melissa L. Haffey', 'MelissaLHaffey@teleworm.com', 'London'),
(5, '2011-12-06', '2011-12-15', 'Orlando P. Lucas', 'OrlandoPLucas@teleworm.com', 'New York'),
(6, '2012-11-07', '2011-12-16', 'Tara R. Hale', 'TaraRHale@teleworm.com', 'Paris'),
(7, '2011-12-19', '2011-12-23', 'Stuart D. Jordan', 'StuartDJordan@teleworm.com', 'Paris'),
(8, '2011-12-25', '2011-12-30', 'John R. Howell', 'JohnRHowell@teleworm.com', 'Paris'),
(9, '2011-12-13', '2011-12-30', 'Nick P. Bueche', 'NickPBueche@teleworm.com', 'New York'),
(10, '2011-12-29', '2012-01-04', 'Solomon S. Moreno', 'SolomonSMoreno@teleworm.com', 'Paris');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE IF NOT EXISTS `dosen` (
  `Id_Dosen` int(4) NOT NULL AUTO_INCREMENT,
  `Nip_Dosen` varchar(12) NOT NULL,
  `Nama_Dosen` varchar(50) NOT NULL,
  `Id_Masuk` int(5) NOT NULL,
  `Prodi` enum('Fisika','Matematika','TIK','Kimia') NOT NULL DEFAULT 'Fisika',
  `Email` varchar(50) NOT NULL,
  `Jenis_Kelamin` enum('L','P') DEFAULT 'L',
  `Level` varchar(10) NOT NULL,
  PRIMARY KEY (`Id_Dosen`),
  KEY `Nama_Dosen` (`Nama_Dosen`),
  KEY `Id_Masuk` (`Id_Masuk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`Id_Dosen`, `Nip_Dosen`, `Nama_Dosen`, `Id_Masuk`, `Prodi`, `Email`, `Jenis_Kelamin`, `Level`) VALUES
(1, '12313131312', 'yandi', 227, 'Fisika', 'yandri@gmai.com', 'L', ''),
(2, '12313131311', 'JNK', 234, 'Fisika', '', '', ''),
(3, '13456765434', 'UHGC', 235, 'Fisika', 'yan@fds.sccs', 'L', ''),
(4, '23232324343', 'kjn', 236, 'Kimia', 'yan@fds.sccs', 'L', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `event_calendar`
--

CREATE TABLE IF NOT EXISTS `event_calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `event_calendar`
--

INSERT INTO `event_calendar` (`id`, `event_date`, `title`, `description`) VALUES
(1, '2013-09-17', 'test event 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat aliquet diam non tincidunt. Vivamus vitae ipsum ac justo elementum tempus.'),
(2, '2013-09-19', 'test event 2', 'Aenean iaculis bibendum ullamcorper. In vulputate velit eu leo aliquet eu auctor magna vestibulum.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `guestbook`
--

CREATE TABLE IF NOT EXISTS `guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time` datetime NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `guestbook`
--

INSERT INTO `guestbook` (`id`, `date_time`, `name`, `email`, `comment`) VALUES
(1, '2014-02-07 00:43:09', 'yadri', 'y', 'dshs'),
(2, '2014-02-07 00:44:25', 'eka', 'eka@gmail.com', 'sayang dimana anda akan mengeahuinya juga kan ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE IF NOT EXISTS `jadwal` (
  `Id_Jadwal` int(3) NOT NULL AUTO_INCREMENT,
  `Kode_Jadwal` int(3) NOT NULL DEFAULT '0',
  `Hari` enum('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu') NOT NULL,
  `Jam_Masuk` time NOT NULL,
  `Jam_Keluar` time NOT NULL,
  `Kelas` varchar(20) NOT NULL,
  `Pengajar` varchar(40) NOT NULL,
  `Ruangan` int(3) NOT NULL,
  `Matakuliah` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Jadwal`),
  KEY `Kode_Matakuliah` (`Matakuliah`),
  KEY `Kode_Ruangan` (`Ruangan`),
  KEY `Id_Ruangan` (`Ruangan`),
  KEY `Id_Ruangan_2` (`Ruangan`,`Matakuliah`),
  KEY `Id_Ruangan_3` (`Ruangan`,`Matakuliah`),
  KEY `Id_Ruangan_4` (`Ruangan`,`Matakuliah`),
  KEY `Id_Ruangan_5` (`Ruangan`,`Matakuliah`),
  KEY `Id_Ruangan_6` (`Ruangan`,`Matakuliah`),
  KEY `Id_Ruangan_7` (`Ruangan`,`Matakuliah`),
  KEY `Id_Ruangan_8` (`Ruangan`),
  KEY `Id_Ruangan_9` (`Ruangan`,`Matakuliah`),
  KEY `Id_Ruangan_10` (`Ruangan`,`Matakuliah`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`Id_Jadwal`, `Kode_Jadwal`, `Hari`, `Jam_Masuk`, `Jam_Keluar`, `Kelas`, `Pengajar`, `Ruangan`, `Matakuliah`) VALUES
(28, 1, 'Sabtu', '00:00:00', '00:00:00', 'TIk 2', 'Wikky Almaki Fahwaz', 101, 'JAringan Komputer'),
(29, 3, 'Senin', '05:00:00', '07:00:00', 'tik3', 'yandri', 201, 'Jarkom');

-- --------------------------------------------------------

--
-- Struktur dari tabel `komputer`
--

CREATE TABLE IF NOT EXISTS `komputer` (
  `Id_Komputer` int(3) NOT NULL AUTO_INCREMENT,
  `Kode_Komputer` enum('001','002','003','004','005','006','007','008','009','010','011','012','013','014','015','016','017','018','019','020','021','022','023','024','025','026','027','028','029','030','031','032','033','034','035','036','037','038','039','040') NOT NULL,
  `Merk_Komputer` varchar(40) NOT NULL,
  `Model_Komputer` varchar(40) NOT NULL,
  `Status` enum('Pakai','Tidak','','') NOT NULL,
  PRIMARY KEY (`Id_Komputer`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data untuk tabel `komputer`
--

INSERT INTO `komputer` (`Id_Komputer`, `Kode_Komputer`, `Merk_Komputer`, `Model_Komputer`, `Status`) VALUES
(22, '001', 'Toshiba', 'Laptop', 'Tidak'),
(24, '010', 'dfvdsfvd', 'evdfv', 'Tidak'),
(25, '008', 'jsvsjg', 'uygjuvsjs', 'Pakai'),
(26, '004', 'kjszxgndiuIUD', 'CBUJFND', 'Pakai');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontak`
--

CREATE TABLE IF NOT EXISTS `kontak` (
  `Id` int(5) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(40) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Subjek` varchar(50) NOT NULL,
  `Pesan` varchar(3000) NOT NULL,
  `Tanggal` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `kontak`
--

INSERT INTO `kontak` (`Id`, `Nama`, `Email`, `Subjek`, `Pesan`, `Tanggal`) VALUES
(1, 'yandri', 'yandripono@gmail.com', 'ramalan', 'AJDNADJUAN', '2014-02-09 00:09:43'),
(2, 'yguy', 'yandripono@gmail.com', 'uygyu', 'uygugu', '2014-02-09 00:13:53'),
(3, 'jeki', 'jeki@gmail.com', 'Sengaka', 'ada da saja ibu ini ...', '2014-02-09 23:53:37'),
(4, 'jnij', 'nnu@g.com', 'un', 'uni', '2014-02-10 10:24:08'),
(7, 'uygu', 'ug@g.v', 'ugu', 'gugu', '2014-02-16 09:31:49'),
(8, 'JEKI', 'jekitak@yahoo.com', 'tugas', 'saya pesan 2 komputer untuk kerja tugas', '2014-02-16 14:42:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konter`
--

CREATE TABLE IF NOT EXISTS `konter` (
  `Ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Tanggal` date NOT NULL,
  `Hits` int(10) NOT NULL DEFAULT '1',
  `Online` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konter`
--

INSERT INTO `konter` (`Ip`, `Tanggal`, `Hits`, `Online`) VALUES
('::1', '2014-02-05', 6, '1391608268');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE IF NOT EXISTS `mahasiswa` (
  `Id_Mahasiswa` int(5) NOT NULL AUTO_INCREMENT,
  `Nim` varchar(11) NOT NULL,
  `Nama` varchar(50) NOT NULL,
  `Id_Masuk` int(5) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Angkatan` int(4) NOT NULL,
  `Jenis_Kelamin` enum('L','P') DEFAULT 'L',
  `Kabupaten` varchar(30) NOT NULL,
  `Provinsi` varchar(100) DEFAULT NULL,
  `Prodi` enum('Fisika','Matematika','TIK','Kimia') NOT NULL DEFAULT 'Fisika',
  PRIMARY KEY (`Id_Mahasiswa`),
  KEY `Id_Masuk` (`Id_Masuk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=77 ;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`Id_Mahasiswa`, `Nim`, `Nama`, `Id_Masuk`, `Email`, `Angkatan`, `Jenis_Kelamin`, `Kabupaten`, `Provinsi`, `Prodi`) VALUES
(62, '12121212121', 'Yandri', 207, 'yandri@gmailk.com', 2010, 'L', 'Kupang', 'Nanggro Aceh Darussalam', 'Fisika'),
(69, '33222222239', 'vhv', 218, 'yandri@gmai.com', 2010, 'L', '', 'Nanggro Aceh Darussalam ', 'Fisika'),
(70, '12212122222', 'guj', 219, 'yandri@gmai.com', 2010, 'L', '', 'Nanggro Aceh Darussalam ', 'Fisika'),
(71, '32222111111', 'kjk', 220, 'yandri@gmai.com', 2010, 'L', '', ' Nusa Tenggara Timur', 'Fisika'),
(72, '86535357365', 'yfyyhyh', 231, 'yandri@gmai.com', 2012, 'L', 'Kupang', 'Jawa TImur', 'Matematika'),
(74, '10001230024', 'saliban', 252, 'yandri@gmai.com', 2011, 'L', 'Belitung', 'Lampung', 'TIK'),
(75, '10010310023', 'Yandri Pono', 256, 'yandri@gmai.com', 2010, 'L', 'Belitiugn', 'Sumatera Barat ', 'TIK'),
(76, '10010310021', 'Hermanus Totos', 261, 'hermanus@gmail.com', 2013, 'L', 'Kupang', ' Nusa Tenggara Timur', 'TIK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `masuk`
--

CREATE TABLE IF NOT EXISTS `masuk` (
  `Id_Masuk` int(5) NOT NULL AUTO_INCREMENT,
  `Password` varchar(255) NOT NULL,
  `Username` varchar(11) NOT NULL,
  `Level` varchar(20) NOT NULL,
  `Online` tinyint(4) NOT NULL,
  `Waktu_Login` time NOT NULL,
  `Waktu_Logout` time NOT NULL,
  PRIMARY KEY (`Id_Masuk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=264 ;

--
-- Dumping data untuk tabel `masuk`
--

INSERT INTO `masuk` (`Id_Masuk`, `Password`, `Username`, `Level`, `Online`, `Waktu_Login`, `Waktu_Logout`) VALUES
(207, 'lkjhgfds', 'ponolaish', 'mahasiswa', 0, '00:00:00', '00:00:00'),
(208, 'admin123', 'admin', 'admin', 1, '00:00:00', '00:00:00'),
(213, 'dsdsdsds', 'vvnvnvnv', 'mahasiswa', 0, '00:00:00', '00:00:00'),
(218, 'hfsusfsf', 'hvh', 'mahasiswa', 0, '00:00:00', '00:00:00'),
(219, 'hhhhhhhh', 'hjhqwqw', 'mahasiswa', 0, '00:00:00', '00:00:00'),
(220, 'qwe1qwqw', 'jkwewew', 'mahasiswa', 0, '00:00:00', '00:00:00'),
(222, 'bhbjhbjh', 'jnkhiku', 'pengajar', 0, '00:00:00', '00:00:00'),
(223, 'ysasasas', 'ysasasas', 'pengajar', 0, '00:00:00', '00:00:00'),
(224, 'bh1212jj', 'hjuiui', 'pengajar', 0, '00:00:00', '00:00:00'),
(227, 'jbkbh888', 'nbijbkbb', 'pengajar', 0, '00:00:00', '00:00:00'),
(231, 'jvjvjbjh', 'yandri123', 'mahasiswa', 0, '00:00:00', '00:00:00'),
(232, '', '', '', 0, '21:34:37', '00:00:00'),
(233, 'dsdsk323', 'jnedkjdc', 'pengajar', 0, '00:00:00', '00:00:00'),
(234, 'CSWWWWDW', 'JKJWSFS', 'pengajar', 0, '00:00:00', '00:00:00'),
(235, 'HGVCVJJH', 'JHGCDFGH', 'pengajar', 0, '00:00:00', '00:00:00'),
(236, 'bhbjhbjh', 'jnkhiku', 'pengajar', 0, '00:00:00', '00:00:00'),
(237, '', '', '', 0, '22:17:42', '00:00:00'),
(238, '', '', '', 0, '08:43:59', '00:00:00'),
(239, 'admin123', 'adminqwe', 'admin', 0, '00:00:00', '00:00:00'),
(250, 'yfsgfusf', 'skshjdjhsbj', 'mahasiswa', 0, '00:00:00', '00:00:00'),
(252, 'saliban1', 'saliban123', 'mahasiswa', 0, '00:00:00', '00:00:00'),
(253, '', '', '', 0, '14:20:33', '00:00:00'),
(254, 'jeki1234', 'jeki123', 'admin', 0, '00:00:00', '00:00:00'),
(255, '', '', '', 0, '14:22:09', '00:00:00'),
(256, '12345678', 'yandri12', 'mahasiswa', 0, '00:00:00', '00:00:00'),
(257, '', '', '', 0, '14:28:53', '00:00:00'),
(258, '', '', '', 0, '14:33:50', '00:00:00'),
(259, '', '', '', 0, '14:53:41', '00:00:00'),
(260, '', '', '', 0, '20:03:36', '00:00:00'),
(261, 'herman12', 'herman123', 'mahasiswa', 0, '00:00:00', '00:00:00'),
(262, '', '', '', 0, '22:46:37', '00:00:00'),
(263, '', '', '', 0, '23:15:39', '00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `matakuliah`
--

CREATE TABLE IF NOT EXISTS `matakuliah` (
  `Id_Matakuliah` int(3) NOT NULL AUTO_INCREMENT,
  `Nama_Matakuliah` varchar(50) NOT NULL,
  `Kode_Matakuliah` varchar(20) NOT NULL,
  `Hari` enum('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu') NOT NULL,
  `Jam` time NOT NULL,
  PRIMARY KEY (`Id_Matakuliah`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `matakuliah`
--

INSERT INTO `matakuliah` (`Id_Matakuliah`, `Nama_Matakuliah`, `Kode_Matakuliah`, `Hari`, `Jam`) VALUES
(1, 'matematika', '123', 'Senin', '08:00:00'),
(2, 'matematika', '12345', 'Senin', '08:00:00'),
(3, 'basis data', '1232', 'Rabu', '00:00:00'),
(4, 'basis data', '1232', 'Rabu', '00:00:00'),
(5, 'basis data', '1231', 'Rabu', '00:00:08'),
(6, 'basis data', '1231', 'Rabu', '00:00:08'),
(7, 'basis data', '1234', 'Rabu', '08:00:00'),
(8, 'basis data', '1234', 'Rabu', '08:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `memesan`
--

CREATE TABLE IF NOT EXISTS `memesan` (
  `Id_Mahasiswa` int(5) NOT NULL,
  `Id_Pemesanan` int(5) NOT NULL AUTO_INCREMENT,
  `Kode_Pemesanan` varchar(4) NOT NULL,
  `Id_Komputer` int(3) NOT NULL,
  `Tanggal_Pakai` date NOT NULL,
  `Jam_Mulai_Pakai` time NOT NULL,
  `Jam_Akhir_Pakai` time NOT NULL,
  PRIMARY KEY (`Id_Pemesanan`),
  KEY `Kode_Komputer` (`Id_Komputer`),
  KEY `Id_Mahasiswa` (`Id_Mahasiswa`,`Id_Komputer`),
  KEY `Id_Mahasiswa_2` (`Id_Mahasiswa`,`Id_Komputer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `meminjam`
--

CREATE TABLE IF NOT EXISTS `meminjam` (
  `Id_Peminjaman` int(4) NOT NULL AUTO_INCREMENT,
  `Kode_Peminjaman` varchar(5) NOT NULL,
  `Tanggal_Masuk` date NOT NULL,
  `Tanggal_Keluar` date NOT NULL,
  `Jam_Masuk` time NOT NULL,
  `Jam_Keluar` time NOT NULL,
  `Keperluan` varchar(1000) NOT NULL,
  `Id_Tutor` int(4) NOT NULL,
  `Id_Dosen` int(4) NOT NULL,
  `Id_Ruangan` int(4) NOT NULL,
  PRIMARY KEY (`Id_Peminjaman`),
  KEY `Nip_Tutor` (`Id_Tutor`),
  KEY `Nip_Tutor_2` (`Id_Tutor`,`Id_Dosen`,`Id_Ruangan`),
  KEY `Nip_Tutor_3` (`Id_Tutor`,`Id_Dosen`,`Id_Ruangan`),
  KEY `Nim_Dosen` (`Id_Dosen`),
  KEY `Kode_Ruangan` (`Id_Ruangan`),
  KEY `Nip_Tutor_4` (`Id_Tutor`),
  KEY `Id_Tutor` (`Id_Tutor`),
  KEY `Id_Tutor_2` (`Id_Tutor`,`Id_Dosen`,`Id_Ruangan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mengajar`
--

CREATE TABLE IF NOT EXISTS `mengajar` (
  `Id_Mengajar` int(4) NOT NULL AUTO_INCREMENT,
  `Id_Dosen` int(4) NOT NULL,
  `Id_Tutor` int(4) NOT NULL,
  `Tanggal_Mengajar` date NOT NULL,
  `Jam_Masuk_Mengajar` time NOT NULL,
  `Jam_Keluar_Mengajar` time NOT NULL,
  `Id_Matakuliah` int(3) NOT NULL,
  PRIMARY KEY (`Id_Mengajar`),
  KEY `Nip_Dosen` (`Id_Dosen`,`Id_Tutor`,`Id_Matakuliah`),
  KEY `Nip_Dosen_2` (`Id_Dosen`,`Id_Tutor`),
  KEY `Nip_Tutor` (`Id_Tutor`),
  KEY `Nip_Tutor_2` (`Id_Tutor`),
  KEY `Id_Dosen` (`Id_Dosen`,`Id_Tutor`,`Id_Matakuliah`),
  KEY `Id_Matakuliah` (`Id_Matakuliah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mengambil`
--

CREATE TABLE IF NOT EXISTS `mengambil` (
  `Id_Mengambil` int(3) NOT NULL AUTO_INCREMENT,
  `Id_Mahasiswa` int(5) NOT NULL,
  `Id_Matakuliah` int(3) NOT NULL,
  `Tanggal_Ambil` date NOT NULL,
  PRIMARY KEY (`Id_Mengambil`),
  KEY `Nim` (`Id_Mahasiswa`,`Id_Matakuliah`),
  KEY `Kode_Matakuliah` (`Id_Matakuliah`),
  KEY `Id_Matakuliah` (`Id_Matakuliah`),
  KEY `Id_Mahasiswa` (`Id_Mahasiswa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruangan`
--

CREATE TABLE IF NOT EXISTS `ruangan` (
  `Id_Ruangan` int(2) NOT NULL AUTO_INCREMENT,
  `Kode_Ruangan` int(3) NOT NULL,
  `Nama_Ruangan` varchar(50) NOT NULL,
  `Lokasi` varchar(30) DEFAULT NULL,
  `Kapasitas` int(5) NOT NULL,
  PRIMARY KEY (`Id_Ruangan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data untuk tabel `ruangan`
--

INSERT INTO `ruangan` (`Id_Ruangan`, `Kode_Ruangan`, `Nama_Ruangan`, `Lokasi`, `Kapasitas`) VALUES
(51, 101, 'ruang lab uvgyfddd', 'lt2', 1222);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_biodata`
--

CREATE TABLE IF NOT EXISTS `tabel_biodata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `jenis_kelamin` enum('Laki-Laki','Perempuan') NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `tabel_biodata`
--

INSERT INTO `tabel_biodata` (`id`, `nama`, `jenis_kelamin`, `alamat`, `no_hp`) VALUES
(9, 'Yandri pono', 'Laki-Laki', 'Sure', '0852345'),
(8, 'h', 'Laki-Laki', 'h', '0852126');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_statistik`
--

CREATE TABLE IF NOT EXISTS `tb_statistik` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(20) NOT NULL,
  `tgl` date NOT NULL,
  `hits` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `tb_statistik`
--

INSERT INTO `tb_statistik` (`id`, `ip_address`, `tgl`, `hits`) VALUES
(1, '::1', '2014-01-22', 1),
(2, '::1', '2014-01-29', 1),
(3, '10.150.1.5', '2014-01-29', 1),
(4, '10.150.5.54', '2014-01-29', 1),
(5, '10.150.3.23', '2014-01-28', 1),
(6, '10.150.5.59', '2014-01-28', 1),
(7, '202.234.23.234', '2014-01-28', 1),
(8, '10.123.123.12', '2014-01-27', 1),
(9, '::1', '2014-02-05', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tutor`
--

CREATE TABLE IF NOT EXISTS `tutor` (
  `Id_Tutor` int(4) NOT NULL AUTO_INCREMENT,
  `Nip_Tutor` varchar(12) NOT NULL,
  `Nama_Tutor` varchar(50) NOT NULL,
  `Id_Masuk` int(4) NOT NULL,
  `Prodi` enum('Fisika','Matematika','TIK','Kimia') NOT NULL DEFAULT 'Fisika',
  `Email` varchar(50) NOT NULL,
  `Jenis_Kelamin` enum('L','P') DEFAULT 'L',
  `Level` varchar(10) NOT NULL,
  PRIMARY KEY (`Id_Tutor`),
  KEY `Id_Masuk` (`Id_Masuk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `tutor`
--

INSERT INTO `tutor` (`Id_Tutor`, `Nip_Tutor`, `Nama_Tutor`, `Id_Masuk`, `Prodi`, `Email`, `Jenis_Kelamin`, `Level`) VALUES
(4, '23232324343', 'kjn', 222, 'Kimia', 'yan@fds.sccs', 'L', ''),
(5, '10010201002', 'yasasa', 223, 'Kimia', 'yandri@gmai.com', 'L', ''),
(6, '77777777777', 'pono liash', 224, 'Matematika', 'yandri@gmai.com', 'P', ''),
(7, '12131831391', 'kj,nknkn', 233, 'TIK', 'j@d.aci', 'L', '');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `dosen`
--
ALTER TABLE `dosen`
  ADD CONSTRAINT `dosen_ibfk_1` FOREIGN KEY (`Id_Masuk`) REFERENCES `masuk` (`Id_Masuk`);

--
-- Ketidakleluasaan untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`Id_Masuk`) REFERENCES `masuk` (`Id_Masuk`);

--
-- Ketidakleluasaan untuk tabel `memesan`
--
ALTER TABLE `memesan`
  ADD CONSTRAINT `memesan_ibfk_1` FOREIGN KEY (`Id_Mahasiswa`) REFERENCES `mahasiswa` (`Id_Mahasiswa`),
  ADD CONSTRAINT `memesan_ibfk_2` FOREIGN KEY (`Id_Komputer`) REFERENCES `komputer` (`Id_Komputer`);

--
-- Ketidakleluasaan untuk tabel `meminjam`
--
ALTER TABLE `meminjam`
  ADD CONSTRAINT `meminjam_ibfk_1` FOREIGN KEY (`Id_Tutor`) REFERENCES `tutor` (`Id_Tutor`),
  ADD CONSTRAINT `meminjam_ibfk_2` FOREIGN KEY (`Id_Dosen`) REFERENCES `dosen` (`Id_Dosen`),
  ADD CONSTRAINT `meminjam_ibfk_3` FOREIGN KEY (`Id_Ruangan`) REFERENCES `ruangan` (`Id_Ruangan`);

--
-- Ketidakleluasaan untuk tabel `mengajar`
--
ALTER TABLE `mengajar`
  ADD CONSTRAINT `mengajar_ibfk_1` FOREIGN KEY (`Id_Dosen`) REFERENCES `dosen` (`Id_Dosen`),
  ADD CONSTRAINT `mengajar_ibfk_2` FOREIGN KEY (`Id_Tutor`) REFERENCES `tutor` (`Id_Tutor`),
  ADD CONSTRAINT `mengajar_ibfk_3` FOREIGN KEY (`Id_Matakuliah`) REFERENCES `matakuliah` (`Id_Matakuliah`);

--
-- Ketidakleluasaan untuk tabel `mengambil`
--
ALTER TABLE `mengambil`
  ADD CONSTRAINT `mengambil_ibfk_1` FOREIGN KEY (`Id_Matakuliah`) REFERENCES `matakuliah` (`Id_Matakuliah`),
  ADD CONSTRAINT `mengambil_ibfk_2` FOREIGN KEY (`Id_Mahasiswa`) REFERENCES `mahasiswa` (`Id_Mahasiswa`);

--
-- Ketidakleluasaan untuk tabel `tutor`
--
ALTER TABLE `tutor`
  ADD CONSTRAINT `tutor_ibfk_1` FOREIGN KEY (`Id_Masuk`) REFERENCES `masuk` (`Id_Masuk`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
