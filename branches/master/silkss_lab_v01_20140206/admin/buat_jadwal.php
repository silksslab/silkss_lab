
<?php
include "koneksi.php";
@session_start();
$username= $_SESSION['username'];
if ($username){
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="base.css" /><link rel="stylesheet" type="text/css" href="content.css" /><link rel="stylesheet" type="text/css" href="nav.css" /><title>Buat Jadwal | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>
<div id="siteNav">
<ul>
<li><a href="index.php" class="daddy main-node">BERANDA</a></li><li class="current-page-parent"><a href="penggunaan_lab.php" class="current-page-parent daddy">Penggunaan Lab</a><ul>
<li class="current-page-parent"><a href="jadwal.php" class="current-page-parent daddy">Jadwal</a><ul>
<li id="active"><a href="buat_jadwal.php" class="active no-ch">Buat Jadwal</a></li>
<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
<li><a href="ubah_jadwal.php" class="no-ch">Ubah Jadwal</a></li>
<li><a href="hapus_jadwal.php" class="no-ch">Hapus Jadwal</a></li>
</ul>
</li>
<li><a href="ruangan.php" class="daddy">Ruangan</a><ul class="other-section">
<li><a href="tambah_ruangan.php" class="no-ch">Tambah Ruangan</a></li>
<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
<li><a href="ubah_ruangan.php" class="no-ch">Ubah Ruangan</a></li>
<li><a href="hapus_ruangan.php" class="no-ch">Hapus Ruangan</a></li>
</ul>
</li>
<li><a href="komputer.php" class="daddy">Komputer</a><ul class="other-section">
<li><a href="tambah_komputer.php" class="no-ch">Tambah Komputer</a></li>
<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
<li><a href="ubah_komputer.php" class="no-ch">Ubah Komputer</a></li>
<li><a href="hapus_komputer.php" class="no-ch">Hapus Komputer</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="kinerja.php" class="daddy">Kinerja</a><ul class="other-section">
<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
</ul>
</li>
<li><a href="kebutuhan.php" class="daddy">Kebutuhan</a><ul class="other-section">
<li><a href="mahasiswa.php" class="daddy">Mahasiswa</a><ul class="other-section">
<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
<li><a href="lihat_mahasiswa.php" class="no-ch">Lihat Mahasiswa</a></li>
<li><a href="ubah_mahasiswa.php" class="no-ch">Ubah Mahasiswa</a></li>
<li><a href="hapus_mahasiswa.php" class="no-ch">Hapus Mahasiswa</a></li>
<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pemesanan</a></li>
<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan</a></li>
<li><a href="mengambil_matakuliah.php" class="no-ch">Mengambil Matakuliah</a></li>
</ul>
</li>
<li><a href="dosen.php" class="daddy">Dosen</a><ul class="other-section">
<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
<li><a href="lihat_dosen.php" class="no-ch">Lihat Dosen</a></li>
<li><a href="ubah_dosen.php" class="no-ch">Ubah Dosen</a></li>
<li><a href="hapus_dosen.php" class="no-ch">Hapus Dosen</a></li>
<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Peminjaman Ruangan</a></li>
<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li><a href="tutor.php" class="daddy">Tutor</a><ul class="other-section">
<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
<li><a href="lihat_tutor.php" class="no-ch">Lihat Tutor</a></li>
<li><a href="ubah_tutor.php" class="no-ch">Ubah Tutor</a></li>
<li><a href="hapus_tutor.php" class="no-ch">Hapus Tutor</a></li>
<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Peminjaman</a></li>
<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li><a href="matakuliah.php" class="daddy">Matakuliah</a><ul class="other-section">
<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
<li><a href="lihat_matakuliah.php" class="no-ch">Lihat Matakuliah</a></li>
<li><a href="ubahmatakuliah.php" class="no-ch">UbahMatakuliah</a></li>
<li><a href="hapus_matakuliah.php" class="no-ch">Hapus Matakuliah</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="info_lab.php" class="daddy">Info lab</a><ul class="other-section">
<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
<li><a href="lihat_berita.php" class="no-ch">Lihat Berita</a></li>
</ul>
</li>
<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a><ul class="other-section">
<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
</ul>
</div>

<div id='topPagination'>
		<div class="pagination noprt">
			<?php
			echo"[ Selamat Datang &nbsp;(..".$username."..) ] <a href='../login/logout.php'><u>Keluar</u></a>";
			?>
		<span> </span></a>
		</div>
	</div>
	
<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Buat Jadwal</h1>

<!-- Start jadwal -->
<link rel="stylesheet" href="tambah_jadwal_files/formoid1/formoid-default-red.css" type="text/css" />
<script type="text/javascript" src="tambah_jadwal_files/formoid1/jquery.min.js"></script>
<form  action="simpan_jadwal.php" class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:480px;min-width:150px" method="post">
<div class="title"><h2>Menambahkan Jadwal</h2></div>

	<div class="element-select" ><label class="title">Kode Jadwal<span class="required">*</span></label>
	<div class="small"><span><select name="kdjadwal" required="required">

		<option value="01">01</option><br/>
		<option value="02">02</option><br/>
		<option value="03">03</option><br/>
		<option value="04">04</option><br/>
		<option value="05">05</option><br/>
		<option value="06">06</option><br/>
		<option value="07">07</option><br/>
		<option value="08">08</option><br/>
		<option value="09">09</option><br/>
		<option value="10">10</option><br/>
		<option value="11">11</option><br/>
		<option value="12">12</option><br/>
		<option value="13">13</option><br/>
		<option value="14">14</option><br/>
		<option value="15">15</option><br/>
		<option value="16">16</option><br/>
		<option value="17">17</option><br/>
		<option value="18">18</option><br/>
		<option value="19">19</option><br/>
		<option value="20">20</option><br/>
		<option value="21">21</option><br/>
		<option value="22">22</option><br/>
		<option value="23">23</option><br/>
		<option value="24">24</option><br/>
		<option value="25">25</option><br/>
		<option value="26">26</option><br/>
		<option value="27">27</option><br/>
		<option value="28">28</option><br/>
		<option value="29">29</option><br/>
		<option value="30">30</option><br/>
		<option value="31">31</option><br/>
		<option value="32">32</option><br/>
		<option value="33">33</option><br/>
		<option value="34">34</option><br/>
		<option value="35">35</option><br/>
		<option value="36">36</option><br/>
		<option value="37">37</option><br/>
		<option value="38">38</option><br/>
		<option value="39">39</option><br/>
		<option value="40">40</option><br/></select><i></i></span></div></div>
		
	<div>
	
	<!-- 
	<table width="80%">
	<tr>
	<td>
		<label class="description" for="element_1">Jam Masuk </label> 
	</td>	
	<td>
			<td> <label><i style="color:red;font-size:12px;" >jam</i></label> </td> 
			<td> <input id="element_1_1" name="jam_masuk" type="number" class="element text " size="2" type="text" maxlength="2" value="00"/>  </td>
			
	</td>
	<td>	
			<td><label><i style="color:red;font-size:12px;" > Mnt </i></label></td>
			 <td> <input id="element_1_2" name="menit_masuk" type="number" class="element text " size="2" type="text" maxlength="2" value="00"/>  </td>
			
	</td>	
	<td>	
			<td> <label> <i style="color:red;font-size:12px;" > Detik </i></label> </td>
			 <td> <input id="element_1_3" name="det_masuk" type="number" class="element text " size="2" type="text" maxlength="2" value="00"/> </td>
			<td> <i style="color : red;font-size:12px;"> jam/mnt.detik berupa angka</i></td>
			
	</td>
	</tr>
	</table>
	</div>	
	
	<div>
	<table width="100%">
	<tr>
	<td>
		<label class="description" for="element_1">Jam Keluar</label> 
	</td>	
	<td>
			<td> <label><i style="color:red;font-size:12px;" >jam</i></label> </td> 
			<td> <input id="element_1_1" name="jam_keluar" class="element text " size="2" type="text" maxlength="2" value="00"/>  </td>
			
	</td>
	<td>	
			<td><label><i style="color:red;font-size:12px;" > Mnt </i></label></td>
			 <td> <input id="element_1_2" name="menit_keluar" class="element text " size="2" type="text" maxlength="2" value="00"/>  </td>
			
	</td>	
	<td>	
			<td> <label> <i style="color:red;font-size:12px;" > Detik </i></label> </td>
			 <td> <input id="element_1_3" name="det_keluar" class="element text " size="2" type="text" maxlength="2" value="00"/> </td>
			<td> <i style="color : red;font-size:12px;"> jam/mnt.detik berupa angka</i></td>
			
	</td>
	</tr>
	</table>
	</div>
	
-->

	
	<div class="element-select" ><label class="title">Hari<span class="required">*</span></label><div class="small"><span>
	<select name="harijdwl" required="required">
		<option value="Senin">Senin</option><br/>
		<option value="Selasa">Selasa</option><br/>
		<option value="Rabu">Rabu</option><br/>
		<option value="Kamis">Kamis</option><br/>
		<option value="Jumat">Jumat</option><br/>
		<option value="Sabtu">Sabtu</option><br/>
		<option value="Minggu">Minggu</option><br/>
	</select><i></i></span></div></div>
	
<!--
	<div class="element-input" ><label class="title">Jam Mulai Kuliah<span class="required">*</span></label>
		<table >
		<tr>
		<td>
		<div class="element-select" ><label class="title"><i style="color:red;">Jam</i> <span class="required"></span></label>
		</td>
		<td>
		<div class="element-select" >
		<div style="position:relative;width:60px;"><span>
		<select name="jam_masuk" required="required">

		<option value="01">01</option><br/>
		<option value="02">02</option><br/>
		<option value="03">03</option><br/>
		<option value="04">04</option><br/>
		<option value="05">05</option><br/>
		<option value="06">06</option><br/>
		<option value="07">07</option><br/>
		<option value="08">08</option><br/>
		<option value="09">09</option><br/>
		<option value="10">10</option><br/>
		<option value="11">11</option><br/>
		<option value="12">12</option><br/>
		<option value="13">13</option><br/>
		<option value="14">14</option><br/>
		<option value="15">15</option><br/>
		<option value="16">16</option><br/>
		<option value="17">17</option><br/>
		<option value="18">18</option><br/>
		<option value="19">19</option><br/>
		<option value="20">20</option><br/>
		<option value="21">21</option><br/>
		<option value="22">22</option><br/>
		<option value="23">23</option><br/></select>
		<i></i></span></div>
		</div>
		</td>		
		</div>
		</tr>
		
		<tr>
		<td>
		<div class="element-select" ><label class="title"><i style="color:red;">Menit</i><span class="required"></span></label>
		</td>
		<td>
		<div class="element-select" >
		<div style="position:relative;width:60px;"><span><select name="mnt_masuk" required="required">

		<option value="00">00</option><br/>
		<option value="05">05</option><br/>
		<option value="10">10</option><br/>
		<option value="15">15</option><br/>
		<option value="20">20</option><br/>
		<option value="25">25</option><br/>
		<option value="30">30</option><br/>
		<option value="35">35</option><br/>
		<option value="40">40</option><br/>
		<option value="45">45</option><br/>
		<option value="50">50</option><br/>
		<option value="55">55</option><br/></select><i></i></span></div>
		</div>
		</div>
		</td>
		</tr>
		</table>
		
	</div>
	
	<div class="element-input" ><label class="title">Jam Selesai Kuliah<span class="required">*</span></label>
		<table >
		<tr>
		<td>
		<div class="element-select" ><label class="title"><i style="color:red;">Jam</i> <span class="required"></span></label>
		</td>
		<td>
		<div class="element-select" >
		<div style="position:relative;width:60px;"><span><select name="jam_keluar" required="required">

		<option value="01">01</option><br/>
		<option value="02">02</option><br/>
		<option value="03">03</option><br/>
		<option value="04">04</option><br/>
		<option value="05">05</option><br/>
		<option value="06">06</option><br/>
		<option value="07">07</option><br/>
		<option value="08">08</option><br/>
		<option value="09">09</option><br/>
		<option value="10">10</option><br/>
		<option value="11">11</option><br/>
		<option value="12">12</option><br/>
		<option value="13">13</option><br/>
		<option value="14">14</option><br/>
		<option value="15">15</option><br/>
		<option value="16">16</option><br/>
		<option value="17">17</option><br/>
		<option value="18">18</option><br/>
		<option value="19">19</option><br/>
		<option value="20">20</option><br/>
		<option value="21">21</option><br/>
		<option value="22">22</option><br/>
		<option value="23">23</option><br/></select><i></i></span></div>
		</div>
		</td>		
		</div>
		</tr>
		
		<tr>
		<td>
		<div class="element-select" ><label class="title"><i style="color:red;">Menit</i><span class="required"></span></label>
		</td>
		<td>
		<div class="element-select" >
		<div style="position:relative;width:60px;"><span><select name="mnt_keluar" required="required">

		<option value="00">00</option><br/>
		<option value="05">05</option><br/>
		<option value="10">10</option><br/>
		<option value="15">15</option><br/>
		<option value="20">20</option><br/>
		<option value="25">25</option><br/>
		<option value="30">30</option><br/>
		<option value="35">35</option><br/>
		<option value="40">40</option><br/>
		<option value="45">45</option><br/>
		<option value="50">50</option><br/>
		<option value="55">55</option><br/></select><i></i></span></div>
		</div>
		</div>
		</td>
		</tr>
		</table>
		
	</div>
-->
	<script>
	function angka(e) {
	if (!/^[0-9]+$/.test(e.value)) {
      e.value = e.value.substring(0,e.value.length-1);
	}
	}

	</script>
	
	<!--
	Jam Masuk : <input type="time" name="jam_masuk" value="00:00" maxlength="4"  id="harga" onkeyup="angka(this)"> <i style="color : red;font-size:12px;"> jam/mnt berupa angka</i></br> 
	Jam Keluar : <input type="time" name="jam_keluar" value="00:00" maxlength="4"   value="00:00" id="harga" onkeyup="angka(this)" > <i style="color : red;font-size:12px;"> jam/mnt berupa angka</i></br>
	-->
	<table >
	<tr>
	<td>
	Jam Masuk :
	</td>
	<td>
	<select type="time" name="jam_masuk" value="00:00" maxlength="4"  id="harga" onkeyup="angka(this)" required="required">

		<option  value="01">01</option><br/>
		<option value="02">02</option><br/>
		<option value="03">03</option><br/>
		<option value="04">04</option><br/>
		<option value="05">05</option><br/>
		<option value="06">06</option><br/>
		<option value="07">07</option><br/>
		<option value="08">08</option><br/>
		<option value="09">09</option><br/>
		<option value="10">10</option><br/>
		<option value="11">11</option><br/>
		<option value="12">12</option><br/>
		<option value="13">13</option><br/>
		<option value="14">14</option><br/>
		<option value="15">15</option><br/>
		<option value="16">16</option><br/>
		<option value="17">17</option><br/>
		<option value="18">18</option><br/>
		<option value="19">19</option><br/>
		<option value="20">20</option><br/>
		<option value="21">21</option><br/>
		<option value="22">22</option><br/>
		<option value="23">23</option><br/></select>
		</td> 
		
		<td>
		<select type="time" name="jam_masuk" value="00:00" maxlength="4"  id="harga" onkeyup="angka(this)" required="required">

		<option value="00">00</option><br/>
		<option value="05">05</option><br/>
		<option value="10">10</option><br/>
		<option value="15">15</option><br/>
		<option value="20">20</option><br/>
		<option value="25">25</option><br/>
		<option value="30">30</option><br/>
		<option value="35">35</option><br/>
		<option value="40">40</option><br/>
		<option value="45">45</option><br/>
		<option value="50">50</option><br/>
		<option value="55">55</option><br/>
		</select>
		</td>
		
		</tr>
		
		<tr>
	<td>
	Jam Keluar: 
	</td>
	<td>
	<select type="time" name="jam_keluar" value="00:00" maxlength="4"  id="harga" onkeyup="angka(this)" required="required">

		<option  value="01">01</option><br/>
		<option value="02">02</option><br/>
		<option value="03">03</option><br/>
		<option value="04">04</option><br/>
		<option value="05">05</option><br/>
		<option value="06">06</option><br/>
		<option value="07">07</option><br/>
		<option value="08">08</option><br/>
		<option value="09">09</option><br/>
		<option value="10">10</option><br/>
		<option value="11">11</option><br/>
		<option value="12">12</option><br/>
		<option value="13">13</option><br/>
		<option value="14">14</option><br/>
		<option value="15">15</option><br/>
		<option value="16">16</option><br/>
		<option value="17">17</option><br/>
		<option value="18">18</option><br/>
		<option value="19">19</option><br/>
		<option value="20">20</option><br/>
		<option value="21">21</option><br/>
		<option value="22">22</option><br/>
		<option value="23">23</option><br/></select>
		</td> 
		
		<td>
		<select type="time" name="jam_keluar" value="00:00" maxlength="4"  id="harga" onkeyup="angka(this)" required="required">

		<option value="00">00</option><br/>
		<option value="05">05</option><br/>
		<option value="10">10</option><br/>
		<option value="15">15</option><br/>
		<option value="20">20</option><br/>
		<option value="25">25</option><br/>
		<option value="30">30</option><br/>
		<option value="35">35</option><br/>
		<option value="40">40</option><br/>
		<option value="45">45</option><br/>
		<option value="50">50</option><br/>
		<option value="55">55</option><br/></select>
		</td>
		</tr>
	</table>
	
	<div class="element-input" ><label class="title">Matakuliah<span class="required">*</span></label>
		<input class="medium" type="text" name="mkjdwl" required="required"/>
	</div>
	<div class="element-input" ><label class="title">Kelas<span class="required">*</span></label>
		<input class="small" type="text" name="klsjdwl" required="required"/>
	</div>
	<div class="element-input" ><label class="title">Pengajar<span class="required">*</span></label>
		<input class="medium" type="text" name="pengjdwl" required="required"/>
	</div>
		<div class="element-select" ><label class="title">Ruangan<span class="required">*</span></label>
		<div class="small"><span><select name="ruang" required="required">

		<option value="101">101</option><br/>
		<option value="102">102</option><br/>
		<option value="103">103</option><br/>
		<option value="104">104</option><br/>
		<option value="105">105</option><br/>
		<option value="106">106</option><br/>
		<option value="107">107</option><br/>
		<option value="108">108</option><br/>
		<option value="109">109</option><br/>
		<option value="201">201</option><br/>
		<option value="202">202</option><br/>
		<option value="203">203</option><br/>
		<option value="204">204</option><br/>
		<option value="205">205</option><br/>
		<option value="206">206</option><br/>
		<option value="207">207</option><br/>
		<option value="208">208</option><br/>
		<option value="209">209</option><br/>
		<option value="301">301</option><br/>
		<option value="302">302</option><br/>
		<option value="303">303</option><br/>
		<option value="304">304</option><br/>
		<option value="305">305</option><br/>
		<option value="306">306</option><br/>
		<option value="307">307</option><br/>
		<option value="308">308</option><br/>
		<option value="309">309</option><br/>
		<option value="401">401</option><br/>
		<option value="402">402</option><br/>
		<option value="403">403</option><br/>
		<option value="404">404</option><br/>
		<option value="405">405</option><br/>
		<option value="406">406</option><br/>
		<option value="407">407</option><br/>
		<option value="408">408</option><br/>
		<option value="409">409</option><br/></select><i></i></span></div></div>
		
		

<div class="submit"><input type="submit" value="Simpan"/></div></form>
<script type="text/javascript" src="tambah_jadwal_files/formoid1/formoid-default-red.js"></script>


<!-- Stop jadwal-->

</div>
</div>
</div>
<div id="bottomPagination" >Copy Right Labkom STKIP Surya 2013/2014
</div>
</body></html>
<?php
}
	else {
		header("location:../index.php");
	}

?>