<?php
include "koneksi.php";
@session_start();
$username= $_SESSION['username'];
if ($username){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="../base.css" />
<link rel="stylesheet" type="text/css" href="../content.css" />
<link rel="stylesheet" type="text/css" href="../nav.css" />
<title>SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>


<!--Calender -->
		<link href="../calender/style/style.css" rel="stylesheet" type="text/css" />
<!-- Calender-->


<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="common.js"></script>
</head>
<body>
<div id="content">
		<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
				SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA 
		</div>
	<div id="siteNav">
	<ul>
	<li id="active"><a href="index.php" class="active daddy main-node">BERANDA</a></li>
	<li><a href="penggunaan_lab.php" class="daddy">Penggunaan Lab</a>

	<ul class="other-section">
				<li><a >Jadwal</a>
							<ul class="other-section">
							<li><a href="buat_jadwal.php" class="no-ch">Buat Jadwal</a></li>
							<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
							
							</ul>
				</li>
				
				<li><a  class="daddy">Ruangan</a>
							<ul class="other-section">
							<li><a href="tambah_ruangan.php" class="no-ch">Tambah Ruangan</a></li>
							<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
							
							</ul>
				</li>
				<li><a >Komputer</a>
							<ul class="other-section">
							<li><a href="tambah_komputer.php" class="no-ch">Tambah Komputer</a></li>
							<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
							
							</ul>
				</li>
				
				
	</ul>
	</li>

	<li><a href="kinerja.php" class="daddy">Kinerja</a>
				<ul class="other-section">
				<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
				<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
				<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
				<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
				</ul>
				</li>
				
	<li><a href="kebutuhan.php" class="daddy">Kebutuhan</a>
	<ul class="other-section">
					<li><a  class="daddy">Mahasiswa</a>
					<ul class="other-section">
							<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
							<li><a href="lihat_mahasiswa_2.php" class="no-ch">Lihat Mahasiswa</a></li>
							<li><a href="formulir_pemakaian.php" class="no-ch">Formulir Pakai Komputer</a></li>
							<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pesan Komputer</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan Kom.
							</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemakaian Kom.</a></li>
							
					</ul>
					</li>
					
					<li><a class="daddy">Dosen</a>
					<ul class="other-section">
							<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
							<li><a href="lihat_dosen.php" class="no-ch">Lihat Dosen</a></li>
							
							<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Pinjam Ruang</a></li>
							<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a  class="daddy">Tutor</a>
					<ul class="other-section">
							<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
							<li><a href="lihat_tutor.php" class="no-ch">Lihat Tutor</a></li>
							
							<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Pinjam Ruang </a></li>
							<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a  class="daddy">Matakuliah</a>
					<ul class="other-section">
							<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
							<li><a href="lihat_matakuliah.php" class="no-ch">Lihat Matakuliah</a></li>
							
					</ul>
					</li>
	</ul>
	</li>

	<li><a href="info_lab.php" class="daddy">Info lab</a>
			<ul class="other-section">
					<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
					<li><a href="lihat_berita.php" class="no-ch">Lihat Berita</a></li>
			</ul>
	</li>

	<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a>
			<ul class="other-section">
					<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
					<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
			</ul>
	</li>

	</div>
	<div id='topPagination'>
		<div class="pagination noprt">
			<?php
			echo"Selamat Datang &nbsp; ' <i>".$username." '</i> <a href='../login/logout.php'><u>Keluar</u></a>";
			?>
		<span> </span></a>
		</div>
	</div>
	<div id="main">
	<div id="nodeDecoration"><h1 id="nodeTitle"> BERANDA</h1>
	
		<div>

			<div>
			
				<div id="content4">	
				<iframe src="../slide_show/index.html" style="float:left;width:872px;height:360px;max-width:100%;overflow:hidden;border:none;padding:0;margin:0 auto;display:block;" marginheight="0" marginwidth="108px"></iframe>
				</div>
									
				<!-- End SLIDE SHOW -->
				<div style="Position:relative;border:2px;width:740px;border-radius:5px;text-align:center;text-shadow:white 1px 1px ;border:1px solid #F3E6E6;background-color:#F3E6E6;font-size:20px;color:black;float:left;margin-top:-10px;">
				<p> Selamat Datang Di Sistem Informasi Laboratorium Komputer STKIP SURYA </p>
				</div>

				<div id="content1">
				<h2 id="judul"><div class="judul"> Kalender </div></h2>
				<?php
				
				include "../calender/index.php";
				?>
				<br>
				<h2 id="judul"><div class="judul">Pengguna Online </div></h2>
				<?php 
				
				// menampilkan member yang sedang login
				$get_on =mysql_query("SELECT mhs.Nama,m.Username FROM mahasiswa AS mhs
										LEFT JOIN masuk AS m ON mhs.Id_Masuk=m.Id_Masuk
										WHERE m.online='1'");
				while ($row=mysql_fetch_assoc($get_on)){
					$username= $row['Username'];
					$name= $row['Nama'];
					//echo"$username";
					echo" 
					<table border='0'>
					<tr>
					<td>
						<div style='background-color:#00FF00;border-radius:10px;
						border:1px;width:10px;height:10px;float:left;word-spacing:20px;' >
						</div>
					</td>
					
					<td>
						<div style='float:left;word-spacing:0px;'>$name</div>
					</td>
					</tr>					
					</table>";
					//echo "</br>";
				}
				?>
				<?php ?>
				
				<h2 id="judul"><div class="judul">Buku Tamu </div></h2>
					<p id="bktamu"><a href="tulis_komentar.php">Klik untuk tulis komentar </a> </p>
					<p id="bktamu"><a href="lihat_bukutamu.php">Lihat semua komentar </a>  </p>
					
				</div>
			
			</div>
		</div>
		
		
		<!-- START lihat berita info lab-->
									
			<div id="content2">
			<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#F3E6E6; border:1px solid #F3E6E6;border-radius:5px ;-moz-border-radius:5px ;" >	
			<tr>
					<td style="background:#F3E6E6;">
						<div style="backround-color:lightgray; text-align:center;font-weight:bold;font-size:30px:">BERITA SITUS
						</div>
					</td>
				</tr>
			</table>
			<?php
			include("info_lab_koneksi.php");
			$qry=mysql_query("select * from berita_situs order by Id_Berita Desc limit 5"); 
			$no=1;
			while ($hasil=mysql_fetch_array($qry))
			{
			$no++;
			if ($no%2==0)
			{
			?>
			<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:white; border:1px solid #F3E6E6;border-radius:5px 0px 0px 5px;-moz-border-radius:5px 0px 0px 5px;" >
				
				 <tr>
					<td align="left" height="50px">
							<div style="margin:-10px 0px;background:LightGray ;border-radius:2px 0px 0px 5px;-moz-border-radius:0px 2px 5px 0px; width:100%; color:black; font-weight:bold;font-size:15px; text-transform:capitalize;" >
								<img src="gbr/prof4.gif" width="30" height="30" style="border:none" /> <? echo " $hasil[Judul] "; ?>
								
							</div>
					</td>
				</tr>
				
				<tr>
					<td align="left" height="0px">
							<div style="margin:-12px 0px;background:LightGray ;border-radius:2px 0px 0px 5px;-moz-border-radius:0px 2px 5px 0px; width:100%; color:black; font-size:15px; text-transform:capitalize;" >
								 <? echo " <font color='#CD5C5C' face=arial, Helvetica, sans-serif size=2> oleh - $hasil[Nama] </font>"; ?>
								<? 
									echo "<font color='#556B2F' face=arial, Helvetica, sans-serif size=1> - Tanggal muat berita : $hasil[Tanggal_Muat]</font>";
								?>
							</div>
					</td>
				</tr>
						
				<tr>
					<td>
						<? 
							echo"<font face=arial, Helvetica, sans-serif size=2></br> $hasil[Isi_Berita]</font>";
						?>
					</td>
				</tr>
				<tr>
					<td>
					<br/>
						<? 
							// echo "<font color='#999' face=arial, Helvetica, sans-serif size=1>Tanggal Muat Berita : </font>";
							// $tanggal=$hasil['Tanggal_Muat'];
							// $tgl=substr($tanggal,8,2);
							// $bln=substr($tanggal,5,2);
							// $thn=substr($tanggal,0,4);				
							
							// echo "  <font color='#999' face=arial, Helvetica, sans-serif size=1>
										// $tgl-$bln-$thn 
									// </font>";
						?>			
					</td>
				</tr>
			</table>
				
				
			<?php }
			else
			{
			?>
			<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:white; border:1px solid #F3E6E6;border-radius:5px 0px 0px 5px;-moz-border-radius:5px 0px 0px 5px;" >
				<tr align="<?php echo $align; ?>">
					<td align="left" height="50px">
							<div style="margin:-10px 0px;background:LightGray ;border-radius:2px 0px 0px 5px;-moz-border-radius:0px 2px 5px 0px; width:100%; color:black; font-weight:bold;font-size:15px; text-transform:capitalize;" >
								<img src="gbr/prof4.gif" width="30" height="30" style="border:none" /> <? echo " $hasil[Judul] "; ?>
								
							</div>
					</td>
				</tr>
				
				<tr>
					<td align="left" height="0px">
							<div style="margin:-12px 0px;background:LightGray ;border-radius:2px 0px 0px 5px;-moz-border-radius:0px 2px 5px 0px; width:100%; color:black; font-size:15px; text-transform:capitalize;" >
								 <? echo " <font color='#CD5C5C' face=arial, Helvetica, sans-serif size=2> oleh - $hasil[Nama] </font>"; ?>
								<? 
									echo "<font color='#556B2F' face=arial, Helvetica, sans-serif size=1> - Tanggal muat berita : $hasil[Tanggal_Muat]</font>";
								?>
							</div>
					</td>
				</tr>
						
				<tr>
					<td >
							<? 
								echo"<font face=arial, Helvetica, sans-serif size=2></br> $hasil[Isi_Berita]</font>";
							?>		
					</td>
				</tr>
				<tr>
					<td>
					
							<? 
							// echo "<font color='#999' face=arial, Helvetica, sans-serif size=1>Tanggal Muat Berita : </font>";
								// $tanggal=$hasil['Tanggal_Muat'];
								// $tgl=substr($tanggal,8,2);
								// $bln=substr($tanggal,5,2);
								// $thn=substr($tanggal,0,4);				
							// echo "<font color='#999' face=arial, Helvetica, sans-serif size=1> $tgl-$bln-$thn <br/> </font>";
							?>
							
					</td>
				</tr>
				</table>
				
				<?php }
			}
			?>
			</div>
		
				<!-- End lihat berita info lab -->
	</div>
	
	
	</div>
</div>
<div id="bottomPagination">Copy Right Labkom STKIP Surya 2013/2014
</div>
</body></html>

<?php
}
	else {
		header("location:../index.php");
	}
?>
