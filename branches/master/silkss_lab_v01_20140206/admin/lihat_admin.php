<?php 
// $host="localhost";
// $userdb="root";
// $passdb="";
// $dbname="silkss_db";
include 'koneksi.php';
?>
<?php



?>
<?php
include "koneksi.php";
@session_start();
$username= $_SESSION['username'];
if ($username){
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="../base.css" />
<link rel="stylesheet" type="text/css" href="../content.css" />
<link rel="stylesheet" type="text/css" href="../nav.css" />
<title>Lihat Mahasiswa | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="../common.js"></script>

    <script type="text/javascript">
	function konfirmasiHapus(Id_Mahasiswa,Nama)
	{
		var Id_Mahasiswa = Id_Mahasiswa;
		var Nama = Nama;
		var jawab;
		
		jawab = confirm("Apakah data '"+Nama+"' akan dihapus ?")
		if(jawab)
		{
			window.location = "hapus_mahasiswa.php?Id=="+Id_Mahasiswa;
			return false;
		}else{
			alert("Penghapusan data dibatalkan");
		}
	}
	</script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>
<div id="siteNav">
<ul>
<li><a href="index.php" class="daddy main-node">BERANDA</a></li><li><a href="penggunaan_lab.php" class="daddy">Penggunaan Lab</a><ul class="other-section">
<li><a href="jadwal.php" class="daddy">Jadwal</a><ul class="other-section">
<li><a href="buat_jadwal.php" class="no-ch">Buat Jadwal</a></li>
<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
<li><a href="ubah_jadwal.php" class="no-ch">Ubah Jadwal</a></li>
<li><a href="hapus_jadwal.php" class="no-ch">Hapus Jadwal</a></li>
</ul>
</li>
<li><a href="ruangan.php" class="daddy">Ruangan</a><ul class="other-section">
<li><a href="tambah_ruangan.php" class="no-ch">Tambah Ruangan</a></li>
<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
<li><a href="ubah_ruangan.php" class="no-ch">Ubah Ruangan</a></li>
<li><a href="hapus_ruangan.php" class="no-ch">Hapus Ruangan</a></li>
</ul>
</li>
<li><a href="komputer.php" class="daddy">Komputer</a><ul class="other-section">
<li><a href="tambah_komputer.php" class="no-ch">Tambah Komputer</a></li>
<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
<li><a href="ubah_komputer.php" class="no-ch">Ubah Komputer</a></li>
<li><a href="hapus_komputer.php" class="no-ch">Hapus Komputer</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="kinerja.php" class="daddy">Kinerja</a><ul class="other-section">
<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
</ul>
</li>
<li class="current-page-parent"><a href="kebutuhan.php" class="current-page-parent daddy">Kebutuhan</a><ul>
<li class="current-page-parent"><a href="mahasiswa.php" class="current-page-parent daddy">Mahasiswa</a><ul>
<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
<li id="active"><a href="lihat_mahasiswa.php" class="active no-ch">Lihat Mahasiswa</a></li>
<li><a href="ubah_mahasiswa.php" class="no-ch">Ubah Mahasiswa</a></li>
<li><a href="hapus_mahasiswa.php" class="no-ch">Hapus Mahasiswa</a></li>
<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pemesanan</a></li>
<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan</a></li>
<li><a href="mengambil_matakuliah.php" class="no-ch">Mengambil Matakuliah</a></li>
</ul>
</li>
<li><a href="dosen.php" class="daddy">Dosen</a><ul class="other-section">
<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
<li><a href="lihat_dosen.php" class="no-ch">Lihat Dosen</a></li>
<li><a href="ubah_dosen.php" class="no-ch">Ubah Dosen</a></li>
<li><a href="hapus_dosen.php" class="no-ch">Hapus Dosen</a></li>
<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Peminjaman Ruangan</a></li>
<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li><a href="tutor.php" class="daddy">Tutor</a><ul class="other-section">
<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
<li><a href="lihat_tutor.php" class="no-ch">Lihat Tutor</a></li>
<li><a href="ubah_tutor.php" class="no-ch">Ubah Tutor</a></li>
<li><a href="hapus_tutor.php" class="no-ch">Hapus Tutor</a></li>
<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Peminjaman</a></li>
<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li><a href="matakuliah.php" class="daddy">Matakuliah</a><ul class="other-section">
<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
<li><a href="lihat_matakuliah.php" class="no-ch">Lihat Matakuliah</a></li>
<li><a href="ubahmatakuliah.php" class="no-ch">UbahMatakuliah</a></li>
<li><a href="hapus_matakuliah.php" class="no-ch">Hapus Matakuliah</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="info_lab.php" class="daddy">Info lab</a><ul class="other-section">
<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
<li><a href="lihat_berita.php" class="no-ch">Lihat Berita</a></li>
</ul>
</li>
<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a><ul class="other-section">
<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
</ul>
</div>

<div id='topPagination'>
		<div class="pagination noprt">
			<?php
			echo"[ Selamat Datang &nbsp;(..".$username."..) ] <a href='../login/logout.php'><u>Keluar</u></a>";
			?>
		<span> </span></a>
		</div>
	</div>

<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Lihat Data Mahasiswa</h1>
</div>
		<div class="">
					<form action="<?$_SERVER['PHP_SELF']?>" method='post' name='pencarian' id='pencarian'>
					  <input type='text' name='search' id='search'>
					  <input type='submit' name='submit' id='submit' value='CARI'>
					</form>
		</div>
		

		

<?php	


//Koneksi Ke Database
 $connect=mysqli_connect($host,$userdb,$passdb,$dbname);
// Cek Koneksi ke database
	// if (mysqli_connect_errno())
		// {
		// echo "koneksi ke databse error: " . mysqli_connect_error();//jika koneksi error, tampilkan errornya apa?
		// }
	// else
		// {
		// echo " ";//jika koneksi sukses
		// }
		
		//$connect=include ("koneksi.php");
		
		echo "<div class='tambahdata'> <a href='tambah_mahasiswa.php'><b>+<b>Tambah Data Baru</a> </div> </br>";
		// ================ TAMPILKAN DATANYA =====================//
		echo "<table border='1' class='tabeldata'><tr>
				<th width='20%'>Kontrol</th>
				
				<th width='10%'>NIM</th>
				<th width='10%'>Nama </th>
				<th width='8%'>Id Masuk</th> 
				<th width='10%'>Email</th> 
				<th width='5%'>Angkatan</th> 
				<th width='4%'>JK</th>
				<th width='8%'>Kabupaten</th>
				<th width='10%'>Provinsi</th>
				<th width='9%'>Prodi</th></tr>";
		$sql = "SELECT * FROM `mahasiswa` ORDER BY `Id_Mahasiswa` DESC  ";
		$result=mysqli_query($connect,$sql);
		if ($result==null)//cek 
		  {
		  die('</br>Error: ' . mysqli_error($connect));//jika pencarian data error, tampilkan errornya apa ?
		  }
		echo " "; //jika pencarian data sukses/berhasil
		$data 	= mysqli_num_rows($result);
		if ($data == 0) {
			echo "<tr><td  colspan='4'> Data Kosong</td></tr>";
		} else {
			$no = 1;
			while ($row = mysqli_fetch_array($result)) {
				
				// echo 	"<tr>
						// <td id='tengah'>
						// <form action='' method='post'>
							// <input type='hidden' name='Id_Mahasiswa' value=".$row['Id_Mahasiswa'].">
							// <input type='submit' name='btnaction' value='Edit'> |
							// <input type='submit' name='btnaction' onclick=
										    // \"return konfirmasiHapus(".$row['Id_Mahasiswa'].",
											// '".$row['Nama']."');\" value='Hapus'>
						// </form>
						// </td> ";
						echo "<tr>
						<td>
						<a href='ubah_mahasiswa.php'>Edit </a> |
						<a href=''>Hapus </a>
						</td>
						
						<td>$row[Nim]</td>
						<td>$row[Nama]</td>
						<td>$row[Id_Masuk]</td>
						<td>$row[Email]</td>
						<td>$row[Angkatan]</td>
						<td>$row[Jenis_Kelamin]</td> 
						<td>$row[Kabupaten]</td>
						<td>$row[Provinsi]</td>
						<td>$row[Prodi]</td>
						</tr>";
				$no++;
			}
		}
		echo "</table>";
		
		
mysqli_close($connect);	// tutup koneksi		
?>


</div>

</div>
</div>
<div id="bottomPagination">Copy Right Labkom STKIP Surya 2013/2014
</div>
</body></html>

<?php
}
	else {
		header("location:../index.php");
	}

?>