
<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="base.css" /><link rel="stylesheet" type="text/css" href="content.css" /><link rel="stylesheet" type="text/css" href="nav.css" /><title>Lihat Komputer | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>
<div id="siteNav">
<ul>
<li><a href="index.php" class="daddy main-node">BERANDA</a></li><li class="current-page-parent"><a href="penggunaan_lab.php" class="current-page-parent daddy">Penggunaan Lab</a><ul>
<li><a href="jadwal.php" class="daddy">Jadwal</a><ul class="other-section">
<li><a href="buat_jadwal.php" class="no-ch">Buat Jadwal</a></li>
<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
<li><a href="ubah_jadwal.php" class="no-ch">Ubah Jadwal</a></li>
<li><a href="hapus_jadwal.php" class="no-ch">Hapus Jadwal</a></li>
</ul>
</li>
<li><a href="ruangan.php" class="daddy">Ruangan</a><ul class="other-section">
<li><a href="tambah_ruangan.php" class="no-ch">Tambah Ruangan</a></li>
<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
<li><a href="ubah_ruangan.php" class="no-ch">Ubah Ruangan</a></li>
<li><a href="hapus_ruangan.php" class="no-ch">Hapus Ruangan</a></li>
</ul>
</li>
<li class="current-page-parent"><a href="komputer.php" class="current-page-parent daddy">Komputer</a><ul>
<li><a href="tambah_komputer.php" class="no-ch">Tambah Komputer</a></li>
<li id="active"><a href="lihat_komputer.php" class="active no-ch">Lihat Komputer</a></li>
<li><a href="ubah_komputer.php" class="no-ch">Ubah Komputer</a></li>
<li><a href="hapus_komputer.php" class="no-ch">Hapus Komputer</a></li>
</ul>
</li>

<li><a href="buku_tamu.php" class="daddy">Buku Tamu</a>
							<ul class="other-section">
							<li><a href="tulis_komentar.php" class="no-ch">Tulis  Komentar</a></li>
							<li><a href="lihat_bukutamu.php" class="no-ch">Lihat Semua Buku Tamu</a></li>
							
							</ul>
				</li>

</ul>
</li>
<li><a href="kinerja.php" class="daddy">Kinerja</a><ul class="other-section">
<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
</ul>
</li>
<li><a href="kebutuhan.php" class="daddy">Kebutuhan</a><ul class="other-section">
<li><a href="mahasiswa.php" class="daddy">Mahasiswa</a><ul class="other-section">
<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
<li><a href="lihat_mahasiswa.php" class="no-ch">Lihat Mahasiswa</a></li>
<li><a href="ubah_mahasiswa.php" class="no-ch">Ubah Mahasiswa</a></li>
<li><a href="hapus_mahasiswa.php" class="no-ch">Hapus Mahasiswa</a></li>
<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pemesanan</a></li>
<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan</a></li>
<li><a href="mengambil_matakuliah.php" class="no-ch">Mengambil Matakuliah</a></li>
</ul>
</li>
<li><a href="dosen.php" class="daddy">Dosen</a><ul class="other-section">
<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
<li><a href="lihat_dosen.php" class="no-ch">Lihat Dosen</a></li>
<li><a href="ubah_dosen.php" class="no-ch">Ubah Dosen</a></li>
<li><a href="hapus_dosen.php" class="no-ch">Hapus Dosen</a></li>
<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Peminjaman Ruangan</a></li>
<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li><a href="tutor.php" class="daddy">Tutor</a><ul class="other-section">
<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
<li><a href="lihat_tutor.php" class="no-ch">Lihat Tutor</a></li>
<li><a href="ubah_tutor.php" class="no-ch">Ubah Tutor</a></li>
<li><a href="hapus_tutor.php" class="no-ch">Hapus Tutor</a></li>
<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Peminjaman</a></li>
<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li><a href="matakuliah.php" class="daddy">Matakuliah</a><ul class="other-section">
<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
<li><a href="lihat_matakuliah.php" class="no-ch">Lihat Matakuliah</a></li>
<li><a href="ubahmatakuliah.php" class="no-ch">UbahMatakuliah</a></li>
<li><a href="hapus_matakuliah.php" class="no-ch">Hapus Matakuliah</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="info_lab.php" class="daddy">Info lab</a><ul class="other-section">
<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
<li><a href="lihat_berita.php" class="no-ch">Lihat Berita</a></li>
</ul>
</li>
<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a><ul class="other-section">
<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
</ul>
</div>
<div id='topPagination'><div class="pagination noprt"><a href="tambah_komputer.php" class="prev"><span>&laquo; </span>Previous</a> | <a href="ubah_komputer.php" class="next"> Next<span> &raquo;</span></a></div>
</div><div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Lihat Buku Tamu</h1>

<!-- START BUKU TAMU -->
									
		
<div id="tengah">

<?php
include("koneksi.php");
$qry=mysql_query("select * from buku_tamu order by Id_Tamu Desc limit 10"); 
$no=1;
while ($hasil=mysql_fetch_array($qry))
{
$no++;
if ($no%2==0)
{
?>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:white; border:1px solid #F3E6E6;border-radius:5px 0px 0px 5px;-moz-border-radius:5px 0px 0px 5px;" >
      <tr>
<td align="left" height="50px">
<div style="margin:-10px 0px;background:LightGray ;border-radius:2px 0px 0px 5px;-moz-border-radius:0px 2px 5px 0px; width:100%; color:black; font-weight:bold;font-size:15px; text-transform:capitalize;" >
				
<img src="gbr/prof4.gif" width="30" height="30" style="border:none" /> 
<? echo "$hasil[Nama]"; ?>&nbsp; mengatakan ,,,</div></td>
		      </tr>
            
            <tr>
              <td>
                <? 
		echo"<font face=arial, Helvetica, sans-serif size=2>$hasil[Komentar]</font>";
		?>		</td>
		  </tr>
            <tr>
              <td><br/>
                <? 
		echo "<font color='#999' face=arial, Helvetica, sans-serif size=1>Tanggal Pengiriman :</font>";
		$tanggal=$hasil['Tanggal'];
		$tgl=substr($tanggal,8,2);
		$bln=substr($tanggal,5,2);
		$thn=substr($tanggal,0,4);				
		echo " <font color='#999' face=arial, Helvetica, sans-serif size=1>$tgl-$bln-$thn<br/>Email: <a href=mailto:$hasil[Email]>&nbsp;<u>$hasil[Email]</a></u></font>";
		?>
                </font></td>
              </tr>
    </table>
	<br>
	
<?php }
else
{
?>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:white; border:1px solid #F3E6E6;border-radius:5px 0px 0px 5px;-moz-border-radius:5px 0px 0px 5px;" >
	<tr align="<?php echo $align; ?>">
	<td align="left" height="50px">
	<div style="margin:-10px 0px;background:LightGray ;border-radius:2px 0px 0px 5px;-moz-border-radius:0px 2px 5px 0px; width:100%; color:black; font-weight:bold;font-size:15px; text-transform:capitalize;" >
	<img src="gbr/prof4.gif" width="30" height="30" align="middle" style="border:none" /><? echo "$hasil[Nama]"; ?> &nbsp; mengatakan ,,, </div></td>
		      </tr>
            
            <tr>
              <td >
                <? 
		echo"<font face=arial, Helvetica, sans-serif size=2>$hasil[Komentar]</font>";
		?>		</td>
		  </tr>
            <tr>
              <td><br/>
                <? 
		echo "<font color='#999' face=arial, Helvetica, sans-serif size=1>Tanggal Pengiriman :</font>";
		$tanggal=$hasil['Tanggal'];
		$tgl=substr($tanggal,8,2);
		$bln=substr($tanggal,5,2);
		$thn=substr($tanggal,0,4);				
		echo "<font color='#999' face=arial, Helvetica, sans-serif size=1> $tgl-$bln-$thn <br/> Email : <a href=mailto:$hasil[Email]>&nbsp;<u>$hasil[Email]</a></u></font>";
		?>
                </font></td>
              </tr>
    </table>
	<br/>
	<?php }
}
?>
</div>
				<!-- End BUKU TAMU -->

</div>
<div id='bottomPagination'><div class="pagination noprt"><a href="tambah_komputer.php" class="prev"><span>&laquo; </span>Previous</a> | <a href="ubah_komputer.php" class="next"> Next<span> &raquo;</span></a></div>
</div></div>
</div>
</body></html>
