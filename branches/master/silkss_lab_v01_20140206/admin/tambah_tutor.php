<?php
include "koneksi.php";
@session_start();
$username= $_SESSION['username'];
if ($username){
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="base.css" /><link rel="stylesheet" type="text/css" href="content.css" /><link rel="stylesheet" type="text/css" href="nav.css" /><title>Tambah Tutor | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>
<div id="siteNav">
	<ul>
	<li id="active"><a href="index.php" class="active daddy main-node">BERANDA</a></li>
	<li><a href="penggunaan_lab.php" class="daddy">Penggunaan Lab</a>

	<ul class="other-section">
				<li><a href="jadwal.php" class="daddy">Jadwal</a>
							<ul class="other-section">
							<li><a href="buat_jadwal.php" class="no-ch">Buat Jadwal</a></li>
							<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
							
							</ul>
				</li>
				
				<li><a href="ruangan.php" class="daddy">Ruangan</a>
							<ul class="other-section">
							<li><a href="tambah_ruangan.php" class="no-ch">Tambah Ruangan</a></li>
							<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
							
							</ul>
				</li>
				<li><a href="komputer.php" class="daddy">Komputer</a>
							<ul class="other-section">
							<li><a href="tambah_komputer.php" class="no-ch">Tambah Komputer</a></li>
							<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
							
							</ul>
				</li>
				
				
	</ul>
	</li>

	<li><a href="kinerja.php" class="daddy">Kinerja</a>
				<ul class="other-section">
				<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
				<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
				<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
				<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
				</ul>
				</li>
				
	<li><a href="kebutuhan.php" class="daddy">Kebutuhan</a>
	<ul class="other-section">
					<li><a href="mahasiswa.php" class="daddy">Mahasiswa</a>
					<ul class="other-section">
							<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
							<li><a href="lihat_mahasiswa_2.php" class="no-ch">Lihat Mahasiswa</a></li>
							<li><a href="formulir_pemakaian.php" class="no-ch">Formulir Pakai Komputer</a></li>
							<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pesan Komputer</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan Kom.
							</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemakaian Kom.</a></li>
							
					</ul>
					</li>
					
					<li><a href="dosen.php" class="daddy">Dosen</a>
					<ul class="other-section">
							<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
							<li><a href="lihat_dosen.php" class="no-ch">Lihat Dosen</a></li>
							
							<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Pinjam Ruang</a></li>
							<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a href="tutor.php" class="daddy">Tutor</a>
					<ul class="other-section">
							<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
							<li><a href="lihat_tutor.php" class="no-ch">Lihat Tutor</a></li>
							
							<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Pinjam Ruang </a></li>
							<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a href="matakuliah.php" class="daddy">Matakuliah</a>
					<ul class="other-section">
							<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
							<li><a href="lihat_matakuliah.php" class="no-ch">Lihat Matakuliah</a></li>
							
					</ul>
					</li>
	</ul>
	</li>

	<li><a href="info_lab.php" class="daddy">Info lab</a>
			<ul class="other-section">
					<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
					<li><a href="lihat_berita.php" class="no-ch">Lihat Berita</a></li>
			</ul>
	</li>

	<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a>
			<ul class="other-section">
					<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
					<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
			</ul>
	</li>

	</div>
<div id='topPagination'>
		<div class="pagination noprt">
			<?php
			echo"Selamat Datang &nbsp; ' <i>".$username." '</i> <a href='../login/logout.php'><u>Keluar</u></a>";
			?>
		<span> </span></a>
		</div>
	</div>

<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle"></h1>

<div class="lihatdata">
		<a href="lihat_tutor.php"  >Lihat Data Tutor </a>
	</div>
	<hr></hr>

	
<script language="JavaScript" type="text/javascript">

function ceknip(a) {

re = /^[0-9]{11,}$/;<!--username harus berupa huruf, baik huruf besar ataupun kecil dan banyaknya minimal 1 huruf--!>
return re.test(a);

}

function cekuser(a) {

re = /^[A-Za-z]{5,1}$/;<!--username harus berupa huruf, baik huruf besar ataupun kecil dan banyaknya minimal 1 huruf--!>
return re.test(a);

}


function cekpassword(a) {

re = /^[A-Za-z]{8,1}$/; <!--password harus berupa huruf, baik huruf besar ataupun kecil dan banyaknya minimal 8 huruf--!>
return re.test(a);
}
function checkForm(b) {
if (!ceknip(b.nip.value)) {

alert("Isilah Nip sesuai ketentuan!! Nip berupa angka (minimal 11) ");

b.nim.focus();
return false;
}


if (!cekuser(b.username.value)) {

alert("Isilah Username dan Password sesuai ketentuan!! Username dan Password harus berupa huruf !! ");

b.username.focus();
return false;
}


if (!cekpassword(b.password.value)) {
alert("Masukkan Password dengan benar!! Password minimal terdiri dari 8 huruf !!");
b.password.focus();
return false;
}
return true;
}
</script>
<!-- Start Formoid form-->
<link rel="stylesheet" href="tambah_tutor_files/formoid1/formoid-default-red.css" type="text/css" />
<script type="text/javascript" src="tambah_tutor_files/formoid1/jquery.min.js"></script>
<form action="simpan_tutor.php" class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:880px;min-width:150px" method="post"><div class="title">
<table width="100%">
<h2><center>Menambah Data Tutor</center></h2></div>
<tr>
  <td>
	<div class="element-input" ><label class="title">NIP<span class="required">*</span></label>
  </td>
  <td>
	<input class="small" type="text" name="nip" required="required"/><i>Nip berupa angka min.11 angka</i>
	</div>
  </td>
</tr>
<tr>
	<td>
	<div class="element-input" ><label class="title">Nama<span class="required">*</span></label>
	</td>
	<td>
	<input class="medium" type="text" name="nama" required="required"/>
	</div>
	</td>
</tr>

<tr>
	<td>
	<div class="element-input" ><label class="title">Username <span class="required">*</span></label>
	</td>
	<td>
	<input class="small" type="text" name="username" required="required"/><i>username berupa huruf min.5 huruf</i>
	</div>
	</td>
</tr>
<tr>
	<td>
	<div class="element-input" ><label class="title">Password <span class="required">*</span></label>
	</td>
	<td>
	<input class="small" type="password" name="password" required="required"/><i>password berupa huruf min.8 huruf</i>
	</div>
	</td>
</tr>

<tr>
	<td>
	<div class="element-select" >
			<label class="title">Level<span class="required">*</span></label>
		</td>
		<td>
		<div class="element-select" >
		<div class="small">	
		<span>
		<select name="level" required="required">
			<option value="pengajar">pengajar</option><br/>
		</select>
		</span>
		</div>	
		</div>
		</td>
	</div>
	</tr>

<tr>
	<td>
	<div class="element-select" ><label class="title">Prodi<span class="required">*</span></label>
	</td>
	<td>
	<div class="small"><span><div class="element-select" >
	<select name="prodi" required="required">
		<option value="Fisika">Fisika</option><br/>
		<option value="TIK">TIK</option><br/>
		<option value="Matematika">Matematika</option><br/>
		<option value="Kimia">Kimia</option><br/>
	</select><i></i></span>
	</div>
	</div>
	</div>
	</td>
</tr>
<tr>
	<td>
	<div class="element-email" ><label class="title">Email</label>
	</td>
	<td>
	<input class="medium" type="email" name="email" value="" /></div>
	</td>
</tr>

<tr>
	<td>
	<div class="element-radio" ><label class="title">Jenis Kelamin</label>	
		</td>
		<td>
		<div class="column column1">
		<div class="element-radio" >
		<input type="radio" name="jk" value="L" /><span>L</span><br/>
		<input type="radio" name="jk" value="P" /><span>P</span><br/>
		</div><span class="clearfix"></span>
		</div>
		</td>
	</div>
</tr>
<tr>
<td>


</td>
<td>
<div class="submit"><input type="submit" value="Simpan"/></div>
</td>
</table>
</form>
<script type="text/javascript" src="tambah_tutor_files/formoid1/formoid-default-red.js"></script>
<!-- Stop Formoid form-->



</div>
</div>
</div>

</body>

</html>
<div id="bottomPagination">Copy Right Labkom STKIP Surya 2013/2014
</div>
<?php
}
	else {
		header("location:../index.php");
	}

?>