
<?php
include "koneksi.php";
@session_start();
$username= $_SESSION['username'];
if ($username){
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
<link rel="stylesheet" type="text/css" href="../base.css" />
<link rel="stylesheet" type="text/css" href="../content.css" />
<link rel="stylesheet" type="text/css" href="../nav.css" />
<title>Ubah Mahasiswa | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="../common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>
<div id="siteNav">
	<ul>
	<li id="active"><a href="index.php" class="active daddy main-node">BERANDA</a></li>
	<li><a href="penggunaan_lab.php" class="daddy">Penggunaan Lab</a>

	<ul class="other-section">
				<li><a href="jadwal.php" class="daddy">Jadwal</a>
							<ul class="other-section">
							<li><a href="buat_jadwal.php" class="no-ch">Buat Jadwal</a></li>
							<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
							
							</ul>
				</li>
				
				<li><a href="ruangan.php" class="daddy">Ruangan</a>
							<ul class="other-section">
							<li><a href="tambah_ruangan.php" class="no-ch">Tambah Ruangan</a></li>
							<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
							
							</ul>
				</li>
				<li><a href="komputer.php" class="daddy">Komputer</a>
							<ul class="other-section">
							<li><a href="tambah_komputer.php" class="no-ch">Tambah Komputer</a></li>
							<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
							
							</ul>
				</li>
				
				
	</ul>
	</li>

	<li><a href="kinerja.php" class="daddy">Kinerja</a>
				<ul class="other-section">
				<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
				<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
				<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
				<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
				</ul>
				</li>
				
	<li><a href="kebutuhan.php" class="daddy">Kebutuhan</a>
	<ul class="other-section">
					<li><a href="mahasiswa.php" class="daddy">Mahasiswa</a>
					<ul class="other-section">
							<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
							<li><a href="lihat_mahasiswa_2.php" class="no-ch">Lihat Mahasiswa</a></li>
							<li><a href="formulir_pemakaian.php" class="no-ch">Formulir Pakai Komputer</a></li>
							<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pesan Komputer</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan Kom.
							</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemakaian Kom.</a></li>
							
					</ul>
					</li>
					
					<li><a href="dosen.php" class="daddy">Dosen</a>
					<ul class="other-section">
							<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
							<li><a href="lihat_dosen.php" class="no-ch">Lihat Dosen</a></li>
							
							<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Pinjam Ruang</a></li>
							<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a href="tutor.php" class="daddy">Tutor</a>
					<ul class="other-section">
							<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
							<li><a href="lihat_tutor.php" class="no-ch">Lihat Tutor</a></li>
							
							<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Pinjam Ruang </a></li>
							<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a href="matakuliah.php" class="daddy">Matakuliah</a>
					<ul class="other-section">
							<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
							<li><a href="lihat_matakuliah.php" class="no-ch">Lihat Matakuliah</a></li>
							
					</ul>
					</li>
	</ul>
	</li>

	<li><a href="info_lab.php" class="daddy">Info lab</a>
			<ul class="other-section">
					<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
					<li><a href="lihat_berita.php" class="no-ch">Lihat Berita</a></li>
			</ul>
	</li>

	<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a>
			<ul class="other-section">
					<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
					<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
			</ul>
	</li>

	</div>

	<div id='topPagination'>
		<div class="pagination noprt">
			<?php
			echo"Selamat Datang &nbsp; ' <i>".$username." '</i> <a href='../login/logout.php'><u>Keluar</u></a>";
			?>
		<span> </span></a>
		</div>
	</div>


<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle"></h1>
<div class="lihatdata">
		<a href="lihat_mahasiswa_2.php"  >Lihat Data Mahasiswa </a>
	</div>
	<hr></hr>
<?php
//ubah mahasiswa

include 'connect.php';
if (isset($_GET['id'])) {
    $query = $dbh->query("SELECT * FROM mahasiswa AS mhs
				LEFT JOIN masuk AS m ON mhs.Id_Masuk=m.Id_Masuk
				WHERE mhs.Id_Mahasiswa = '$_GET[id]'");
    $data  = $query->fetch(PDO::FETCH_ASSOC);
} else {
    echo "ID tidak tersedia!<br /><a href='lihat_mahasiswa.php'>Kembali</a>";
    exit();
}

if ($data === false) {
    echo "Data tidak ditemukan!<br /><a href='lihat_mahasiswa.php'>Kembali</a>";
    exit();
}
?>


   

	<script language="JavaScript" type="text/javascript">

function ceknim(a) {
re = /^[0-9]{11,}$/;<!--username harus berupa huruf, baik huruf besar ataupun kecil dan banyaknya minimal 1 huruf--!>
return re.test(a);
}

function cekuser(a) {
re = /^[A-Za-z]{5,1}$/;<!--username harus berupa huruf, baik huruf besar ataupun kecil dan banyaknya minimal 1 huruf--!>
return re.test(a);

}

function cekpassword(a) {

re = /^[A-Za-z]{8,1}$/; <!--password harus berupa huruf, baik huruf besar ataupun kecil dan banyaknya minimal 8 huruf--!>
return re.test(a);
}
function checkForm(b) {
if (!ceknim(b.nim.value)) {
alert("Isilah Nim sesuai ketentuan!! Nim berupa angka (jumlah 11) ");
b.nim.focus();
return false;
}

if (!cekuser(b.username.value)) {
alert("Isilah Username dan Password sesuai ketentuan!! Username dan Password harus berupa huruf !! ");

b.username.focus();
return false;
}

if (!cekpassword(b.password.value)) {
alert("Masukkan Password dengan benar!! Password minimal terdiri dari 8 huruf !!");
b.password.focus();
return false;
}
return true;
}
</script>

	
<link rel="stylesheet" href="tambah_mahasiswa_files/formoid1/formoid-default-red.css" type="text/css" />
<script type="text/javascript" src="tambah_mahasiswa_files/formoid1/jquery.min.js">
</script>
<form action="update_mahasiswa.php" class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:880px;min-width:150px" method="POST">
<table width="100%">
	<div class="title"> 
	Ubah Data Mahasiswa		
	</div>
	<div class="element-input" >
	<tr>
		<input type="hidden" name="id" value="<?php echo $data['Id_Mahasiswa']; ?>" />
				
		<td>
		<div class="element-input" >
			<label class="title">Nim<span class="required">*</span></label>
		</td>
		<td>
			<input class="small" type="text" name="nim" required value="<?php echo $data['Nim']; ?>"/><i class="note">Nim berupa angka min.11 angka</i>
			</div>
		</td>
	</tr>
	<tr>
	<div class="element-input" >
		<td>
			<label class="title">Nama<span class="required">*</span></label>
		</td>
		<td>
			<input class="medium" type="text" name="name" required value="<?php echo $data['Nama']; ?>"/></div>
		</td>
	</tr>
	<tr>
	<div class="element-input" >
		<td>
			<label class="title">Username<span class="required">*</span></label>
		</td>
		<td>
			<input class="small" type="text" name="username" required value="<?php echo $data['Username']; ?>"/><i class="note">username berupa huruf min.5 huruf</i>
	</div>
		</td>
	</tr>
	<tr>
	<div class="element-input" >
		<td>
			<label class="title">Password <span class="required">*</span></label>
		</td>
		<td>
			<input class="small" type="password" name="password" required value="<?php echo $data['Password']; ?>"/><i class="note">password berupa huruf min.8 huruf</i>
	</div>
		</td>
	</tr>
	
	<tr>
	<td>
	<div class="element-select" >
			<label class="title">Level<span class="required">*</span></label>
		</td>
		<td>
		<div class="element-select" >
		<div class="small">	
		<span>
		<select name="level" required="required">
			<option value="mahasiswa">mahasiswa</option><br/>
		</select>
		</span>
		</div>	
		</div>
		</td>
	</div>
	</td>
	</tr>
	
	<tr>
	
	<div class="element-email" >
		<td>
			<label class="title">Email<span class="required">*</span></label>
		</td>
		<td>
			<input class="medium" type="email" name="email" value="" required value="<?php echo $data['Email']; ?>"/></div>
		</td>
	</tr>
	
	<tr>
	<td>
	<div class="element-select" >
			<label class="title">Angkatan<span class="required">*</span></label>
		</td>
		<td>
		<div class="element-select" >
		<div class="small">	
		<span>
		<select name="angkatan" required value="<?php echo $data['Angkatan']; ?>">
			<option value="2010">2010</option><br/>
			<option value="2011">2011</option><br/>
			<option value="2012">2012</option><br/>
			<option value="2013">2013</option><br/>
			<option value="2014">2014</option><br/>
			<option value="2015">2015</option><br/>
			<option value="2016">2016</option><br/>
			<option value="2017">2017</option><br/>
			<option value="2018">2018</option><br/>
			<option value="2019">2019</option><br/>
			<option value="2020">2020</option><br/>
			<option value="2021">2021</option><br/>
			<option value="2022">2022</option><br/>
			<option value="2023">2023</option><br/>
			<option value="2024">2024</option><br/>
			<option value="2025">2025</option><br/>
		</select><i></i>
		</span>
		</div>	
		</div>
		</td>
	</div>
	</td>
	</tr>
	<tr>
	<div class="element-radio" >
		<td>
			<label class="title">Jenis Kelamin<span class="required">*</span></label>	
		</td>
		<td>
		<div class="column column1"><input type="radio" name="jk" value="L"  required value="<?php echo $data['Jenis_Kelamin']; ?>" /><span>L</span><br/>
									<input type="radio" name="jk" value="P" required value="<?php echo $data['Jenis_Kelamin']; ?>" /><span>P</span><br/></div><span class="clearfix"></span>
	</div>
		</td>
	</tr>
	<tr>
		<td>
			<div class="element-input" >
			<label class="title">Kabupaten<span class="required"></span></label>
		</td>
		<td>
			<input class="medium" type="text" name="kab"  value="<?php echo $data['Kabupaten']; ?>" /></div>
		</td>
	</tr>
	
	<tr>
		<td>
		<div class="element-select" ><label class="title">Provinsi<span class="required">*</span></label>
	</td>
	<td>
	<div class="element-select" >
		<div class="small"><span><select name="prov" required value="<?php echo $data['Provinsi']; ?>">
		<option value="Nanggro Aceh Darussalam ">Nanggro Aceh Darussalam </option><br/>
		<option value="Sumatera Utara ">Sumatera Utara </option><br/>
		<option value="Sumatera Barat ">Sumatera Barat </option><br/>
		<option value="Riau">Riau</option><br/>
		<option value="Kepulauan Riau">Kepulauan Riau</option><br/>
		<option value="Jambi">Jambi</option><br/>
		<option value="Sumatera Selatan">Sumatera Selatan</option><br/>
		<option value="Bangka Belitung ">Bangka Belitung </option><br/>
		<option value="Bengkulu">Bengkulu</option><br/>
		<option value="Lampung">Lampung</option><br/>
		<option value="DKI Jakarta ">DKI Jakarta </option><br/>
		<option value="Jawa Barat">Jawa Barat</option><br/>
		<option value="Banten">Banten</option><br/>
		<option value="Jawa Tengah">Jawa Tengah</option><br/>
		<option value="Daerah Istimewa Yogyakarta">Daerah Istimewa Yogyakarta</option><br/>
		<option value="Jawa TImur">Jawa TImur</option><br/>
		<option value="Bali">Bali</option><br/>
		<option value=" Nusa Tenggara Barat"> Nusa Tenggara Barat</option><br/>
		<option value=" Nusa Tenggara Timur"> Nusa Tenggara Timur</option><br/>
		<option value="Kalimantan Barat">Kalimantan Barat</option><br/>
		<option value="Kalimantan Tengah">Kalimantan Tengah</option><br/>
		<option value="Kalimantan Selatan">Kalimantan Selatan</option><br/>
		<option value="Kalimantan Timur">Kalimantan Timur</option><br/>
		<option value="Sulawesi Utara">Sulawesi Utara</option><br/>
		<option value="Sulawesi Barat">Sulawesi Barat</option><br/>
		<option value="Sulawesi Tengah">Sulawesi Tengah</option><br/>
		<option value="Sulawesi Tenggara">Sulawesi Tenggara</option><br/>
		<option value="Sulawesi Selatan">Sulawesi Selatan</option><br/>
		<option value="Gorontalo">Gorontalo</option><br/>
		<option value="Maluku ">Maluku </option><br/>
		<option value="Maluku  Utara">Maluku  Utara</option><br/>
		<option value="Papua Barat">Papua Barat</option><br/>
		<option value="Papua ">Papua </option><br/>
		<option value="Kalimantan Utara">Kalimantan Utara</option><br/></select><i></i></span></div></div>
	</div>
	</td>
	</tr>
	
	<tr>
		<td>
			<div class="element-select" >
			<label class="title">Prodi<span class="required">*</span></label>
		</td>
		<td>
			<div class="element-select" >
			<div class="small">
			<span>
			<select name="prodi" required value="<?php echo $data['Prodi']; ?>">
				<option value="Fisika">Fisika</option><br/>
				<option value="TIK">TIK</option><br/>
				<option value="Matematika">Matematika</option><br/>
				<option value="Kimia">Kimia</option><br/></select><i></i>
			</span>
			</div>
			</div>
			</div>
		</td>
		</tr>
		
		<tr>
		<td>
		</td>
		<td>
			<div class="submit">
				<input type="submit"  value="Simpan"/>
				<input type="reset" value="Reset" onclick="return confirm('hapus data yang telah diinput?')">
			
			</div>
		</td>		
		</tr>
	
</table>
</form>
<script type="text/javascript" src="tambah_mahasiswa_files/formoid1/formoid-default-red.js"></script>

</div>

<br/>
	<!-- Stop Formoid form-->
	<center><a href="lihat_mahasiswa_2.php">&Lt; Tabel Mahasiswa</a></center>

</div>



</div>
</div>
<div id="bottomPagination" >Copy Right Labkom STKIP Surya 2013/2014
</div>
</body></html>
<?php
}
	else {
		header("location:../index.php");
	}

?>