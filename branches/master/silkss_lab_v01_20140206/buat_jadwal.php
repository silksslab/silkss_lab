
<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="base.css" /><link rel="stylesheet" type="text/css" href="content.css" /><link rel="stylesheet" type="text/css" href="nav.css" /><title>Buat Jadwal | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>
<div id="siteNav">
<ul>
<li><a href="index.php" class="daddy main-node">BERANDA</a></li><li class="current-page-parent"><a href="penggunaan_lab.php" class="current-page-parent daddy">Penggunaan Lab</a><ul>
<li class="current-page-parent"><a href="jadwal.php" class="current-page-parent daddy">Jadwal</a><ul>
<li id="active"><a href="buat_jadwal.php" class="active no-ch">Buat Jadwal</a></li>
<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
<li><a href="ubah_jadwal.php" class="no-ch">Ubah Jadwal</a></li>
<li><a href="hapus_jadwal.php" class="no-ch">Hapus Jadwal</a></li>
</ul>
</li>
<li><a href="ruangan.php" class="daddy">Ruangan</a><ul class="other-section">
<li><a href="tambah_ruangan.php" class="no-ch">Tambah Ruangan</a></li>
<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
<li><a href="ubah_ruangan.php" class="no-ch">Ubah Ruangan</a></li>
<li><a href="hapus_ruangan.php" class="no-ch">Hapus Ruangan</a></li>
</ul>
</li>
<li><a href="komputer.php" class="daddy">Komputer</a><ul class="other-section">
<li><a href="tambah_komputer.php" class="no-ch">Tambah Komputer</a></li>
<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
<li><a href="ubah_komputer.php" class="no-ch">Ubah Komputer</a></li>
<li><a href="hapus_komputer.php" class="no-ch">Hapus Komputer</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="kinerja.php" class="daddy">Kinerja</a><ul class="other-section">
<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
</ul>
</li>
<li><a href="kebutuhan.php" class="daddy">Kebutuhan</a><ul class="other-section">
<li><a href="mahasiswa.php" class="daddy">Mahasiswa</a><ul class="other-section">
<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
<li><a href="lihat_mahasiswa.php" class="no-ch">Lihat Mahasiswa</a></li>
<li><a href="ubah_mahasiswa.php" class="no-ch">Ubah Mahasiswa</a></li>
<li><a href="hapus_mahasiswa.php" class="no-ch">Hapus Mahasiswa</a></li>
<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pemesanan</a></li>
<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan</a></li>
<li><a href="mengambil_matakuliah.php" class="no-ch">Mengambil Matakuliah</a></li>
</ul>
</li>
<li><a href="dosen.php" class="daddy">Dosen</a><ul class="other-section">
<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
<li><a href="lihat_dosen.php" class="no-ch">Lihat Dosen</a></li>
<li><a href="ubah_dosen.php" class="no-ch">Ubah Dosen</a></li>
<li><a href="hapus_dosen.php" class="no-ch">Hapus Dosen</a></li>
<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Peminjaman Ruangan</a></li>
<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li><a href="tutor.php" class="daddy">Tutor</a><ul class="other-section">
<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
<li><a href="lihat_tutor.php" class="no-ch">Lihat Tutor</a></li>
<li><a href="ubah_tutor.php" class="no-ch">Ubah Tutor</a></li>
<li><a href="hapus_tutor.php" class="no-ch">Hapus Tutor</a></li>
<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Peminjaman</a></li>
<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li><a href="matakuliah.php" class="daddy">Matakuliah</a><ul class="other-section">
<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
<li><a href="lihat_matakuliah.php" class="no-ch">Lihat Matakuliah</a></li>
<li><a href="ubahmatakuliah.php" class="no-ch">UbahMatakuliah</a></li>
<li><a href="hapus_matakuliah.php" class="no-ch">Hapus Matakuliah</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="info_lab.php" class="daddy">Info lab</a><ul class="other-section">
<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
<li><a href="lihat_berita.php" class="no-ch">Lihat Berita</a></li>
</ul>
</li>
<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a><ul class="other-section">
<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
</ul>
</div>
<div id='topPagination'><div class="pagination noprt"><a href="jadwal.php" class="prev"><span>&laquo; </span>Previous</a> | <a href="lihat_jadwal.php" class="next"> Next<span> &raquo;</span></a></div>
</div><div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Buat Jadwal</h1>

<!-- Start jadwal -->
<link rel="stylesheet" href="tambah_jadwal_files/formoid1/formoid-default-red.css" type="text/css" />
<script type="text/javascript" src="tambah_jadwal_files/formoid1/jquery.min.js"></script>
<form class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:480px;min-width:150px" method="post">
<div class="title"><h2>Menambahkan Jadwal</h2></div>
	<div class="element-select" ><label class="title">Hari<span class="required">*</span></label><div class="small"><span>
	<select name="harijdwl" required="required">
		<option value="Senin">Senin</option><br/>
		<option value="Selasa">Selasa</option><br/>
		<option value="Rabu">Rabu</option><br/>
		<option value="Kamis">Kamis</option><br/>
		<option value="Jumat">Jumat</option><br/>
		<option value="Sabtu">Sabtu</option><br/>
		<option value="Minggu">Minggu</option><br/>
	</select><i></i></span></div></div>
	<div class="element-input" ><label class="title">Waktu<span class="required">*</span></label>
		<input class="small" type="text" name="wktjdwl" required="required"/>
	</div>
	<div class="element-input" ><label class="title">Matakuliah<span class="required">*</span></label>
		<input class="medium" type="text" name="mkjdwl" required="required"/>
	</div>
	<div class="element-input" ><label class="title">Kelas<span class="required">*</span></label>
		<input class="small" type="text" name="klsjdwl" required="required"/>
	</div>
	<div class="element-input" ><label class="title">Pengajar<span class="required">*</span></label>
		<input class="medium" type="text" name="pengjdwl" required="required"/>
	</div>

<div class="submit"><input type="submit" value="Simpan"/></div></form>
<script type="text/javascript" src="tambah_jadwal_files/formoid1/formoid-default-red.js"></script>
<div id="formoid-info">Copy-Right Labkom STKIP-Surya</div>

<!-- Stop jadwal-->

</div>
<div id='division_budget'><div class="pagination noprt"><a href="jadwal.php" class="prev"><span>&laquo; </span>Previous</a> | <a href="lihat_jadwal.php" class="next"> Next<span> &raquo;</span></a></div>
</div></div>
</div>
</body></html>
