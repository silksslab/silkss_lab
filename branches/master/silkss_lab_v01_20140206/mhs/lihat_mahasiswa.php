
<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="base.css" /><link rel="stylesheet" type="text/css" href="content.css" /><link rel="stylesheet" type="text/css" href="nav.css" /><title>Lihat Mahasiswa | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>
<div id="siteNav">
<ul>
<li><a href="index.php" class="daddy main-node">BERANDA</a></li><li><a href="penggunaan_lab.php" class="daddy">Penggunaan Lab</a><ul class="other-section">
<li><a href="jadwal.php" class="daddy">Jadwal</a><ul class="other-section">
<li><a href="buat_jadwal.php" class="no-ch">Buat Jadwal</a></li>
<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
<li><a href="ubah_jadwal.php" class="no-ch">Ubah Jadwal</a></li>
<li><a href="hapus_jadwal.php" class="no-ch">Hapus Jadwal</a></li>
</ul>
</li>
<li><a href="ruangan.php" class="daddy">Ruangan</a><ul class="other-section">
<li><a href="tambah_ruangan.php" class="no-ch">Tambah Ruangan</a></li>
<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
<li><a href="ubah_ruangan.php" class="no-ch">Ubah Ruangan</a></li>
<li><a href="hapus_ruangan.php" class="no-ch">Hapus Ruangan</a></li>
</ul>
</li>
<li><a href="komputer.php" class="daddy">Komputer</a><ul class="other-section">
<li><a href="tambah_komputer.php" class="no-ch">Tambah Komputer</a></li>
<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
<li><a href="ubah_komputer.php" class="no-ch">Ubah Komputer</a></li>
<li><a href="hapus_komputer.php" class="no-ch">Hapus Komputer</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="kinerja.php" class="daddy">Kinerja</a><ul class="other-section">
<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
</ul>
</li>
<li class="current-page-parent"><a href="kebutuhan.php" class="current-page-parent daddy">Kebutuhan</a><ul>
<li class="current-page-parent"><a href="mahasiswa.php" class="current-page-parent daddy">Mahasiswa</a><ul>
<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
<li id="active"><a href="lihat_mahasiswa.php" class="active no-ch">Lihat Mahasiswa</a></li>
<li><a href="ubah_mahasiswa.php" class="no-ch">Ubah Mahasiswa</a></li>
<li><a href="hapus_mahasiswa.php" class="no-ch">Hapus Mahasiswa</a></li>
<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pemesanan</a></li>
<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan</a></li>
<li><a href="mengambil_matakuliah.php" class="no-ch">Mengambil Matakuliah</a></li>
</ul>
</li>
<li><a href="dosen.php" class="daddy">Dosen</a><ul class="other-section">
<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
<li><a href="lihat_dosen.php" class="no-ch">Lihat Dosen</a></li>
<li><a href="ubah_dosen.php" class="no-ch">Ubah Dosen</a></li>
<li><a href="hapus_dosen.php" class="no-ch">Hapus Dosen</a></li>
<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Peminjaman Ruangan</a></li>
<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li><a href="tutor.php" class="daddy">Tutor</a><ul class="other-section">
<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
<li><a href="lihat_tutor.php" class="no-ch">Lihat Tutor</a></li>
<li><a href="ubah_tutor.php" class="no-ch">Ubah Tutor</a></li>
<li><a href="hapus_tutor.php" class="no-ch">Hapus Tutor</a></li>
<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Peminjaman</a></li>
<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li><a href="matakuliah.php" class="daddy">Matakuliah</a><ul class="other-section">
<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
<li><a href="lihat_matakuliah.php" class="no-ch">Lihat Matakuliah</a></li>
<li><a href="ubahmatakuliah.php" class="no-ch">UbahMatakuliah</a></li>
<li><a href="hapus_matakuliah.php" class="no-ch">Hapus Matakuliah</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="info_lab.php" class="daddy">Info lab</a><ul class="other-section">
<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
<li><a href="lihat_berita.php" class="no-ch">Lihat Berita</a></li>
</ul>
</li>
<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a><ul class="other-section">
<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
</ul>
</div>
<div id='topPagination'><div class="pagination noprt"><a href="tambah_mahasiswa.php" class="prev"><span>&laquo; </span>Previous</a> | <a href="ubah_mahasiswa.php" class="next"> Next<span> &raquo;</span></a></div>
</div><div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Lihat Mahasiswa</h1>



<?php 

//echo $_SESSION['username'];
// mengambil data yang telah disimpan pada form
//session

//memberikan variabel
$name	= $_POST["name"];
$nim 	= $_POST["nim"];
//$idmasuk	= $_POST["idmasuk"];
$angkatan 	= $_POST["angkatan"];
$email 		= $_POST["email"];
$jk 	= $_POST["jk"];
$prodi	= $_POST["prodi"];
$kab 	= $_POST["kab"];
$prov		= $_POST["prov"];
$password =$_POST["password"];
$username =$_POST["username"];
$level =$_POST["level"];

//Koneksi Ke Database
$connect=mysqli_connect("localhost","root","","silkss_db");
// Cek Koneksi ke database
	if (mysqli_connect_errno())
		{
		echo "koneksi ke databse error: " . mysqli_connect_error();//jika koneksi error, tampilkan errornya apa?
		}
	else
		{
		echo "koneksi ke database sukses";//jika koneksi sukses
		}


$sql = "SELECT Username FROM `masuk` WHERE Username=".$username;
	$result=mysqli_query($connect,$sql);
	if ($result==null)//cek 
		  {
		  
//memasukan data  --> harus perhatikan jumlah field = valuesnya
$sql="INSERT INTO masuk (Id_Masuk,Password,Username,Nama,Level)
VALUES
   (default,
    '$_POST[password]',
	'$_POST[username]',
	'$_POST[name]',
	'$_POST[level]')";
// perintah untuk menjalankan query
if (!mysqli_query($connect,$sql))//cek 
  {
  die('</br>Error: ' . mysqli_error($connect));//jika penambahan data error, tampilkan errornya apa ?
  }
echo " </br> 1 baris bertambah"; //jika penambahan data sukses/berhasil

	
$sql="SELECT Id_Masuk FROM masuk WHERE
Password = '$_POST[password]' and
Username = '$_POST[username]' and
Nama = '$_POST[name]' and
Level = '$_POST[level]'";

$result = mysqli_query($connect,$sql);//buat variabel result utk simpan hasil select

$id_masuk = mysqli_fetch_array($result);
//die(var_dump((int)$id_masuk['Id_Masuk']));

//memasukan data ke database  --> harus perhatikan jumlah field = valuesnya

$sql = "SELECT Nim FROM `mahasiswa` WHERE Nim=".$nim;
	$result=mysqli_query($connect,$sql);
	if ($result==null)//cek 
		  {
		  die('</br>Error: ' . mysqli_error($connect));//jika pencarian data error, tampilkan errornya apa ?
		  }
	echo " </br> Pencarian berhasil"; //jika pencarian data sukses/berhasil
	$data 	= mysqli_num_rows($result);
	if ($data != 0) {
			echo "<tr><td  colspan='4'> <br><div style=background-color:orange;color:black;font-size:15px;text-align:center;>Data  Sudah Ada</div>  </td></tr>";
	} else {

$sql="INSERT INTO mahasiswa (
	Id_Mahasiswa,Nim,Nama,Id_Masuk,
	Email,Angkatan,Jenis_Kelamin,
	Kabupaten,Provinsi,Prodi)
VALUES
   (default, '".$nim."','".$name."', ".$id_masuk['Id_Masuk'].",'".$email."',".$angkatan.",'".$jk."','".$kab."','".$prov."','".$prodi."')";
// die($sql);
//(default, $nim,$name, ".(int)$id_masuk['Id_Masuk'].",
 //   $email, ".(int)$angkatan.",$jk ,
//	$kab, $prov,$prodi)";

	
// perintah untuk menjalankan query
if (!mysqli_query($connect,$sql))//cek 
  {
  die('</br>Error: ' . mysqli_error($connect));//jika penambahan data error, tampilkan errornya apa ?
  }
echo " </br> 1 baris bertambah"; //jika penambahan data sukses/berhasil


	
		// ================ TAMPILKAN DATANYA =====================//
		echo "<table border='1' class='data'><tr>
				<th width='40%'>Kontrol</th>
				<th width='10%'>Id Mahasiswa</th>
				<th width='10%'>NIM</th>
				<th width='10%'>Nama </th>
				<th width='10%'>Id Masuk</th> 
				<th width='10%'>Email</th> 
				<th width='5%'>Angkatan</th> 
				<th width='4%'>JK</th>
				<th width='8%'>Kabupaten</th>
				<th width='10%'>Provinsi</th>
				<th width='10%'>Prodi</th></tr>";
		$sql = "SELECT * FROM `mahasiswa` ORDER BY `Id_Mahasiswa` ASC  ";
		$result=mysqli_query($connect,$sql);
		if ($result==null)//cek 
		  {
		  die('</br>Error: ' . mysqli_error($connect));//jika pencarian data error, tampilkan errornya apa ?
		  }
		echo " </br> Pencarian berhasil"; //jika pencarian data sukses/berhasil
		$data 	= mysqli_num_rows($result);
		if ($data == 0) {
			echo "<tr><td  colspan='4'> Data Kosong</td></tr>";
		} else {
			$no = 1;
			while ($row = mysqli_fetch_array($result)) {
				
				echo 	"<tr>
						<td id='tengah'>
						<a href='''>Ubah</a> |
						<a href=' '>Hapus</a>
						</td>
						<td id='tengah'>$no</td>
						<td>$row[Nim]</td>
						<td>$row[Nama]</td>
						<td>$row[Id_Masuk]</td>
						<td>$row[Email]</td>
						<td>$row[Angkatan]</td>
						<td>$row[Jenis_Kelamin]</td> 
						<td>$row[Kabupaten]</td>
						<td>$row[Provinsi]</td>
						<td>$row[Prodi]</td>
						</tr>";
				$no++;
			}
		}
		echo "</table>";
		}
}		
mysqli_close($connect);	// tutup koneksi		
?>


</div>
<div id='division_budget'><div class="pagination noprt"><a href="tambah_mahasiswa.php" class="prev"><span>&laquo; </span>Previous</a> | <a href="ubah_mahasiswa.php" class="next"> Next<span> &raquo;</span></a></div>
</div></div>
</div>


<link rel="stylesheet" href="tambah_mahasiswa_files/formoid1/formoid-default-red.css" type="text/css" />
<script type="text/javascript" src="tambah_mahasiswa_files/formoid1/jquery.min.js">
</script>
<form action="" class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:880px;min-width:150px" method="POST">
<table width="100%">
	<div class="title"> 
	Ubah Data Mahasiswa		
	</div>
	<div class="element-input" >
	<tr>
		<td>
		<div class="element-input" >
			<label class="title">Nim<span class="required">*</span></label>
		</td>
		<td>
			<input class="small" type="text" name="nim" value="<?php '$nim' ?>" required="required"/></div>
		</td>
	</tr>
	<tr>
	<div class="element-input" >
		<td>
			<label class="title">Nama<span class="required">*</span></label>
		</td>
		<td>
			<input class="medium" type="text" name="name" required="required"/></div>
		</td>
	</tr>
	<tr>
	<div class="element-input" >
		<td>
			<label class="title">Username<span class="required">*</span></label>
		</td>
		<td>
			<input class="small" type="text" name="username" required="required"/>
	</div>
		</td>
	</tr>
	<tr>
	<div class="element-input" >
		<td>
			<label class="title">Password <span class="required">*</span></label>
		</td>
		<td>
			<input class="small" type="password" name="password" required="required"/>
	</div>
		</td>
	</tr>
	
	<tr>
	<div class="element-input" >
		<td>
			<label class="title">Level <span class="required">*</span></label>
		</td>
		<td>
			<input class="small" type="text" name="level" required="required"/>
	</div>
		</td>
	</tr>
	
	<tr>
	
	<div class="element-email" >
		<td>
			<label class="title">Email<span class="required">*</span></label>
		</td>
		<td>
			<input class="large" type="email" name="email" value="" required="required"/></div>
		</td>
	</tr>
	
	<tr>
	<td>
	<div class="element-select" >
			<label class="title">Angkatan<span class="required">*</span></label>
		</td>
		<td>
		<div class="element-select" >
		<div class="small">	
		<span>
		<select name="angkatan" required="required">
			<option value="2010">2010</option><br/>
			<option value="2011">2011</option><br/>
			<option value="2012">2012</option><br/>
			<option value="2013">2013</option><br/>
			<option value="2014">2014</option><br/>
			<option value="2015">2015</option><br/>
			<option value="2016">2016</option><br/>
			<option value="2017">2017</option><br/>
			<option value="2018">2018</option><br/>
			<option value="2019">2019</option><br/>
			<option value="2020">2020</option><br/>
			<option value="2021">2021</option><br/>
			<option value="2022">2022</option><br/>
			<option value="2023">2023</option><br/>
			<option value="2024">2024</option><br/>
			<option value="2025">2025</option><br/>
		</select><i></i>
		</span>
		</div>	
		</div>
		</td>
	</div>
	</td>
	</tr>
	<tr>
	<div class="element-radio" >
		<td>
			<label class="title">Jenis Kelamin<span class="required">*</span></label>	
		</td>
		<td>
		<div class="column column1"><input type="radio" name="jk" value="L" required="required"/><span>L</span><br/>
									<input type="radio" name="jk" value="P" required="required"/><span>P</span><br/></div><span class="clearfix"></span>
	</div>
		</td>
	</tr>
	<tr>
		<td>
			<div class="element-input" >
			<label class="title">Kabupaten<span class="required">*</span></label>
		</td>
		<td>
			<input class="medium" type="text" name="kab" required="required"/></div>
		</td>
	</tr>
	
	<tr>
		<td>
			<div class="element-input" >
			<label class="title">Provinsi</label>
		</td>
		<td>
			<input class="large" type="text" name="prov" /></div>
		</td>
	</tr>
	
	<tr>
		<td>
			<div class="element-select" >
			<label class="title">Prodi<span class="required">*</span></label>
		</td>
		<td>
			<div class="element-select" >
			<div class="small">
			<span>
			<select name="prodi" required="required">
				<option value="Fisika">Fisika</option><br/>
				<option value="TIK">TIK</option><br/>
				<option value="Matematika">Matematika</option><br/>
				<option value="Kimia">Kimia</option><br/></select><i></i>
			</span>
			</div>
			</div>
			</div>
		</td>
		</tr>
		
		<tr>
		<td>
		</td>
		<td>
			<div class="submit">
				<input type="submit" name="btnsimpan" value="Simpan"/>
			</div>
		</td>		
		</tr>
	
</table>
</form>
<script type="text/javascript" src="tambah_mahasiswa_files/formoid1/formoid-default-red.js"></script>

</div>

</body></html>
