<?php

define('EMAIL_FOR_REPORTS', 'yandripono@gmail.com');
define('RECAPTCHA_PRIVATE_KEY', '@privatekey@');
define('FINISH_URI', 'http://');
define('FINISH_ACTION', 'message');
define('FINISH_MESSAGE', 'Thanks for filling out my form!');
define('UPLOAD_ALLOWED_FILE_TYPES', 'doc, docx, xls, csv, txt, rtf, html, zip, jpg, jpeg, png, gif');

require_once str_replace('\\', '/', __DIR__) . '/handler.php';

?>

<?php if (frmd_message()): ?>
<link rel="stylesheet" href="<?=dirname($form_path)?>/formoid-default-red.css" type="text/css" />
<span class="alert alert-success"><?=FINISH_MESSAGE;?></span>
<?php else: ?>
<!-- Start Formoid form-->
<link rel="stylesheet" href="<?=dirname($form_path)?>/formoid-default-red.css" type="text/css" />
<script type="text/javascript" src="<?=dirname($form_path)?>/jquery.min.js"></script>
<form class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:480px;min-width:150px" method="post"><div class="title"><h2>My form</h2></div>
	<div class="element-select"  <?frmd_add_class("select")?>><label class="title">Hari</label><div class="small"><span><select name="select" >

		<option value="Senin ">Senin </option><br/>
		<option value="Selasa">Selasa</option><br/>
		<option value="Rabu">Rabu</option><br/>
		<option value="Kamis">Kamis</option><br/>
		<option value="Jumat">Jumat</option><br/>
		<option value="Sabtu">Sabtu</option><br/></select><i></i></span></div></div>
	<div class="element-input"  <?frmd_add_class("input")?>><label class="title">Waktu</label><input class="medium" type="text" name="input" /></div>
	<div class="element-input"  <?frmd_add_class("input1")?>><label class="title">Mata Kuliah</label><input class="medium" type="text" name="input1" /></div>
	<div class="element-input"  <?frmd_add_class("input3")?>><label class="title">Kelas</label><input class="small" type="text" name="input3" /></div>
	<div class="element-input"  <?frmd_add_class("input2")?>><label class="title">Pengajar</label><input class="medium" type="text" name="input2" /></div>

<div class="submit"><input type="submit" value="Submit"/></div></form>
<script type="text/javascript" src="<?=dirname($form_path)?>/formoid-default-red.js"></script>

<!-- Stop Formoid form-->
<?php endif; ?>

<?php frmd_end_form(); ?>