<?php
/**
 * email_notification
 * Class for email notification
 *
 * Copyright (C) 2014 d_frEak (eric.c13@gmail.com)
 * 2014/01/06
 *
 */

// be sure that this file not accessed directly
if (!defined('INDEX_AUTH')) {
  die("can not access this file directly");
} elseif (INDEX_AUTH != 1) {
  die("can not access this file directly");
}

require LIB.'phpmailer/class.phpmailer.php';
require SIMBIO.'simbio_GUI/table/simbio_table.inc.php';

class email_notification {
	/* Protected properties */
	protected $obj_db = false;
    /* Public properties */

	/**
	 * Class Constructor
	 *
	 */
	public function __construct($obj_db) {
	    $this->obj_db = $obj_db;
	}
	
	// function for procurement request notice 
	// division to librarian
	public function sendProcurementRequestNotice($procurement_id) {
		$subject = "";
		$message = "";
		$address = "";
		$address_name = "";
		
		// query for librarian
		$sql_str='SELECT * FROM user WHERE user_id=5';
		// run the query
		$user_q = $this->obj_db->query($sql_str);
		// check the is the procurement exist
		$user_d = $user_q->fetch_assoc();
		$address = $user_d['email'];
		$address_name = $user_d['realname'];	
		$subject = "Pengadaan Buku";
		$message = "Mohon kepada  ".$address_name.", agar  ".$_SESSION['division'];
		$message .= "segera melakukan pengadaan buku-buku yang ada di bawah ini. <br/>";
		
		// query for bibliography
		$sql_str='SELECT DISTINCT db.title, db.isbn,
			FROM procurement_request AS pr
			LEFT JOIN procurement AS p ON p.id=pr.procurement
			LEFT JOIN division_bibliography AS db ON db.id=pr.divisionbiblio
			LEFT JOIN biblio AS b ON b.isbn_issn=db.isbn
			WHERE p.status=3 OR p.status=2';
		// run the query
		$biblio_q = $this->obj_db->query($sql_str);
		
		// table
		$table = new simbio_table();
		$table->table_attr = 'border=1 align="center" style="width: 100%;" cellpadding="2" cellspacing="0"';
		$row=0;
		
		$table->appendTableRow(array('No', 'ISBN', 'Title'));
		$table->setCellAttr($row, 0, 'valign="top" style="width: 10%;"');
		$table->setCellAttr($row, 1, 'valign="top" style="width: 30%;"');
		$table->setCellAttr($row, 2, 'valign="top" style="width: 60%;"');
		$row++;
		
		while($biblio_d = $biblio_q->fetch_assoc())
		{
			$table->appendTableRow(array($row, $biblio_d['isbn'], $biblio_d['title']));
			$table->setCellAttr($row, 0, 'valign="top" style="width: 10%;"');
			$table->setCellAttr($row, 1, 'valign="top" style="width: 30%;"');
			$table->setCellAttr($row, 2, 'valign="top" style="width: 60%;"');
			$row++;
		}
		$message.=$table->printTable();
		$message.='<br/>Sekian dan terima kasih.';
		// send the email
		$this->sendNotice($subject, $message, $address, $address_name);
	}
	
	// function for procurement request notice 
	// librarian to division 
	public function sendProcurementConfirmationNotice($procurement_id) {
		$subject = "";
		$message = "";
		$address = "";
		$address_name = "";
		
		// query for librarian
		$sql_str='SELECT * FROM user WHERE user_id=5';
		// run the query
		$user_q = $this->obj_db->query($sql_str);
		// check the is the procurement exist
		$user_d = $user_q->fetch_assoc();
		$address = $user_d['email'];
		$address_name = $user_d['realname'];	
		$subject = "Periksa Buku";
		$message = "Diberitahukan kepada   ".$address_name." bahwa buku-buku dibawah ini sudah diperiksa";
		
			// query for bibliography
		$sql_str='SELECT DISTINCT db.title, db.isbn,
			FROM procurement_request AS pr
			LEFT JOIN procurement AS p ON p.id=pr.procurement
			LEFT JOIN division_bibliography AS db ON db.id=pr.divisionbiblio
			LEFT JOIN biblio AS b ON b.isbn_issn=db.isbn
			WHERE p.status=3 OR p.status=2';
		// run the query
		$biblio_q = $this->obj_db->query($sql_str);
		
		// table
		$table = new simbio_table();
		$table->table_attr = 'border=1 align="center" style="width: 100%;" cellpadding="2" cellspacing="0"';
		$row=0;
		
		$table->appendTableRow(array('No', 'ISBN', 'Title'));
		$table->setCellAttr($row, 0, 'valign="top" style="width: 10%;"');
		$table->setCellAttr($row, 1, 'valign="top" style="width: 30%;"');
		$table->setCellAttr($row, 2, 'valign="top" style="width: 60%;"');
		$row++;
		
		while($biblio_d = $biblio_q->fetch_assoc())
		{
			$table->appendTableRow(array($row, $biblio_d['isbn'], $biblio_d['title']));
			$table->setCellAttr($row, 0, 'valign="top" style="width: 10%;"');
			$table->setCellAttr($row, 1, 'valign="top" style="width: 30%;"');
			$table->setCellAttr($row, 2, 'valign="top" style="width: 60%;"');
			$row++;
		}
		$message.=$table->printTable();
		$message.='<br/>Sekian dan terima kasih.';
		
		// send the email
		$this->sendNotice($subject, $message, $address, $address_name);
	}
	
	// function for procurement request notice 
	// division to librarian confirmation
	public function sendProcurementReconfirmationNotice($procurement_id) {
		$subject = "";
		$message = "";
		$address = "";
		$address_name = "";
		
		// query for librarian
		$sql_str='SELECT * FROM user WHERE user_id=5';
		// run the query
		$user_q = $this->obj_db->query($sql_str);
		// check the is the procurement exist
		$user_d = $user_q->fetch_assoc();
		$address = $user_d['email'];
		$address_name = $user_d['realname'];	
		$subject = "Konfirmasi Buku";
		$message = "Diberitahukan kepada   ".$address_name." bahwa buku  ".$_SESSION['division']." sudah diperiksa. Terima Kasih.";
		// send the email
		$this->sendNotice($subject, $message, $address, $address_name);
	}
	
	// function for procurement allocation notice
	// librarian to finance
	public function sendProcurementAllocationNotice($procurement_id) {
	$subject = "";
		$message = "";
		$address = "";
		$address_name = "";
	// query for finance
		$sql_str='SELECT * FROM user WHERE user_id=3';
		// run the query
		$user_q = $this->obj_db->query($sql_str);
		// check the is the procurement exist
		$user_d = $user_q->fetch_assoc();
		$address = $user_d['email'];
		$address_name = $user_d['realname'];
		
		$subject = "Pengalokasian   ";
		
		$message = "Diberitahukan kepada  ".$address_name."agar ";
		$message .= "segera mengalokasikan buku ".$_SESSION['division']." Terima Kasih.";
		// send the email
		$this->sendNotice($subject, $message, $address, $address_name);	
	}
	
	// function for procurement approve notice
	// finance to librarian
	public function sendProcurementApproveNotice($procurement_id) {
	$subject = "";
		$message = "";
		$address = "";
		$address_name = "";
	// query for finance
		$sql_str='SELECT * FROM user WHERE user_id=3';
		// run the query
		$user_q = $this->obj_db->query($sql_str);
		// check the is the procurement exist
		$user_d = $user_q->fetch_assoc();
		$address = $user_d['email'];
		$address_name = $user_d['realname'];
		
		$subject = "Persetujuan   ";
		
		$message = "Diberitahukan kepada  ".$address_name."bahwa ";
		$message .= "bagian keuangan menyetujui pengalokasian buku ".$_SESSION['division']." Terima Kasih. ";
		// send the email
		$this->sendNotice($subject, $message, $address, $address_name);	
	}

	// function for procurement ordered notice
	// Division  to Supplier
	public function sendProcurementOrderedNotice($procurement_id) {
		$subject = "";
		$message = "";
		$address = "";
		$address_name = "";
//query for librarian to divsion
		$sql_str='SELECT * FROM user WHERE user_id=4';
	//	run the query
		$user_q = $this->obj_db->query($sql_str);
	//	check the is the procurement exist
		$user_d = $user_q->fetch_assoc();
		$address = $user_d['email'];
		$address_name = $user_d['realname'];
		
		$subject = "Pesan Buku";
		
		$message = "Diberitahukan kepada  ".$address_name."bahwa kami memesan  buku-buku berikut ini ";
		
				// query for bibliography
		$sql_str='SELECT DISTINCT b.title, b.isbn_issn,SUM(pod.quota)
			FROM procurement_order_detail AS pod
			LEFT JOIN procurement_order AS po ON pod.order=po.id
			LEFT JOIN biblio AS b ON pod.biblio=b.biblio_id
			GROUP BY pod.biblio';
		// run the query
		$biblio_q = $this->obj_db->query($sql_str);
		
		// table
		$table = new simbio_table();
		$table->table_attr = 'border=1 align="center" style="width: 100%;" cellpadding="2" cellspacing="0"';
		$row=0;
		
		$table->appendTableRow(array('No', 'ISBN', 'Title','Quota'));
		$table->setCellAttr($row, 0, 'valign="top" style="width: 10%;"');
		$table->setCellAttr($row, 1, 'valign="top" style="width: 30%;"');
		$table->setCellAttr($row, 2, 'valign="top" style="width: 60%;"');
		$row++;
		
		while($biblio_d = $biblio_q->fetch_assoc())
		{
			$table->appendTableRow(array($row, $biblio_d['isbn_issn'], $biblio_d['title'], $biblio_d['quota']));
			$table->setCellAttr($row, 0, 'valign="top" style="width: 10%;"');
			$table->setCellAttr($row, 1, 'valign="top" style="width: 30%;"');
			$table->setCellAttr($row, 2, 'valign="top" style="width: 60%;"');
			$row++;
		}
		$message.=$table->printTable();
		$message.='<br/>Sekian dan terima kasih.';
		
		//send the email
		$this->sendNotice($subject, $message, $address, $address_name);
	
	}
	
	// function for procurement pending notice
	// finance to librarian and division
	public function sendProcurementPendingNotice($procurement_id) {
		$subject = "";
		$message = "";
		$address = "";
		$address_name = "";
		$address_lib = "";
		$address_name_lib = "";
		
	// query for division and librarian 
		$sql_str='SELECT * FROM user WHERE user_id=4';
		$sql_str_lib='SELECT * FROM user WHERE user_id=5';
		// run the query
		$user_q = $this->obj_db->query($sql_str);
		$user_q_lib = $this->obj_db->query($sql_str_lib);
		// check the is the procurement exist
		$user_d = $user_q->fetch_assoc();
		$address = $user_d['email'];
		$address_name = $user_d['realname'];
		
		$user_d_lib = $user_q_lib->fetch_assoc();
		$address_lib = $user_d_lib['email'];
		$address_name_lib = $user_d_lib['realname'];
		
		$subject = "Tunda Persetujuan ";
		
		$message = "Diberitahukan kepada  ".$address_name." dan ".$address_name_lib."bahwa ";
		$message .= "dari pihak keuangan menunda persetujuan buku ".$_SESSION['division']." Terima Kasih.";
		// send the email
		$this->sendNotice($subject, $message, $address,$address_name);
		$this->sendNotice($subject, $message, $address_lib,$address_name_lib);
	
	}

	
	// function for procurement arrive notice
	// librarian to division
	public function sendProcurementArrivedNotice($procurement_id) {
		$subject = "";
		$message = "";
		$address = "";
		$address_name = "";
	// query for librarian to divsion
		$sql_str='SELECT * FROM user WHERE user_id=4';
		// run the query
		$user_q = $this->obj_db->query($sql_str);
		// check the is the procurement exist
		$user_d = $user_q->fetch_assoc();
		$address = $user_d['email'];
		$address_name = $user_d['realname'];
		
		$subject = "Buku-buku Sampai ";
		
		$message = "Diberitahukan kepada  ".$address_name."bagian divisi bahwa ";
		$message .= "buku yang di pesan sudah sampai. Sekian dan terima kasih.  ";
		// send the email
		$this->sendNotice($subject, $message, $address, $address_name);
	
	}
	

	// function for request to all suppliers
	// librarian to all supplier (spam)
	public function sendRequestSupplier() {
	
		$subject = "";
		$message = "";
	
		$subject = "Pengadaan Buku ";
		$message = "Kami ingin memesan buku berikut :<br/><br/>";
		
		// query for biblio
		$sql_str='SELECT DISTINCT b.title, b.isbn_issn
			FROM procurement_request AS pr
			LEFT JOIN procurement AS p ON p.id=pr.procurement
			LEFT JOIN division_bibliography AS db ON db.id=pr.divisionbiblio
			LEFT JOIN biblio AS b ON b.isbn_issn=db.isbn
			WHERE p.status=3 OR p.status=2';
		// run the query
		$biblio_q = $this->obj_db->query($sql_str);
		
		// table
		$table = new simbio_table();
		$table->table_attr = 'border=1 align="center" style="width: 100%;" cellpadding="2" cellspacing="0"';
		$row=0;
		
		$table->appendTableRow(array('No', 'ISBN', 'Title'));
		$table->setCellAttr($row, 0, 'valign="top" style="width: 10%;"');
		$table->setCellAttr($row, 1, 'valign="top" style="width: 30%;"');
		$table->setCellAttr($row, 2, 'valign="top" style="width: 60%;"');
		$row++;
		
		while($biblio_d = $biblio_q->fetch_assoc())
		{
			$table->appendTableRow(array($row, $biblio_d['isbn_issn'], $biblio_d['title']));
			$table->setCellAttr($row, 0, 'valign="top" style="width: 10%;"');
			$table->setCellAttr($row, 1, 'valign="top" style="width: 30%;"');
			$table->setCellAttr($row, 2, 'valign="top" style="width: 60%;"');
			$row++;
		}
		$message.=$table->printTable();
		$message.='<br/>Mohon membalas email ini dengan harga serta jumlah buku yang dimiliki.';
		$message.='<br/>Terima kasih.';
		
		// query for supplier
		$sql_str='SELECT e_mail, supplier_name FROM mst_supplier';
		// run the query
		$supplier_q = $this->obj_db->query($sql_str);
		// check the is the procurement exist
		while($supplier_d = $supplier_q->fetch_assoc())
		{
			// send the email
			$this->sendNotice($subject, $message, $supplier_d['e_mail'], $supplier_d['supplier_name']);
		}
	}
	
		
	
	// function template notice for email
	public function sendNotice($subject, $message, $address, $address_name)
	{
        global $sysconf;
        if (!class_exists('PHPMailer')) {
			// utility::jsAlert('PHPMailer not found'); 
            return false;
        }

        $_mail = new PHPMailer(false);
        $_mail->IsSMTP(); // telling the class to use SMTP

        // get message template
        $_msg_tpl = @file_get_contents(SB.'admin/admin_template/overdue-mail-tpl.html');

        // date
        $_curr_date = date('Y-m-d H:i:s');
		
		// e-mail setting
        // $_mail->SMTPDebug = 2;
        $_mail->SMTPAuth = $sysconf['mail']['auth_enable'];
        $_mail->Host = $sysconf['mail']['server'];
        $_mail->Port = $sysconf['mail']['server_port'];
        $_mail->Username = $sysconf['mail']['auth_username'];
        $_mail->Password = $sysconf['mail']['auth_password'];
        $_mail->SetFrom($sysconf['mail']['from'], $sysconf['mail']['from_name']);
        $_mail->AddReplyTo($sysconf['mail']['reply_to'], $sysconf['mail']['reply_to_name']);
        $_mail->AddAddress($address, $address_name);
        $_mail->Subject = $subject;
        // $_mail->AltBody = strip_tags($_message);
        $_mail->MsgHTML($message);
		
        $_sent = $_mail->Send();
        if (!$_sent) {
			// utility::jsAlert('Send failed'); 
            return array('status' => 'ERROR', 'message' => $_mail->ErrorInfo);
            utility::writeLogs($this->obj_db, 'staff', isset($_SESSION['uid'])?$_SESSION['uid']:'1', 'email', 'FAILED to send overdue notification e-mail to '.$address.' ('.$_mail->ErrorInfo.')');
        } else {
			// utility::jsAlert('Send  success'); 
            return array('status' => 'SENT', 'message' => 'Overdue notification E-Mail have been sent to '.$address);
            utility::writeLogs($this->obj_db, 'staff', isset($_SESSION['uid'])?$_SESSION['uid']:'1', 'email', 'Overdue notification e-mail sent to '.$address);
        }
	}
}
