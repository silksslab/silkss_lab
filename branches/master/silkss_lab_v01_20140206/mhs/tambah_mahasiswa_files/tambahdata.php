<?php
	include("koneksidb.php");

	if($_POST['btnsimpan'] == "Simpan")
	{
		$nim = $_POST['nim'];
		$nama = $_POST['nama'];
		$tgl = $_POST['tgl']; 
		$bln = $_POST['bln']; 
		$thn = $_POST['thn'];
		$status = $_POST['status'];
		
		$tgllahir = $thn."-".$bln."-".$tgl;
		
		if($nim != "" and $nama != "" and $tgl != "" and $bln != "" and $thn != "")
		{
			$sql = "INSERT INTO siswa VALUES('$nim','$nama','$tgllahir','$status')";
			mysql_query($sql);

			if(mysql_errno() == 0)
			{
				echo"<script>alert('Data berhasil disimpan !');</script>";
			}else{
				echo"<script>alert('Data gagal disimpan !');</script>";
			}
		}
		else{
			echo"<script>alert('Data harus diisi lengkap !');</script>";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tambah Data Siswa</title>
</head>
<body>
<table align="center">
<tr>
	<td>
        <fieldset>
        <legend><strong>[ Tambah Data Siswa ]</strong></legend>
        <form action="" method="post">
        <table>
        	<tr>
            	<td>Nim</td>
                <td>
                	<input type="text" name="nim" />
                </td>
            </tr>
            <tr>
            	<td>Nama</td>
                <td>
                	<input type="text" name="nama" />
                </td>
            </tr>
            <tr>
            	<td>Tgl Lahir</td>
                <td>
                	<select name="tgl">
                    	<option value="">- Pilih Tgl -</option>
                        <?php
							for($t=1;$t<=31;$t++)
							{
								$val_t = str_pad($t,2,'0',STR_PAD_LEFT);
								echo"<option value='$val_t'>$val_t</option>";
							}
						?>
                    </select>
                    &nbsp;
                	<select name="bln">
                    	<option value="">- Pilih Bulan -</option>
						<?php
                            $arrbulan = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','November','Desember');
                            for($b=0;$b<11;$b++)
                            {
                                $val_b = str_pad($b+1,2,'0',STR_PAD_LEFT);
                                echo"<option value='$val_b'>$arrbulan[$b]</option>";
                            }
                        ?>
                    </select>
                    &nbsp;
                    <select name="thn">
                    	<option value="">- Pilih Tahun -</option>
                        <?php
							$thn_skrng = date('Y');
							$thn_mulai = $thn_skrng - 30;  
							$thn_akhir = $thn_skrng - 5;
							
							for($th=$thn_mulai;$th<=$thn_akhir;$th++)
							{
								echo"<option value='$th'>$th</option>";
							}
						?>
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Status</td>
                <td>
                	<input type="radio" name="status" value="aktif" checked="checked"/> Aktif
                    <input type="radio" name="status" value="nonaktif" /> Non Aktif
                </td>
            </tr>
            <tr>
            	<td colspan="2" align="right">
                	<input type="submit" name="btnsimpan" value="Simpan" />
                </td>
            </tr>
        </table>
        </form>        
        </fieldset>
	</td>
</tr>
</table>
</body>
<a href="tampildata.php"> Lihat data </> <hr>

</html>