<?php
	include("koneksidb.php");
	include("fungsi.php");
	
	if($_POST['btnaction'] == "Edit")
	{
		$nim = $_POST['nim'];
		$page = "editdata.php?nim=".$nim;
		echo redirectPage($page);
	}
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>PHP Dasar 3 - Koneksi Database MySQL dan Menampilkan Data</title>
    <script type="text/javascript">
	function konfirmasiHapus(nim,nama)
	{
		var nim = nim;
		var nama = nama;
		var jawab;
		
		jawab = confirm("Apakah data '"+nama+"' akan dihapus ?")
		if(jawab)
		{
			window.location = "hapusdata.php?nim="+nim;
			return false;
		}else{
			alert("Penghapusan data dibatalkan");
		}
	}
	</script>
</head>
<body>

<!-- ### tAMPILKAN DATANYA ### -->
<table border="1" align="center">
	<tr>
    	<th>Nim</th>
        <th>Nama</th>
        <th>Tgl Lahir</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
<?php
	$sql = "SELECT nim, nama, tgllahir, status FROM siswa";
	$hasil = mysql_query($sql);
	if(mysql_num_rows($hasil) > 0)
	{
		while($data = mysql_fetch_array($hasil))
		{
			echo"<tr>";
				echo"<td>".$data['nim']."</td>";
				echo"<td>".$data['nama']."</td>";
				echo"<td>".$data['tgllahir']."</td>";
				echo"<td>".$data['status']."</td>";
				echo"<td>";
					echo"<form action='' method='post'>";
					echo"<input type='hidden' name='nim' value='".$data['nim']."'>";
					echo"<input type='submit' name='btnaction' value='Edit'>";
					echo"<input type='submit' name='btnaction' onclick=\"return konfirmasiHapus('".$data['nim']."','".$data['nama']."');\" value='Hapus'>";
					echo"</form>";
				echo"</td>";
			echo"</tr>";
		}
	}else{
		echo"<tr>";
				echo"<td>Data Belum Ada</td>";
		echo"</tr>";
	}
?>
</table>

<a href="tambahdata.php"> Tambah data baru </> <hr>
</body>
</html>