<?php

define('EMAIL_FOR_REPORTS', 'yandripono@gmail.com');
define('RECAPTCHA_PRIVATE_KEY', '@privatekey@');
define('FINISH_URI', 'http://');
define('FINISH_ACTION', 'message');
define('FINISH_MESSAGE', 'Thanks for filling out my form!');
define('UPLOAD_ALLOWED_FILE_TYPES', 'doc, docx, xls, csv, txt, rtf, html, zip, jpg, jpeg, png, gif');

require_once str_replace('\\', '/', __DIR__) . '/handler.php';

?>

<?php if (frmd_message()): ?>
<link rel="stylesheet" href="<?=dirname($form_path)?>/formoid-default-red.css" type="text/css" />
<span class="alert alert-success"><?=FINISH_MESSAGE;?></span>
<?php else: ?>
<!-- Start Formoid form-->
<link rel="stylesheet" href="<?=dirname($form_path)?>/formoid-default-red.css" type="text/css" />
<script type="text/javascript" src="<?=dirname($form_path)?>/jquery.min.js"></script>
<form action="add_matakuliah.php" class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:880px;min-width:150px" method="post">
<table border="0" width="100%">
			<div class="title"><h2>Menambah Data Matakuliah</h2></div>
			<tr>
			<td>
				<div class="element-input" ><label class="title">Nama Matakuliah<span class="required">*</span></label>
			</td>
			<td>
					<input class="medium" type="text" name="namamk" required="required"/></div>
			</td>
			</tr>
			<tr>
			<td>
				<div class="element-input" ><label class="title">Kode Matakuliah<span class="required">*</span></label>
			</td>
			<td>
					<input class="medium" type="text" name="kdmk" required="required"/></div>
			</td>
			</tr>
			
			<tr>
			<td>
				<div class="element-select" ><label class="title">Hari<span class="required">*</span></label>
			</td>
			<td>
				<div class="element-select" >
				<div style="width:130px;"><span>
				<select name="harimk" required="required">
					<option value="Senin">Senin</option><br/>
					<option value="Selasa">Selasa</option><br/>
					<option value="Rabu">Rabu</option><br/>
					<option value="Kamis">Kamis</option><br/>
					<option value="Jumat">Jumat</option><br/>
				</select><i></i></span>
				</div>
				</div>
				</div>
			</td>
			</tr>
			
			<tr>
			<td>
			<div class="element-select" ><label class="title">Jam<span class="required">*</span></label>
			</td>
			<td>
				<div class="element-select" >
				<div style="width:130px;">
				<span><i> Jam</i>
				<select class="jam" name="jammk" required="required">

					<option value="01">01</option><br/>
					<option value="02">02</option><br/>
					<option value="03">03</option><br/>
					<option value="04">04</option><br/>
					<option value="05">05</option><br/>
					<option value="06">06</option><br/>
					<option value="07">07</option><br/>
					<option value="08">08</option><br/>
					<option value="09">09</option><br/>
					<option value="10">10</option><br/>
					<option value="11">11</option><br/>
					<option value="12">12</option><br/>
					<option value="13">13</option><br/>
					<option value="14">14</option><br/>
					<option value="15">15</option><br/>
					<option value="16">16</option><br/>
					<option value="17">17</option><br/>
					<option value="18">18</option><br/>
					<option value="19">19</option><br/>
					<option value="20">20</option><br/>
					<option value="21">21</option><br/>
					<option value="22">22</option><br/>
					<option value="23">23</option><br/>
					<option value="24">24</option>
				</select>
				</span>
				</div>
				</div>
				</div>
			
			
			
				<div class="element-select" >
				<div style="width:130px;"><span><i> Menit</i>
				<select name="menitmk" required="required"> 
					
					<option value="01">01</option><br/>
					<option value="02">02</option><br/>
					<option value="03">03</option><br/>
					<option value="04">04</option><br/>
					<option value="05">05</option><br/>
					<option value="06">06</option><br/>
					<option value="07">07</option><br/>
					<option value="08">08</option><br/>
					<option value="09">09</option><br/>
					<option value="10">10</option><br/>
					<option value="11">11</option><br/>
					<option value="12">12</option><br/>
					<option value="13">13</option><br/>
					<option value="14">14</option><br/>
					<option value="15">15</option><br/>
					<option value="16">16</option><br/>
					<option value="17">17</option><br/>
					<option value="18">18</option><br/>
					<option value="19">19</option><br/>
					<option value="20">20</option><br/>
					<option value="21">21</option><br/>
					<option value="22">22</option><br/>
					<option value="23">23</option><br/>
					<option value="24">24</option><br/>
					<option value="25">25</option><br/>
					<option value="26">26</option><br/>
					<option value="27">27</option><br/>
					<option value="28">28</option><br/>
					<option value="29">29</option><br/>
					<option value="30">30</option><br/>
					<option value="31">31</option><br/>
					<option value="32">32</option><br/>
					<option value="33">33</option><br/>
					<option value="34">34</option><br/>
					<option value="35">35</option><br/>
					<option value="36">36</option><br/>
					<option value="37">37</option><br/>
					<option value="38">38</option><br/>
					<option value="39">39</option><br/>
					<option value="40">40</option><br/>
					<option value="41">41</option><br/>
					<option value="42">42</option><br/>
					<option value="43">43</option><br/>
					<option value="44">44</option><br/>
					<option value="45">45</option><br/>
					<option value="46">46</option><br/>
					<option value="47">47</option><br/>
					<option value="48">48</option><br/>
					<option value="49">49</option><br/>
					<option value="50">50</option><br/>
					<option value="51">51</option><br/>
					<option value="52">52</option><br/>
					<option value="53">53</option><br/>
					<option value="54">54</option><br/>
					<option value="55">55</option><br/>
					<option value="56">56</option><br/>
					<option value="57">57</option><br/>
					<option value="58">58</option><br/>
					<option value="59">59</option><br/>
				</select><i></i></span>
				</div>
				</div>
				</div>
			</td>
			</tr>
			
			<tr>
			<td>
			</td>
			<td>			
			<div class="submit">
				<input type="submit" value="Simpan"/>
			</div>
			</td>
			</tr>
</table>
</form>
<script type="text/javascript" src="<?=dirname($form_path)?>/formoid-default-red.js"></script>

<!-- Stop Formoid form-->
<?php endif; ?>

<?php frmd_end_form(); ?>