<?php

define('EMAIL_FOR_REPORTS', 'yandripono@gmail.com');
define('RECAPTCHA_PRIVATE_KEY', '@privatekey@');
define('FINISH_URI', 'http://');
define('FINISH_ACTION', 'message');
define('FINISH_MESSAGE', 'Thanks for filling out my form!');
define('UPLOAD_ALLOWED_FILE_TYPES', 'doc, docx, xls, csv, txt, rtf, html, zip, jpg, jpeg, png, gif');

require_once str_replace('\\', '/', __DIR__) . '/handler.php';

?>

<?php if (frmd_message()): ?>
<link rel="stylesheet" href="<?=dirname($form_path)?>/formoid-default-red.css" type="text/css" />
<span class="alert alert-success"><?=FINISH_MESSAGE;?></span>
<?php else: ?>
<!-- Start Formoid form-->
<link rel="stylesheet" href="<?=dirname($form_path)?>/formoid-default-red.css" type="text/css" />
<script type="text/javascript" src="<?=dirname($form_path)?>/jquery.min.js"></script>
<form action="add_tutor.php" class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:880px;min-width:150px" method="post"><div class="title">
<table width="100%">
<h2><center>Menambah Data Tutor</center></h2></div>
<tr>
  <td>
	<div class="element-input" ><label class="title">NIP<span class="required">*</span></label>
  </td>
  <td>
	<input class="small" type="text" name="nip" required="required"/>
	</div>
  </td>
</tr>
<tr>
	<td>
	<div class="element-input" ><label class="title">Nama<span class="required">*</span></label>
	</td>
	<td>
	<input class="medium" type="text" name="nama" required="required"/>
	</div>
	</td>
</tr>
<tr>
	<td>
	<div class="element-input" ><label class="title">Username</label>
	</td>
	<td>
	<input class="small" type="text" name="username" />
	</div>
	</td>
</tr>
<tr>
	<td>
	<div class="element-input" ><label class="title">Password</label>
	</td>
	<td>
	<input class="small" type="password" name="password" />
	</div>
	</td>
</tr>
<tr>
	<td>
	<div class="element-select" ><label class="title">Prodi<span class="required">*</span></label>
	</td>
	<td>
	<div class="small"><span><div class="element-select" >
	<select name="prodi" required="required">
		<option value="Fisika">Fisika</option><br/>
		<option value="TIK">TIK</option><br/>
		<option value="Matematika">Matematika</option><br/>
		<option value="Kimia">Kimia</option><br/>
	</select><i></i></span>
	</div>
	</div>
	</div>
	</td>
</tr>
<tr>
	<td>
	<div class="element-email" ><label class="title">Email</label>
	</td>
	<td>
	<input class="medium" type="email" name="email" value="" /></div>
	</td>
</tr>

<tr>
	<td>
	<div class="element-radio" ><label class="title">Jenis Kelamin</label>	
		</td>
		<td>
		<div class="column column1">
		<div class="element-radio" >
		<div class="column column1">
		<input type="radio" name="jk" value="L" /><span>L</span><br/>
		<input type="radio" name="jk" value="P" /><span>P</span><br/>
		</div>
		</div><span class="clearfix"></span>
		</div>
		</td>
	</div>
</tr>
<tr>
<td>


</td>
<td>
<div class="submit"><input type="submit" value="Simpan"/></div>
</td>
</table>
</form>
<script type="text/javascript" src="<?=dirname($form_path)?>/formoid-default-red.js"></script>

<!-- Stop Formoid form-->
<?php endif; ?>

<?php frmd_end_form(); ?>