<?php include "con_peng.php";?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>Statistik pengunjung GRAFIK</title>
<link rel="stylesheet" href="style.css"/>

<!-- Load Liblary Pendukung Plugin gvChart -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript" src="lib/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="lib/jquery.gvChart-1.0.1.min.js"></script>
<script type="text/javascript">
	gvChartInit();
	jQuery(document).ready(function(){
		jQuery('#table').gvChart({
			chartType: 'AreaChart',
			gvSettings: {
				vAxis: {title: 'Jumlah'}, // Membentuk title verikal
				hAxis: {title: 'Bulan'},  // Membentuk title horizontal
				width: 800,
				height: 300,
			}
		});
	});
</script>
</head>
<body>
<div>
<h2>Statistik pengunjung dengan GRAFIK</h2>
	<table id='table'>
        <caption>Statistik Pengunjung</caption>
        <thead>
            <tr>
                <th>Tanggal</th>
                <th><?=tanggal_indonesia(date("Y-m-d",strtotime("-6 day")))?></th>
                <th><?=tanggal_indonesia(date("Y-m-d",strtotime("-5 day")))?></th>
                <th><?=tanggal_indonesia(date("Y-m-d",strtotime("-4 day")))?></th>
                <th><?=tanggal_indonesia(date("Y-m-d",strtotime("-3 day")))?></th>
                <th><?=tanggal_indonesia(date("Y-m-d",strtotime("-2 day")))?></th>
                <th><?=tanggal_indonesia(date("Y-m-d",strtotime("-1 day")))?></th>
                <th><?=tanggal_indonesia(date("Y-m-d",strtotime("now")))?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Pengunjung</th>
				<td><?=get_online(date("Y-m-d", strtotime("-6 day")))?></td>
				<td><?=get_online(date("Y-m-d", strtotime("-5 day")))?></td>
				<td><?=get_online(date("Y-m-d", strtotime("-4 day")))?></td>
				<td><?=get_online(date("Y-m-d", strtotime("-3 day")))?></td>
				<td><?=get_online(date("Y-m-d", strtotime("-2 day")))?></td>
				<td><?=get_online(date("Y-m-d", strtotime("-1 day")))?></td>
				<td><?=get_online(date("Y-m-d", strtotime("now")))?></td>
            </tr>
        </tbody>
    </table>
</div>
</body>
</html>