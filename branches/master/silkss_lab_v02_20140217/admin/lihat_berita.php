<?php
include "koneksi.php";
@session_start();
$username= $_SESSION['username'];
if ($username){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="../base.css" />
<link rel="stylesheet" type="text/css" href="../content.css" />
<link rel="stylesheet" type="text/css" href="../nav.css" /><title>Lihat Berita | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>
<script type="text/javascript" src="../scrollup/jquery-1.4.min.js">
		</script>
		<script type="text/javascript" src="../scrollup/jquery.pjScrollUp.min.js">
		</script>
		<script>
		$(function() {
			$(document).pjScrollUp({
				imgSrc: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABc5JREFUeNq8mWtIZVUUx7fX59Uan2OKesehJiVUJCYiqA/JfIrQCSJoxhkIwQ9+6NvMfJqhIYMmiFBuE81A+IjUGSSVEiVNFJEsJlPSfIzm+62j5hMfu/XfnnXndLvqOcfHhr/3eM9+/Fz7tdbSS0opLJQA0iukV0lJpDOkSFKI9n6RNEMaInWSftW0YnYgL5OA50jvk94hpZBs/GJ7e1tsbW2pZx8fH+Ht7a1vJzXQClIJqeuoAWNIH5KySOH4YnZ2VoyMjIjJyUkxPz8vVldXxcbGhqrs5+cnAgMDRVhYmIiKihKxsbEiMjJS6KxbSPqCNGgO0Mvr/zWkfI9+fkJ6AXW7u7tFR0eHSM/KEsM0MErg2truJ0GirBKc+rTb1adjdFRU3L8vUlJSRGJiIlsX03+LxizyMKb+WT6VNhdy93sf0h2plYGBAVlYWCi7ExKUVgID0Y0hoe6ww6HafZefL/v6+qSu5JPsbmO75BlQShpdFqP15uamrKurk7MREWoAo1B7CaBQTU2NpCXBkN+TQowBSulL+hatlpeXZUlJiXyUmmrKYgdpNShI9VlUVCQXFxcZskIZxgDgHYYrKChQHR0VmLswI5XXr+shv9ofUMp3eVrZcscFx8IYWNvr6+sM+cFegFGkQdSor68/ETg9ZHV1NQNOkuI9AX6Gt4ODg2pDWFlzvAGwxsyuSbTr7e1lyK+Zi2+C50nZuA0aGxvFbHi460wzWn5PTRV/OJ3icUGBOh/XgoIMt7Wv7N6A7Q8eCFpeeMzUrlCXBW8Bmw5hS0cJpqi8vFzSVSd5iaAfs5ZEm/b2drbi5zzFdlIHvikrKzMNyHDYWPpiBRLTXFxcLHd2dtDFY9KzAHyDtEN3q+rMzNrbC84qJNebmJjgLt4C4DU8tbW1mbLeQXBWIVG3tbWVm39s48U4NTVlakMM3rwp0tPTlWu1X0lLSxNjd++a2jhkQX5MAqADT3Nzcy6vxCocXK+hoaFDQYIB7pvmZTkAGEmLUvlzh4GbmZkRpaWlSsPDw5YhcbytESSOPCphIB2DV+F0OtUBbWXNTU9Py0cXLrgO6rnTpyVZ0tKaBENeXp4kSFSfsOn9QquWG7l0SQSRdx1HloNWyFF95vz5Q1mSeQC4iEH9/f1dnrAVuISeHtf3h4X09fXlsdYBOGOz2VQM4V5GHA4xevu2yMjIMAxnBnLq3j0VFughYaSAgAAe7wkAVWsEOBxD6Au+d4vQDoQzCumpgCE0NJRCFRUfjQDwTzwh+nIvEXT0nLl8WTQ3N5uG8wSpP4IaGhrEc9nZ6lhhZ4GLjqULi/FN3onYWe67C1cfdl1LS4ukw1y2XLxoyaHA7h6Nj1fuXFNTk8edzFft+Pg4b/p0AFIt2YXf4EV7GhyN0Hjs7Nl9j6KDhLYM4emYwR8B71pzFv5GIGXT0hHlsCfi1r0OT5QQug7DKWC3Wrgt1pf7tPL6S0pK4vX3A2mB/cFE0j84hBG34i85KXdfb72anBwORXFKv+zu8ufhTX9/vyW3/bBhKMbs6uritVfgKSaJw7WHt7W1tSceNFVVVTHcLOncXmFnJmrAzAiqTwISm/Jhbq4kZ4UBcw4K3J2otbS0pILq4w7cf87MlOReMVyhkcyCXcuVqIgf2x6QR7kmOfUBy+ngfiKdMpo8CtZyJSriR1DNrtRR7FaV8qis1E8r4CKNZ7eeWvJL7qGnp0dW3bihOje7y3mXoi2Oks7OTr2L+I3Lch4AjSQwkVX9iBSLFC91rhKYV65eFT0JCS433T3Qh1fCzkcc3dsPyb1KTk5WBzEysAiDSLk0pnO/BKbRFDAyD9e0iD+IAxt4KPhcWFhQbrqWFVCukp3ggoODRXR0tHCQ2xYTE8N9rZPKSJ+Suo86iZ5KukJ6m/Si3vuFdRFH4BmAkNd/Z2SA9COpmPTbcWX5uZwivU56jfQSKZ4Uqv17AgXZ9CdaHvov0i+kJnW3miz/CjAAph4RsgkK5+gAAAAASUVORK5CYII="
			});
		});
		</script>
<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="../common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>

<div id="siteNav">
	<ul>
	<li id="active"><a href="index.php" >BERANDA</a></li>
	<li><a href="penggunaan_lab.php" class="daddy">Penggunaan Lab</a>

	<ul class="other-section">
				<li><a >Jadwal</a>
							<ul class="other-section">
							<li><a href="buat_jadwal.php" class="no-ch">Buat Jadwal</a></li>
							<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
							
							</ul>
				</li>
				
				<li><a  class="daddy">Ruangan</a>
							<ul class="other-section">
							<li><a href="tambah_ruangan.php" class="no-ch">Tambah Ruangan</a></li>
							<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
							
							</ul>
				</li>
				<li><a >Komputer</a>
							<ul class="other-section">
							<li><a href="tambah_komputer.php" class="no-ch">Tambah Komputer</a></li>
							<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
							
							</ul>
				</li>
				
				
	</ul>
	</li>

	<li><a href="kinerja.php" class="daddy">Kinerja</a>
				<ul class="other-section">
				<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
				<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
				<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
				<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
				</ul>
				</li>
				
	<li><a href="kebutuhan.php" class="daddy">Kebutuhan</a>
	<ul class="other-section">
					<li><a  class="daddy">Mahasiswa</a>
					<ul class="other-section">
							<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
							<li><a href="lihat_mahasiswa_2.php" class="no-ch">Lihat Mahasiswa</a></li>
							<li><a href="formulir_pemakaian.php" class="no-ch">Formulir Pakai Komputer</a></li>
							<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pesan Komputer</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan Kom.
							</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemakaian Kom.</a></li>
							
					</ul>
					</li>
					
					<li><a class="daddy">Dosen</a>
					<ul class="other-section">
							<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
							<li><a href="lihat_dosen.php" class="no-ch">Lihat Dosen</a></li>
							
							<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Pinjam Ruang</a></li>
							<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a  class="daddy">Tutor</a>
					<ul class="other-section">
							<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
							<li><a href="lihat_tutor.php" class="no-ch">Lihat Tutor</a></li>
							
							<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Pinjam Ruang </a></li>
							<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a  class="daddy">Matakuliah</a>
					<ul class="other-section">
							<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
							<li><a href="lihat_matakuliah.php" class="no-ch">Lihat Matakuliah</a></li>
							
					</ul>
					</li>
	</ul>
	</li>

	<li><a href="info_lab.php" class="active daddy main-node">Info lab</a>
			<ul class="other-section">
					<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
					<li><a href="lihat_berita.php" class="active daddy main-node" >Lihat Berita</a></li>
			</ul>
	</li>

	<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a>
			<ul class="other-section">
					<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
					<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
			</ul>
	</li>

	</div>
	<div id='topPagination'>
		<div class="pagination noprt">
			<?php
			echo"Selamat Datang &nbsp; ' <i>".$username." '</i> <a href='../login/logout.php'><u>Keluar</u></a>";
			?>
		<span> </span></a>
		</div>
	</div>
<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Lihat Berita</h1>


<!-- START lihat berita info lab-->
									
<div id="tengah">

<?php
include("info_lab_koneksi.php");
$qry=mysql_query("select * from berita_situs order by Id_Berita Desc limit 5"); 
$no=1;
while ($hasil=mysql_fetch_array($qry))
{
$no++;
if ($no%2==0)
{
?>

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:white; border:1px solid #F3E6E6;border-radius:5px 0px 0px 5px;-moz-border-radius:5px 0px 0px 5px;" >
	 <tr>
			<td align="left">
                <a href="ubah_berita.php?id=<?php echo $hasil['Id_Berita'] ?>"><img alt="edit" title="Edit" src="icon/edit.png" /></a>
                &nbsp;&nbsp; 
                <a href="hapus_berita.php?id=<?php echo $hasil['Id_Berita'] ?>" onclick="return confirm('Anda yakin akan menghapus data?')"><img alt="hapus" title="Hapus" src="icon/hapus.png" /></a>
			</td>
	</tr>
     <tr>
		<td align="left" height="50px">
				<div style="margin:-10px 0px;background:LightGray ;border-radius:2px 0px 0px 5px;-moz-border-radius:0px 2px 5px 0px; width:100%; color:black; font-weight:bold;font-size:15px; text-transform:capitalize;" >
					<img src="gbr/prof4.gif" width="30" height="30" style="border:none" /> <? echo " $hasil[Judul] "; ?>
					
				</div>
		</td>
	</tr>
	
	<tr>
		<td align="left" height="0px">
				<div style="margin:-12px 0px;background:LightGray ;border-radius:2px 0px 0px 5px;-moz-border-radius:0px 2px 5px 0px; width:100%; color:black; font-size:15px; text-transform:capitalize;" >
					 <? echo " <font color='#CD5C5C' face=arial, Helvetica, sans-serif size=2> oleh - $hasil[Nama] </font>"; ?>
					<? 
						echo "<font color='#556B2F' face=arial, Helvetica, sans-serif size=1> - Tanggal muat berita : $hasil[Tanggal_Muat]</font>";
					?>
				</div>
		</td>
	</tr>
            
    <tr>
        <td>
			<? 
				echo"<font face=arial, Helvetica, sans-serif size=2></br> $hasil[Isi_Berita]</font>";
			?>
		</td>
	</tr>
    <tr>
		<td>
		<br/>
			<? 
				// echo "<font color='#999' face=arial, Helvetica, sans-serif size=1>Tanggal Muat Berita : </font>";
				// $tanggal=$hasil['Tanggal_Muat'];
				// $tgl=substr($tanggal,8,2);
				// $bln=substr($tanggal,5,2);
				// $thn=substr($tanggal,0,4);				
				
				// echo "  <font color='#999' face=arial, Helvetica, sans-serif size=1>
							// $tgl-$bln-$thn 
						// </font>";
			?>			
		</td>
    </tr>
</table>
	<br>
	
<?php }
else
{
?>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:white; border:1px solid #F3E6E6;border-radius:5px 0px 0px 5px;-moz-border-radius:5px 0px 0px 5px;" >
    <tr>
			<td align="left">
                <a href="ubah_berita.php?id=<?php echo $hasil['Id_Berita'] ?>"><img alt="edit" title="Edit" src="icon/edit.png" /></a> 
                &nbsp;&nbsp; 
                <a href="hapus_berita.php?id=<?php echo $hasil['Id_Berita'] ?>" onclick="return confirm('Anda yakin akan menghapus data?')"><img alt="hapus" title="Hapus" src="icon/hapus.png" /></a>
			</td>
	</tr>
	<tr align="<?php echo $align; ?>">
		<td align="left" height="50px">
				<div style="margin:-10px 0px;background:LightGray ;border-radius:2px 0px 0px 5px;-moz-border-radius:0px 2px 5px 0px; width:100%; color:black; font-weight:bold;font-size:15px; text-transform:capitalize;" >
					<img src="gbr/prof4.gif" width="30" height="30" style="border:none" /> <? echo " $hasil[Judul] "; ?>
					
				</div>
		</td>
	</tr>
	
	<tr>
		<td align="left" height="0px">
				<div style="margin:-12px 0px;background:LightGray ;border-radius:2px 0px 0px 5px;-moz-border-radius:0px 2px 5px 0px; width:100%; color:black; font-size:15px; text-transform:capitalize;" >
					 <? echo " <font color='#CD5C5C' face=arial, Helvetica, sans-serif size=2> oleh - $hasil[Nama] </font>"; ?>
					<? 
						echo "<font color='#556B2F' face=arial, Helvetica, sans-serif size=1> - Tanggal muat berita : $hasil[Tanggal_Muat]</font>";
					?>
				</div>
		</td>
	</tr>
            
    <tr>
        <td >
                <? 
					echo"<font face=arial, Helvetica, sans-serif size=2></br> $hasil[Isi_Berita]</font>";
				?>		
		</td>
	</tr>
    <tr>
        <td>
		<br/>
                <? 
				// echo "<font color='#999' face=arial, Helvetica, sans-serif size=1>Tanggal Muat Berita : </font>";
					// $tanggal=$hasil['Tanggal_Muat'];
					// $tgl=substr($tanggal,8,2);
					// $bln=substr($tanggal,5,2);
					// $thn=substr($tanggal,0,4);				
				// echo "<font color='#999' face=arial, Helvetica, sans-serif size=1> $tgl-$bln-$thn <br/> </font>";
				?>
                
		</td>
    </tr>
    </table>
	<br/>
	<?php }
}
?>
</div>
		
				<!-- End lihat berita info lab -->

</div>
</div>
</div>
</body>
<div id="bottomPagination">Copy Right Labkom STKIP Surya 2013/2014
</div>
</html>

<?php
}
	else {
		header("location:../index.php");
	}
?>
