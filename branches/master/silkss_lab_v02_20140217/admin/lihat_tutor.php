<?php
include "koneksi.php";
@session_start();
$username= $_SESSION['username'];
if ($username){
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="../base.css" />
<link rel="stylesheet" type="text/css" href="../content.css" />
<link rel="stylesheet" type="text/css" href="../nav.css" />
<title>Lihat Tutor | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="../common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>
<div id="siteNav">
	<ul>
	<li id="active"><a href="index.php">BERANDA</a></li>
	<li><a href="penggunaan_lab.php" class="daddy">Penggunaan Lab</a>

	<ul class="other-section">
				<li><a >Jadwal</a>
							<ul class="other-section">
							<li><a href="buat_jadwal.php" class="no-ch">Buat Jadwal</a></li>
							<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
							
							</ul>
				</li>
				
				<li><a  class="daddy">Ruangan</a>
							<ul class="other-section">
							<li><a href="tambah_ruangan.php" class="no-ch">Tambah Ruangan</a></li>
							<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
							
							</ul>
				</li>
				<li><a >Komputer</a>
							<ul class="other-section">
							<li><a href="tambah_komputer.php" class="no-ch">Tambah Komputer</a></li>
							<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
							
							</ul>
				</li>
				
				
	</ul>
	</li>

	<li><a href="kinerja.php" class="daddy">Kinerja</a>
				<ul class="other-section">
				<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
				<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
				<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
				<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
				</ul>
				</li>
				
	<li><a href="kebutuhan.php"  class="active daddy main-node">Kebutuhan</a>
	<ul class="other-section">
					<li><a >Mahasiswa</a>
					<ul class="other-section">
							<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
							<li><a href="lihat_mahasiswa_2.php"  class="active daddy main-node">Lihat Mahasiswa</a></li>
							<li><a href="formulir_pemakaian.php" class="no-ch">Formulir Pakai Komputer</a></li>
							<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pesan Komputer</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan Kom.
							</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemakaian Kom.</a></li>
							
					</ul>
					</li>
					
					<li><a class="daddy">Dosen</a>
					<ul class="other-section">
							<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
							<li><a href="lihat_dosen.php" class="no-ch">Lihat Dosen</a></li>
							
							<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Pinjam Ruang</a></li>
							<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a  class="active daddy main-node">Tutor</a>
					<ul class="other-section">
							<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
							<li><a href="lihat_tutor.php" class="active daddy main-node">Lihat Tutor</a></li>
							
							<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Pinjam Ruang </a></li>
							<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a  class="daddy">Matakuliah</a>
					<ul class="other-section">
							<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
							<li><a href="lihat_matakuliah.php" class="no-ch">Lihat Matakuliah</a></li>
							
					</ul>
					</li>
	</ul>
	</li>

	<li><a href="info_lab.php" class="daddy">Info lab</a>
			<ul class="other-section">
					<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
					<li><a href="lihat_berita.php" class="no-ch">Lihat Berita</a></li>
			</ul>
	</li>

	<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a>
			<ul class="other-section">
					<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
					<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
			</ul>
	</li>

	</div>
	<div id='topPagination'>
		<div class="pagination noprt">
			<?php
			echo"Selamat Datang &nbsp; ' <i>".$username." '</i> <a href='../login/logout.php'><u>Keluar</u></a>";
			?>
		<span> </span></a>
		</div>
	</div>

<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Data Tutor</h1>

		<div class=""> 
			<form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
				Cari Tutor <input size="35" type="text" name="q" placeholder="Masukkan Nama/Nip/Prodi" value="<?php if (isset($_GET['q'])){ echo $_GET['q']; } ?>"/>    
								<input type='submit' name="<?php echo $_SERVER['PHP_SELF']; ?>" id='q' Value="Cari">
					
			</form>
 
		</div>

<?php
 include "koneksi.php";
 if (isset($_GET['q'])){
     $query = "SElECT * FROM 
				tutor  WHERE Nama_Tutor LIKE '%$_GET[q]%' OR Nip_Tutor LIKE '%$_GET[q]%' OR Prodi LIKE '%$_GET[q]%'";
    $result = mysql_query($query);
    $jml = mysql_num_rows($result);
     
    if ($jml>0){
		
		echo "<div class='tambahdata'> <a href='tambah_tutor.php'><b>+<b>Tambah Data Baru</a> </div> </br>";
	
         echo "<table border='1' class='tabeldata' width='100%'>
				<tr>
		<th width='5%'>Kontrol</th>
		<th width='5%'>Id Tutor</th>
		<th width='10%'>Nip</th>
		<th width='15%'>Nama </th>
		<th width='7%'>Id Masuk</th> 
		<th width='10%'>Prodi</th> 
		<th width='15%'>Email</th> 
		<th width='8%'>Jns Kel.</th>
		</tr>";
        $no=1;    
         
        while ($row= mysql_fetch_array($result)) {
          echo "<tr>
						<td align='center'>
						<a href='ubah_tutor.php?id=$row[Id_Tutor] '><img alt='edit' title='Edit' src='icon/edit.png' /></a>
						&nbsp;&nbsp; 
						<a href='hapus_tutor.php?id= $row[Id_Tutor]' onclick=\"return confirm('Anda yakin akan menghapus data?')\"><img alt='hapus' title='Hapus' src='icon/hapus.png' /></a>
						</td>
						<td>$no</td>
						<td>$row[Nip_Tutor]</td>
						<td>$row[Nama_Tutor]</td>
						<td>$row[Id_Masuk]</td>
						<td>$row[Prodi]</td>
						<td>$row[Email]</td>
						<td>$row[Jenis_Kelamin]</td>
						</tr>";    
         $no++;    
        }
        echo "</table>";         
    } else {
        echo "Pencarian $_GET[q] tidak ditemukan";
    }
 }
?>	

<?php 

//Koneksi Ke Database
$connect=mysqli_connect("localhost","root","","silkss_db");

	echo "<div class='tambahdata'> <a href='tambah_tutor.php'><b>+<b>Tambah Data Baru</a> </div> </br>";
		
		// -------- TAMPILKAN DATANYA-----//
		echo "<table border='1' class='tabeldata' width='100%'>
		<tr>
		<th width='5%'>Kontrol</th>
		<th width='5%'>Id Tutor</th>
		<th width='10%'>Nip</th>
		<th width='15%'>Nama </th>
		<th width='7%'>Id Masuk</th> 
		<th width='10%'>Prodi</th> 
		<th width='15%'>Email</th> 
		<th width='8%'>Jns Kel.</th>
		</tr>";
		
		//tampilkan datanya
		$sql = "SELECT * FROM `tutor` ORDER BY `Id_Tutor` DESC  ";
		$result=mysqli_query($connect,$sql);
		if ($result==null)//cek 
		  {
		  die('</br>Error: ' . mysqli_error($connect));//jika pencarian data error, tampilkan errornya apa ?
		  }
		//echo " </br> "; //jika pencarian data sukses/berhasil
		$data 	= mysqli_num_rows($result);
		if ($data == 0) {
			echo "<tr><td  colspan='4'> Data Kosong</td></tr>";
		} else {
			$no = 1;
			while ($row = mysqli_fetch_array($result)) {
				
				echo 	"<tr>
						<td align='center'>
						<a href='ubah_tutor.php?id=$row[Id_Tutor] '><img alt='edit' title='Edit' src='icon/edit.png' /></a>
						&nbsp;&nbsp; 
						<a href='hapus_tutor.php?id= $row[Id_Tutor]' onclick=\"return confirm('Anda yakin akan menghapus data?')\"><img alt='hapus' title='Hapus' src='icon/hapus.png' /></a>
						</td>
						<td>$no</td>
						<td>$row[Nip_Tutor]</td>
						<td>$row[Nama_Tutor]</td>
						<td>$row[Id_Masuk]</td>
						<td>$row[Prodi]</td>
						<td>$row[Email]</td>
						<td>$row[Jenis_Kelamin]</td>
						</tr>";
						
				$no++;
			}
		}
		echo "</table>";
		

mysqli_close($connect);	// tutup koneksi		
?>


</div>
</div>
</div>
<div id="bottomPagination">Copy Right Labkom STKIP Surya 2013/2014
</div>
</body></html>
<?php
}
	else {
		header("location:../index.php");
	}
?>