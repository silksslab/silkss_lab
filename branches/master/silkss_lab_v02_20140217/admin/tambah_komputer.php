<?php
include "koneksi.php";
@session_start();
$username= $_SESSION['username'];
if ($username){
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="../base.css" />
<link rel="stylesheet" type="text/css" href="../content.css" />
<link rel="stylesheet" type="text/css" href="../nav.css" />
<title>Tambah Komputer | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="../common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>

<div id="siteNav">
	<ul>
	<li id="active"><a href="index.php" >BERANDA</a></li>
	<li><a href="penggunaan_lab.php"  class="active daddy main-node">Penggunaan Lab</a>

	<ul class="other-section">
				<li><a >Jadwal</a>
							<ul class="other-section">
							<li><a href="buat_jadwal.php" class="no-ch">Buat Jadwal</a></li>
							<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
							
							</ul>
				</li>
				
				<li><a  class="daddy">Ruangan</a>
							<ul class="other-section">
							<li><a href="tambah_ruangan.php" class="no-ch">Tambah Ruangan</a></li>
							<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
							
							</ul>
				</li>
				<li><a  class="active daddy main-node">Komputer</a>
							<ul class="other-section">
							<li><a href="tambah_komputer.php"  class="active daddy main-node"> Tambah Komputer</a></li>
							<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
							
							</ul>
				</li>
				
				
	</ul>
	</li>

	<li><a href="kinerja.php" class="daddy">Kinerja</a>
				<ul class="other-section">
				<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
				<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
				<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
				<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
				</ul>
				</li>
				
	<li><a href="kebutuhan.php" class="daddy">Kebutuhan</a>
	<ul class="other-section">
					<li><a  class="daddy">Mahasiswa</a>
					<ul class="other-section">
							<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
							<li><a href="lihat_mahasiswa_2.php" class="no-ch">Lihat Mahasiswa</a></li>
							<li><a href="formulir_pemakaian.php" class="no-ch">Formulir Pakai Komputer</a></li>
							<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pesan Komputer</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan Kom.
							</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemakaian Kom.</a></li>
							
					</ul>
					</li>
					
					<li><a class="daddy">Dosen</a>
					<ul class="other-section">
							<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
							<li><a href="lihat_dosen.php" class="no-ch">Lihat Dosen</a></li>
							
							<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Pinjam Ruang</a></li>
							<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a  class="daddy">Tutor</a>
					<ul class="other-section">
							<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
							<li><a href="lihat_tutor.php" class="no-ch">Lihat Tutor</a></li>
							
							<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Pinjam Ruang </a></li>
							<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a  class="daddy">Matakuliah</a>
					<ul class="other-section">
							<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
							<li><a href="lihat_matakuliah.php" class="no-ch">Lihat Matakuliah</a></li>
							
					</ul>
					</li>
	</ul>
	</li>

	<li><a href="info_lab.php" class="daddy">Info lab</a>
			<ul class="other-section">
					<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
					<li><a href="lihat_berita.php" class="no-ch">Lihat Berita</a></li>
			</ul>
	</li>

	<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a>
			<ul class="other-section">
					<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
					<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
			</ul>
	</li>

	</div>
	<div id='topPagination'>
		<div class="pagination noprt">
			<?php
			echo"Selamat Datang &nbsp; ' <i>".$username." '</i> <a href='../login/logout.php'><u>Keluar</u></a>";
			?>
		<span> </span></a>
		</div>
	</div>

<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Tambah Komputer</h1>

<div class="lihatdata">
		<a href="lihat_komputer.php"  >Lihat Data Komputer </a>
	</div>
	<hr></hr>

<!-- Start Formoid form-->
<link rel="stylesheet" href="tambah_komputer_files/formoid1/formoid-default-red.css" type="text/css" />
<script type="text/javascript" src="tambah_komputer_files/formoid1/jquery.min.js"></script>
<form onsubmit="return checkForm(this)" action="simpan_komputer.php" class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:880px;min-width:150px" method="post"><div class="title">
<h2>Menambah Data Komputer</h2></div>
	
	<div class="element-select" ><label class="title">Kode Komputer<span class="required">*</span></label>
	<div style="width:60px;"><span><select name="kdkom" required="required">

		<option value="001">001</option><br/>
		<option value="002">002</option><br/>
		<option value="003">003</option><br/>
		<option value="004">004</option><br/>
		<option value="005">005</option><br/>
		<option value="006">006</option><br/>
		<option value="007">007</option><br/>
		<option value="008">008</option><br/>
		<option value="009">009</option><br/>
		<option value="010">010</option><br/>
		<option value="011">011</option><br/>
		<option value="012">012</option><br/>
		<option value="013">013</option><br/>
		<option value="014">014</option><br/>
		<option value="015">015</option><br/>
		<option value="016">016</option><br/>
		<option value="017">017</option><br/>
		<option value="018">018</option><br/>
		<option value="019">019</option><br/>
		<option value="020">020</option><br/>
		<option value="021">021</option><br/>
		<option value="022">022</option><br/>
		<option value="023">023</option><br/>
		<option value="024">024</option><br/>
		<option value="025">025</option><br/>
		<option value="026">026</option><br/>
		<option value="027">027</option><br/>
		<option value="028">028</option><br/>
		<option value="029">029</option><br/>
		<option value="030">030</option><br/>
		<option value="031">031</option><br/>
		<option value="032">032</option><br/>
		<option value="033">033</option><br/>
		<option value="034">034</option><br/>
		<option value="035">035</option><br/>
		<option value="036">036</option><br/>
		<option value="037">037</option><br/>
		<option value="038">038</option><br/>
		<option value="039">039</option><br/>
		<option value="040">040</option><br/></select><i></i></span></div></div>
	
	<div class="element-input" ><label class="title">Merk Komputer<span class="required">*</span></label><input class="medium" type="text" name="merkom" required="required"/></div>
	<div class="element-input" ><label class="title">Model Komputer<span class="required">*</span></label><input class="medium" type="text" name="modkom" required="required"/></div>
	<div class="element-select" ><label class="title">Status Komputer<span class="required">*</span></label><div class="small"><span><select name="statuskom" required="required">

		<option value="Pakai">Pakai</option><br/>
		<option value="Tidak">Tidak</option><br/></select><i></i></span></div></div>

<div class="submit"><input type="submit" value="Simpan"/>
<input type="reset" value="Batal" onclick="return confirm('hapus data yang telah diinput?')">
			
</div></form>
<script type="text/javascript" src="tambah_komputer_files/formoid1/formoid-default-red.js"></script>
<!-- Stop Formoid form-->

</div>
</div>
</div>
<div id="formoid-info">Copy-Right Labkom STKIP-Surya</div>

</body></html>

<?php
}
	else {
		header("location:../index.php");
	}

?>