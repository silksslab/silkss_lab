<?php

define('EMAIL_FOR_REPORTS', 'yandripono@gmail.com');
define('RECAPTCHA_PRIVATE_KEY', '@privatekey@');
define('FINISH_URI', 'http://');
define('FINISH_ACTION', 'message');
define('FINISH_MESSAGE', 'Thanks for filling out my form!');
define('UPLOAD_ALLOWED_FILE_TYPES', 'doc, docx, xls, csv, txt, rtf, html, zip, jpg, jpeg, png, gif');

require_once str_replace('\\', '/', __DIR__) . '/handler.php';

?>

<?php if (frmd_message()): ?>
<link rel="stylesheet" href="<?=dirname($form_path)?>/formoid-default-red.css" type="text/css" />
<span class="alert alert-success"><?=FINISH_MESSAGE;?></span>
<?php else: ?>
<!-- Start Formoid form-->
<link rel="stylesheet" href="<?=dirname($form_path)?>/formoid-default-red.css" type="text/css" />
<script type="text/javascript" src="<?=dirname($form_path)?>/jquery.min.js"></script>

<form action="add_ruang.php" class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:880px;min-width:150px" method="post"><div class="title"><h2>Menambah Data Ruangan</h2></div>
	
	<div class="element-input" ><label class="title">Kode Ruangan<span class="required">*</span></label><input class="small" type="text" name="kdruang" required="required"/></div>
	<div class="element-input" ><label class="title">Nama Ruangan<span class="required">*</span></label><input class="medium" type="text" name="nmruang" required="required"/></div>
	<div class="element-input" ><label class="title">Lokasi</label><input class="small" type="text" name="lokasi" /></div>
	<div class="element-input" ><label class="title">Kapasitas<span class="required">*</span></label><input class="small" type="text" name="kapas" required="required"/></div>

<div class="submit"><input type="submit" value="Simpan"/></div>
</form>

<script type="text/javascript" src="<?=dirname($form_path)?>/formoid-default-red.js"></script>

<!-- Stop Formoid form-->
<?php endif; ?>

<?php frmd_end_form(); ?>