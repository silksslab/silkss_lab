<?php
include "koneksi.php";
@session_start();
$username= $_SESSION['username'];
if ($username){
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="../base.css" />
<link rel="stylesheet" type="text/css" href="../content.css" />
<link rel="stylesheet" type="text/css" href="../nav.css" />
<title>Tambah Ruangan | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="../common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>
<div id="siteNav">
	<ul>
	<li id="active"><a href="index.php" >BERANDA</a></li>
	<li><a href="penggunaan_lab.php" class="active daddy main-node">Penggunaan Lab</a>

	<ul class="other-section">
				<li><a >Jadwal</a>
							<ul class="other-section">
							<li><a href="buat_jadwal.php" class="no-ch">Buat Jadwal</a></li>
							<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
							
							</ul>
				</li>
				
				<li><a  class="active daddy main-node">Ruangan</a>
							<ul class="other-section">
							<li><a href="tambah_ruangan.php" class="active daddy main-node" >Tambah Ruangan</a></li>
							<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
							
							</ul>
				</li>
				<li><a >Komputer</a>
							<ul class="other-section">
							<li><a href="tambah_komputer.php" class="no-ch">Tambah Komputer</a></li>
							<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
							
							</ul>
				</li>
				
				
	</ul>
	</li>

	<li><a href="kinerja.php" class="daddy">Kinerja</a>
				<ul class="other-section">
				<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
				<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
				<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
				<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
				</ul>
				</li>
				
	<li><a href="kebutuhan.php" class="daddy">Kebutuhan</a>
	<ul class="other-section">
					<li><a  class="daddy">Mahasiswa</a>
					<ul class="other-section">
							<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
							<li><a href="lihat_mahasiswa_2.php" class="no-ch">Lihat Mahasiswa</a></li>
							<li><a href="formulir_pemakaian.php" class="no-ch">Formulir Pakai Komputer</a></li>
							<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pesan Komputer</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan Kom.
							</a></li>
							<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemakaian Kom.</a></li>
							
					</ul>
					</li>
					
					<li><a class="daddy">Dosen</a>
					<ul class="other-section">
							<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
							<li><a href="lihat_dosen.php" class="no-ch">Lihat Dosen</a></li>
							
							<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Pinjam Ruang</a></li>
							<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a  class="daddy">Tutor</a>
					<ul class="other-section">
							<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
							<li><a href="lihat_tutor.php" class="no-ch">Lihat Tutor</a></li>
							
							<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Pinjam Ruang </a></li>
							<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
					</ul>
					</li>
					
					<li><a  class="daddy">Matakuliah</a>
					<ul class="other-section">
							<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
							<li><a href="lihat_matakuliah.php" class="no-ch">Lihat Matakuliah</a></li>
							
					</ul>
					</li>
	</ul>
	</li>

	<li><a href="info_lab.php" class="daddy">Info lab</a>
			<ul class="other-section">
					<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
					<li><a href="lihat_berita.php" class="no-ch">Lihat Berita</a></li>
			</ul>
	</li>

	<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a>
			<ul class="other-section">
					<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
					<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
			</ul>
	</li>

	</div>
<div id='topPagination'>
		<div class="pagination noprt">
			<?php
			echo"Selamat Datang &nbsp; ' <i>".$username." '</i> <a href='../login/logout.php'><u>Keluar</u></a>";
			?>
		<span> </span></a>
		</div>
	</div>
	
<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle"></h1>
<div class="lihatdata">
		<a href="lihat_ruangan.php"  >Lihat Data Ruangan </a>
	</div>
	<hr></hr>



<!-- Start Formoid form-->
<link rel="stylesheet" href="tambah_ruang_files/formoid1/formoid-default-red.css" type="text/css" />
<script type="text/javascript" src="tambah_ruang_files/formoid1/jquery.min.js"></script>
<form action="simpan_ruangan.php" class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:880px;min-width:150px" method="post"><div class="title">
<h2><center>Menambah Data Ruangan </center></h2></div>
	
					
				<div class="element-select" ><label class="title">Kode Ruangan<span class="required">*</span></label>
				<div style="width:60px;"><span><select name="kdruang" required="required" >
				<option value="101">101</option><br/>
				<option value="102">102</option><br/>
				<option value="103">103</option><br/>
				<option value="104">104</option><br/>
				<option value="105">105</option><br/>
				<option value="106">106</option><br/>
				<option value="107">107</option><br/>
				<option value="108">108</option><br/>
				<option value="109">109</option><br/>
				<option value="201">201</option><br/>
				<option value="202">202</option><br/>
				<option value="203">203</option><br/>
				<option value="204">204</option><br/>
				<option value="205">205</option><br/>
				<option value="206">206</option><br/>
				<option value="207">207</option><br/>
				<option value="208">208</option><br/>
				<option value="209">209</option><br/>
				<option value="301">301</option><br/>
				<option value="302">302</option><br/>
				<option value="303">303</option><br/>
				<option value="304">304</option><br/>
				<option value="305">305</option><br/>
				<option value="306">306</option><br/>
				<option value="307">307</option><br/>
				<option value="308">308</option><br/>
				<option value="309">309</option><br/>
				<option value="401">401</option><br/>
				<option value="402">402</option><br/>
				<option value="403">403</option><br/>
				<option value="404">404</option><br/>
				<option value="405">405</option><br/>
				<option value="406">406</option><br/>
				<option value="407">407</option><br/>
				<option value="408">408</option><br/>
				<option value="409">409</option><br/></select><i></i></span></div>
				</div>

	<div class="element-input" ><label class="title">Nama Ruangan<span class="required">*</span></label><input class="medium" type="text" name="nmruang" required="required"/></div>
	<div class="element-input" ><label class="title">Lokasi</label><input class="small" type="text" name="lokasi" /></div>
	
		<script>
		function angka(e) {
		if (!/^[0-9]+$/.test(e.value)) {
		  e.value = e.value.substring(0,e.value.length-1);
		}
		}

		</script>

	<div class="element-input" ><label class="title">Kapasitas<span class="required">*</span></label>
	<input class="small" type="number" name="kapas" id="harga" onkeyup="angka(this)" required="required"/>  <i class="note"> Kapasitas berupa angka</i></div>

<div class="submit"><input type="submit" name="btnsimpan" value="Simpan"/>
					<input type="reset" value="Batal" onclick="return confirm('hapus data yang telah diinput?')">
		
</div></form>
<script type="text/javascript" src="tambah_ruang_files/formoid1/formoid-default-red.js"></script>


<!-- Stop Formoid form-->


</div>
</div>
</div>
<div id="formoid-info">Copy-Right Labkom STKIP-Surya</div>
</body></html>
<?php
}
	else {
		header("location:../index.php");
	}

?>