
<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="base.css" />
<link rel="stylesheet" type="text/css" href="content.css" />
<link rel="stylesheet" type="text/css" href="nav.css" />
<title>Info lab | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>
<div id="siteNav">
<ul>
<li><a href="index.php" class="daddy main-node">BERANDA</a></li>

<li id="active"><a href="penggunaan_lab.php" >Penggunaan Lab</a>
<ul>
<li><a href="lihat_jadwal.php" >Jadwal</a>
</li>

</ul>
</li>

<li id="active"><a href="kebutuhan.php" >Kebutuhan</a>
</li>
<li><a href="info_lab.php" class="active daddy">Info lab</a>
</li>
<li><a href="formulir_kontak.php" class="daddy">Kontak Kami</a>
</li>
</ul>
</div>
<div id='topPagination'><div class="pagination noprt"><a href="login/login.php" class="next"> Masuk<span> </span></a></div>
	</div>

<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Info lab</h1>

<div>

<table border="0" >
<tr>
	<td>Alamat 
	</td>
	<td>
: Gedung Surya Research and Education Center (SURE Center), Jl. Scientia Boulevard Blok U 7
	</td>
</tr>
<tr>
	<td>
Telpon 
	</td>
	<td>
	: ---
	</td>
</tr>
<tr>
	<td>
Fax 
	</td>
	<td>
: (021) 5464535
	</td>
</tr>
<tr>
	<td>
Jam Kerja
	</td>
	<td>
: Senin - Jumat :
08.00 - 17.00 WIB
	</td>
</tr>
</table>
</div>
</div>

<div id='bottomPagination'><div class="pagination noprt"></div>Copy Right Labkom STKIP Surya 2013/2014
</div></div>
</div>
</body></html>
