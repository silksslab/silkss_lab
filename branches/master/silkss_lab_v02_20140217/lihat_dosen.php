
<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="base.css" /><link rel="stylesheet" type="text/css" href="content.css" /><link rel="stylesheet" type="text/css" href="nav.css" /><title>Lihat Dosen | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>
<div id="siteNav">
<ul>
<li><a href="index.php" class="daddy main-node">BERANDA</a></li><li><a href="penggunaan_lab.php" class="daddy">Penggunaan Lab</a><ul class="other-section">
<li><a href="jadwal.php" class="daddy">Jadwal</a><ul class="other-section">
<li><a href="buat_jadwal.php" class="no-ch">Buat Jadwal</a></li>
<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
<li><a href="ubah_jadwal.php" class="no-ch">Ubah Jadwal</a></li>
<li><a href="hapus_jadwal.php" class="no-ch">Hapus Jadwal</a></li>
</ul>
</li>
<li><a href="ruangan.php" class="daddy">Ruangan</a><ul class="other-section">
<li><a href="tambah_ruangan.php" class="no-ch">Tambah Ruangan</a></li>
<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
<li><a href="ubah_ruangan.php" class="no-ch">Ubah Ruangan</a></li>
<li><a href="hapus_ruangan.php" class="no-ch">Hapus Ruangan</a></li>
</ul>
</li>
<li><a href="komputer.php" class="daddy">Komputer</a><ul class="other-section">
<li><a href="tambah_komputer.php" class="no-ch">Tambah Komputer</a></li>
<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
<li><a href="ubah_komputer.php" class="no-ch">Ubah Komputer</a></li>
<li><a href="hapus_komputer.php" class="no-ch">Hapus Komputer</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="kinerja.php" class="daddy">Kinerja</a><ul class="other-section">
<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
</ul>
</li>
<li class="current-page-parent"><a href="kebutuhan.php" class="current-page-parent daddy">Kebutuhan</a><ul>
<li><a href="mahasiswa.php" class="daddy">Mahasiswa</a><ul class="other-section">
<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
<li><a href="lihat_mahasiswa.php" class="no-ch">Lihat Mahasiswa</a></li>
<li><a href="ubah_mahasiswa.php" class="no-ch">Ubah Mahasiswa</a></li>
<li><a href="hapus_mahasiswa.php" class="no-ch">Hapus Mahasiswa</a></li>
<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pemesanan</a></li>
<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan</a></li>
<li><a href="mengambil_matakuliah.php" class="no-ch">Mengambil Matakuliah</a></li>
</ul>
</li>
<li class="current-page-parent"><a href="dosen.php" class="current-page-parent daddy">Dosen</a><ul>
<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
<li id="active"><a href="lihat_dosen.php" class="active no-ch">Lihat Dosen</a></li>
<li><a href="ubah_dosen.php" class="no-ch">Ubah Dosen</a></li>
<li><a href="hapus_dosen.php" class="no-ch">Hapus Dosen</a></li>
<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Peminjaman Ruangan</a></li>
<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li><a href="tutor.php" class="daddy">Tutor</a><ul class="other-section">
<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
<li><a href="lihat_tutor.php" class="no-ch">Lihat Tutor</a></li>
<li><a href="ubah_tutor.php" class="no-ch">Ubah Tutor</a></li>
<li><a href="hapus_tutor.php" class="no-ch">Hapus Tutor</a></li>
<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Peminjaman</a></li>
<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li><a href="matakuliah.php" class="daddy">Matakuliah</a><ul class="other-section">
<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
<li><a href="lihat_matakuliah.php" class="no-ch">Lihat Matakuliah</a></li>
<li><a href="ubahmatakuliah.php" class="no-ch">UbahMatakuliah</a></li>
<li><a href="hapus_matakuliah.php" class="no-ch">Hapus Matakuliah</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="info_lab.php" class="daddy">Info lab</a><ul class="other-section">
<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
<li><a href="lihat_berita.php" class="no-ch">Lihat Berita</a></li>
</ul>
</li>
<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a><ul class="other-section">
<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
</ul>
</div>
<div id='topPagination'><div class="pagination noprt"><a href="tambah_dosen.php" class="prev"><span>&laquo; </span>Previous</a> | <a href="ubah_dosen.php" class="next"> Next<span> &raquo;</span></a></div>
</div><div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Lihat Dosen</h1>



<?php 

//echo $_SESSION['username'];
// mengambil data yang telah disimpan pada form
//session

//memberikan variabel
$nama	= $_POST["nama"];
$nip	= $_POST["nip"];
$email 	= $_POST["email"];
$jk 	= $_POST["jk"];
$prodi	= $_POST["prodi"];
$password =$_POST["password"];
$username =$_POST["username"];
$level   =$_POST["level"];

//Koneksi Ke Database
$connect=mysqli_connect("localhost","root","","silkss_db");
// Cek Koneksi ke database
	if (mysqli_connect_errno())
		{
		echo "koneksi ke databse error: " . mysqli_connect_error();//jika koneksi error, tampilkan errornya apa?
		}
	else
		{
		echo "koneksi ke database sukses";//jika koneksi sukses
		}

//cari username di tabel masuk dan cek apakah sudah ada / bleum
$sql = "SELECT Username FROM `masuk` WHERE Username=".$username;
	$result=mysqli_query($connect,$sql);
	if ($result==null)//cek jika belum ada maka
		  {
		  
//memasukan data ke tabel masuk  --> harus perhatikan jumlah field = valuesnya
$sql="INSERT INTO masuk (Id_Masuk,Password,Username,Level)
VALUES
   (default,
    '$_POST[password]',
	'$_POST[username]',
	'$_POST[level]')";
// perintah untuk menjalankan query
if (!mysqli_query($connect,$sql))//cek 
  {
  die('</br>Error: ' . mysqli_error($connect));//jika penambahan data error, tampilkan errornya apa ?
  }
echo " </br> 1 baris bertambah di tabel masuk"; //jika penambahan data sukses/berhasil

//cek id masuk di tabel masuk apakah passwor dan usernamw yg di tabel masuk  = yg baru dimasukan ?
$sql="SELECT Id_Masuk FROM masuk WHERE
Password = '$_POST[password]' and
Username = '$_POST[username]' and
Level = '$_POST[level]'";

$result = mysqli_query($connect,$sql);//buat variabel result utk simpan hasil select

$id_masuk = mysqli_fetch_array($result);
//die(var_dump((int)$id_masuk['Id_Masuk']));

//memasukan data ke database  --> harus perhatikan jumlah field = valuesnya

//cek Nip di tabel tutor apakah = nip yg baru dimasukan ?
$sql = "SELECT Nip_Dosen FROM `dosen` WHERE Nip_Dosen=".$nip;
	$result=mysqli_query($connect,$sql);
	if ($result==null)//cek jika kosong maka
		  {
		  die('</br>Error: ' . mysqli_error($connect));//jika pencarian data error, tampilkan errornya apa ?
		  }
	echo " </br> Pencarian berhasil"; //jika pencarian data sukses/berhasil
	$data 	= mysqli_num_rows($result);
	if ($data != 0) {
			echo "<tr><td  colspan='4'> <br><div style=background-color:orange;color:black;font-size:15px;text-align:center;>Data  Sudah Ada</div> </td></tr>";
	} else {

$sql="INSERT INTO dosen(
	Id_Dosen,Nip_Dosen,Nama_Dosen,Id_Masuk,
	Prodi,Email,Jenis_Kelamin)
VALUES 
   (default, '".$nip."','".$nama."', ".$id_masuk['Id_Masuk'].",'".$prodi."','".$email."','".$jk."')";

	
// perintah untuk menjalankan query
if (!mysqli_query($connect,$sql))//cek 
  {
  die('</br>Error: ' . mysqli_error($connect));//jika penambahan data error, tampilkan errornya apa ?
  }
echo " </br> 1 baris bertambah di tabel dosen"; //jika penambahan data sukses/berhasil

	
		// -------- TAMPILKAN DATANYA-----//
		echo "<table border='1' class='data'>
		<tr>
		<th width='5%'>Id Dosen</th>
		<th width='10%'>Nip</th>
		<th width='17%'>Nama </th>
		<th width='10%'>Id Masuk</th> 
		<th width='10%'>Prodi</th> 
		<th width='20%'>Email</th> 
		<th width='5%'>Jenis Kelamin</th>
		</tr>";
		
		//tampilkan datanya
		$sql = "SELECT * FROM `dosen` ORDER BY `Id_Dosen` ASC  ";
		$result=mysqli_query($connect,$sql);
		if ($result==null)//cek 
		  {
		  die('</br>Error: ' . mysqli_error($connect));//jika pencarian data error, tampilkan errornya apa ?
		  }
		echo " </br> Pencarian berhasil"; //jika pencarian data sukses/berhasil
		$data 	= mysqli_num_rows($result);
		if ($data == 0) {
			echo "<tr><td  colspan='4'> Data Kosong</td></tr>";
		} else {
			$no = 1;
			while ($row = mysqli_fetch_array($result)) {
				
				echo 	"<tr>
						<td>$no</td>
						<td>$row[Nip_Dosen]</td>
						<td>$row[Nama_Dosen]</td>
						<td>$row[Id_Masuk]</td>
						<td>$row[Prodi]</td>
						<td>$row[Email]</td>
						<td>$row[Jenis_Kelamin]</td>
						</tr>";
						
				$no++;
			}
		}
		echo "</table> ";
		}
}		
mysqli_close($connect);	// tutup koneksi		
?>



</div>
<div id='division_budget'><div class="pagination noprt"><a href="tambah_dosen.php" class="prev"><span>&laquo; </span>Previous</a> | <a href="ubah_dosen.php" class="next"> Next<span> &raquo;</span></a></div>
</div></div>
</div>
</body></html>
