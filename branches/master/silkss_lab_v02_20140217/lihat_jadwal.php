<?php 

include 'admin/connect.php';
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>

		<script type="text/javascript" src="scrollup/jquery-1.4.min.js">
		</script>
		<script type="text/javascript" src="scrollup/jquery.pjScrollUp.min.js">
		</script>
		<script>
		$(function() {
			$(document).pjScrollUp({
				imgSrc: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABc5JREFUeNq8mWtIZVUUx7fX59Uan2OKesehJiVUJCYiqA/JfIrQCSJoxhkIwQ9+6NvMfJqhIYMmiFBuE81A+IjUGSSVEiVNFJEsJlPSfIzm+62j5hMfu/XfnnXndLvqOcfHhr/3eM9+/Fz7tdbSS0opLJQA0iukV0lJpDOkSFKI9n6RNEMaInWSftW0YnYgL5OA50jvk94hpZBs/GJ7e1tsbW2pZx8fH+Ht7a1vJzXQClIJqeuoAWNIH5KySOH4YnZ2VoyMjIjJyUkxPz8vVldXxcbGhqrs5+cnAgMDRVhYmIiKihKxsbEiMjJS6KxbSPqCNGgO0Mvr/zWkfI9+fkJ6AXW7u7tFR0eHSM/KEsM0MErg2truJ0GirBKc+rTb1adjdFRU3L8vUlJSRGJiIlsX03+LxizyMKb+WT6VNhdy93sf0h2plYGBAVlYWCi7ExKUVgID0Y0hoe6ww6HafZefL/v6+qSu5JPsbmO75BlQShpdFqP15uamrKurk7MREWoAo1B7CaBQTU2NpCXBkN+TQowBSulL+hatlpeXZUlJiXyUmmrKYgdpNShI9VlUVCQXFxcZskIZxgDgHYYrKChQHR0VmLswI5XXr+shv9ofUMp3eVrZcscFx8IYWNvr6+sM+cFegFGkQdSor68/ETg9ZHV1NQNOkuI9AX6Gt4ODg2pDWFlzvAGwxsyuSbTr7e1lyK+Zi2+C50nZuA0aGxvFbHi460wzWn5PTRV/OJ3icUGBOh/XgoIMt7Wv7N6A7Q8eCFpeeMzUrlCXBW8Bmw5hS0cJpqi8vFzSVSd5iaAfs5ZEm/b2drbi5zzFdlIHvikrKzMNyHDYWPpiBRLTXFxcLHd2dtDFY9KzAHyDtEN3q+rMzNrbC84qJNebmJjgLt4C4DU8tbW1mbLeQXBWIVG3tbWVm39s48U4NTVlakMM3rwp0tPTlWu1X0lLSxNjd++a2jhkQX5MAqADT3Nzcy6vxCocXK+hoaFDQYIB7pvmZTkAGEmLUvlzh4GbmZkRpaWlSsPDw5YhcbytESSOPCphIB2DV+F0OtUBbWXNTU9Py0cXLrgO6rnTpyVZ0tKaBENeXp4kSFSfsOn9QquWG7l0SQSRdx1HloNWyFF95vz5Q1mSeQC4iEH9/f1dnrAVuISeHtf3h4X09fXlsdYBOGOz2VQM4V5GHA4xevu2yMjIMAxnBnLq3j0VFughYaSAgAAe7wkAVWsEOBxD6Au+d4vQDoQzCumpgCE0NJRCFRUfjQDwTzwh+nIvEXT0nLl8WTQ3N5uG8wSpP4IaGhrEc9nZ6lhhZ4GLjqULi/FN3onYWe67C1cfdl1LS4ukw1y2XLxoyaHA7h6Nj1fuXFNTk8edzFft+Pg4b/p0AFIt2YXf4EV7GhyN0Hjs7Nl9j6KDhLYM4emYwR8B71pzFv5GIGXT0hHlsCfi1r0OT5QQug7DKWC3Wrgt1pf7tPL6S0pK4vX3A2mB/cFE0j84hBG34i85KXdfb72anBwORXFKv+zu8ufhTX9/vyW3/bBhKMbs6uritVfgKSaJw7WHt7W1tSceNFVVVTHcLOncXmFnJmrAzAiqTwISm/Jhbq4kZ4UBcw4K3J2otbS0pILq4w7cf87MlOReMVyhkcyCXcuVqIgf2x6QR7kmOfUBy+ngfiKdMpo8CtZyJSriR1DNrtRR7FaV8qis1E8r4CKNZ7eeWvJL7qGnp0dW3bihOje7y3mXoi2Oks7OTr2L+I3Lch4AjSQwkVX9iBSLFC91rhKYV65eFT0JCS433T3Qh1fCzkcc3dsPyb1KTk5WBzEysAiDSLk0pnO/BKbRFDAyD9e0iD+IAxt4KPhcWFhQbrqWFVCukp3ggoODRXR0tHCQ2xYTE8N9rZPKSJ+Suo86iZ5KukJ6m/Si3vuFdRFH4BmAkNd/Z2SA9COpmPTbcWX5uZwivU56jfQSKZ4Uqv17AgXZ9CdaHvov0i+kJnW3miz/CjAAph4RsgkK5+gAAAAASUVORK5CYII="
			});
		});
		</script>
<link rel="stylesheet" type="text/css" href="base.css" />
<link rel="stylesheet" type="text/css" href="content.css" />
<link rel="stylesheet" type="text/css" href="nav.css" />
<title>Lihat Jadwal | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>

<div id="siteNav">
	<ul>
	<li id="active"><a href="index.php" >BERANDA</a></li>
	<li><a href="penggunaan_lab.php" class="active daddy main-node">Penggunaan Lab</a>

	<ul class="other-section">
				<li><a href="lihat_jadwal.php" class="active daddy main-node">Jadwal</a>
							
				</li>
				
				
				
				
	</ul>
	</li>
				
	<li><a href="kebutuhan.php" class="daddy">Kebutuhan</a>

	</li>

	<li><a href="info_lab.php" class="daddy">Info lab</a>

	</li>

	<li><a href="formulir_kontak.php" class="daddy">Kontak Kami</a>

	</li>

	</div>
	<div id='topPagination'><div class="pagination noprt"><a href="login/login.php" class="next"> Masuk<span> </span></a></div>
	</div>
<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Lihat Jadwal</h1>

<?php
include 'admin/connect.php';
?>


<table border="1" width="100%" class='tabeldata'>
    <thead>
        <tr>
			
			
			<th width="10%">Hari</th>
            <th width="10%" >Waktu</th>
            <th width="20%">Matakuliah</th>
            <th width="5%">Kelas </th>
            <th width="10%">Pengajar</th>
            <th width="7%">Ruangan</th>
            
        </tr>
    </thead>
    
    <tbody>
    <?php
    $sql = "SELECT * FROM jadwal ORDER BY Id_Jadwal DESC";
    $no  = 1;
    foreach ($dbh->query($sql) as $data) :
    ?>
        <tr>
            
            <td><?php echo $data['Hari'] ?></td>
            <td><?php echo $data['Jam_Masuk'] ?> - <?php echo $data['Jam_Keluar'] ?> </td>
            <td><?php echo $data['Matakuliah'] ?></td>
            <td><?php echo $data['Kelas'] ?></td>
			<td><?php echo $data['Pengajar'] ?></td>
			<td><?php echo $data['Ruangan'] ?></td>
            
        </tr>
    <?php
	
	//die($data['Id_Ruangan']);
    endforeach;
	
    ?>
    </tbody>
	
</table>



</div>
</div>
</div>
<div id="bottomPagination" >Copy Right Labkom STKIP Surya 2013/2014
</div>
</body></html>
