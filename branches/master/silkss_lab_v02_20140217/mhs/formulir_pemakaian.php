<?php
include "koneksi.php";
@session_start();
$username= $_SESSION['username'];
if ($username){
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="../base.css" />
<link rel="stylesheet" type="text/css" href="../content.css" />
<link rel="stylesheet" type="text/css" href="../nav.css" />
<title>Formulir Pemakaian | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

		<script type="text/javascript" src="../scrollup/jquery-1.4.min.js">
		</script>
		<script type="text/javascript" src="../scrollup/jquery.pjScrollUp.min.js">
		</script>
		<script>
		$(function() {
			$(document).pjScrollUp({
				imgSrc: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABc5JREFUeNq8mWtIZVUUx7fX59Uan2OKesehJiVUJCYiqA/JfIrQCSJoxhkIwQ9+6NvMfJqhIYMmiFBuE81A+IjUGSSVEiVNFJEsJlPSfIzm+62j5hMfu/XfnnXndLvqOcfHhr/3eM9+/Fz7tdbSS0opLJQA0iukV0lJpDOkSFKI9n6RNEMaInWSftW0YnYgL5OA50jvk94hpZBs/GJ7e1tsbW2pZx8fH+Ht7a1vJzXQClIJqeuoAWNIH5KySOH4YnZ2VoyMjIjJyUkxPz8vVldXxcbGhqrs5+cnAgMDRVhYmIiKihKxsbEiMjJS6KxbSPqCNGgO0Mvr/zWkfI9+fkJ6AXW7u7tFR0eHSM/KEsM0MErg2truJ0GirBKc+rTb1adjdFRU3L8vUlJSRGJiIlsX03+LxizyMKb+WT6VNhdy93sf0h2plYGBAVlYWCi7ExKUVgID0Y0hoe6ww6HafZefL/v6+qSu5JPsbmO75BlQShpdFqP15uamrKurk7MREWoAo1B7CaBQTU2NpCXBkN+TQowBSulL+hatlpeXZUlJiXyUmmrKYgdpNShI9VlUVCQXFxcZskIZxgDgHYYrKChQHR0VmLswI5XXr+shv9ofUMp3eVrZcscFx8IYWNvr6+sM+cFegFGkQdSor68/ETg9ZHV1NQNOkuI9AX6Gt4ODg2pDWFlzvAGwxsyuSbTr7e1lyK+Zi2+C50nZuA0aGxvFbHi460wzWn5PTRV/OJ3icUGBOh/XgoIMt7Wv7N6A7Q8eCFpeeMzUrlCXBW8Bmw5hS0cJpqi8vFzSVSd5iaAfs5ZEm/b2drbi5zzFdlIHvikrKzMNyHDYWPpiBRLTXFxcLHd2dtDFY9KzAHyDtEN3q+rMzNrbC84qJNebmJjgLt4C4DU8tbW1mbLeQXBWIVG3tbWVm39s48U4NTVlakMM3rwp0tPTlWu1X0lLSxNjd++a2jhkQX5MAqADT3Nzcy6vxCocXK+hoaFDQYIB7pvmZTkAGEmLUvlzh4GbmZkRpaWlSsPDw5YhcbytESSOPCphIB2DV+F0OtUBbWXNTU9Py0cXLrgO6rnTpyVZ0tKaBENeXp4kSFSfsOn9QquWG7l0SQSRdx1HloNWyFF95vz5Q1mSeQC4iEH9/f1dnrAVuISeHtf3h4X09fXlsdYBOGOz2VQM4V5GHA4xevu2yMjIMAxnBnLq3j0VFughYaSAgAAe7wkAVWsEOBxD6Au+d4vQDoQzCumpgCE0NJRCFRUfjQDwTzwh+nIvEXT0nLl8WTQ3N5uG8wSpP4IaGhrEc9nZ6lhhZ4GLjqULi/FN3onYWe67C1cfdl1LS4ukw1y2XLxoyaHA7h6Nj1fuXFNTk8edzFft+Pg4b/p0AFIt2YXf4EV7GhyN0Hjs7Nl9j6KDhLYM4emYwR8B71pzFv5GIGXT0hHlsCfi1r0OT5QQug7DKWC3Wrgt1pf7tPL6S0pK4vX3A2mB/cFE0j84hBG34i85KXdfb72anBwORXFKv+zu8ufhTX9/vyW3/bBhKMbs6uritVfgKSaJw7WHt7W1tSceNFVVVTHcLOncXmFnJmrAzAiqTwISm/Jhbq4kZ4UBcw4K3J2otbS0pILq4w7cf87MlOReMVyhkcyCXcuVqIgf2x6QR7kmOfUBy+ngfiKdMpo8CtZyJSriR1DNrtRR7FaV8qis1E8r4CKNZ7eeWvJL7qGnp0dW3bihOje7y3mXoi2Oks7OTr2L+I3Lch4AjSQwkVX9iBSLFC91rhKYV65eFT0JCS433T3Qh1fCzkcc3dsPyb1KTk5WBzEysAiDSLk0pnO/BKbRFDAyD9e0iD+IAxt4KPhcWFhQbrqWFVCukp3ggoODRXR0tHCQ2xYTE8N9rZPKSJ+Suo86iZ5KukJ6m/Si3vuFdRFH4BmAkNd/Z2SA9COpmPTbcWX5uZwivU56jfQSKZ4Uqv17AgXZ9CdaHvov0i+kJnW3miz/CjAAph4RsgkK5+gAAAAASUVORK5CYII="
			});
		});
		</script>


<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="../common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>

<div id="siteNav">
	<ul>
	<li id="active"><a href="index.php"  > BERANDA</a></li>
	<li><a href="penggunaan_lab.php" class="daddy">Penggunaan Lab</a>

	<ul class="other-section">
				<li><a >Jadwal</a>
							<ul class="other-section">
							
							<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
							
							</ul>
				</li>
				
				
				<li><a >Komputer</a>
							<ul class="other-section">
							
							<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
							
							</ul>
				</li>
				
				
	</ul>
	</li>

	<li><a href="kinerja.php" class="daddy">Kinerja</a>
				<ul class="other-section">
				<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
				
				</ul>
				</li>
				
	<li><a href="kebutuhan.php" class="active daddy main-node" >Kebutuhan</a>
	<ul class="other-section">
					
							
							<li><a class="active daddy main-node" href="formulir_pemakaian.php" class="no-ch">Formulir Pakai Komputer</a></li>
							<li><a class="daddy" href="formulir_pemesanan.php" class="no-ch">Formulir Pesan Komputer</a></li>
		
	</ul>
	</li>

	<li><a href="info_lab.php" class="daddy">Info lab</a>
			
	</li>

	<li><a href="formulir_kontak.php" class="daddy">Kontak Kami</a>
			
	</li>

	</div>
	<div id='topPagination'>
		<div class="pagination noprt">
			<?php
			echo"Selamat Datang &nbsp; ' <i>".$username." '</i> <a href='../login/logout.php'><u>Keluar</u></a>";
			?>
		<span> </span></a>
		</div>
	</div>

<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Formulir Pemakaian Komputer</h1></div>

<link rel="stylesheet" href="../tambah_mahasiswa_files/formoid1/formoid-default-red.css" type="text/css" />
<script type="text/javascript" src="../tambah_mahasiswa_files/formoid1/jquery.min.js">
</script>
<form onsubmit="return checkForm(this)" action="simpan_mahasiswa.php" class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:880px;min-width:150px" method="POST">
<table width="80%">

	<div class="element-input" >
	<tr>
		<td>
		<div class="element-input" >
		<script>
	function angka(e) {
	if (!/^[0-9]+$/.test(e.value)) {
      e.value = e.value.substring(0,e.value.length-1);
	}
	}

	</script>
			<label class="title">Nim<span class="required"></span></label>
		</td>
		<td>
			<input class="small" type="text" name="nim" maxlength="11"  id="harga" onkeyup="angka(this)"required="required"/> </div>
		</td>
	</tr>
	<tr>
	<div class="element-input" >
		<td>
			<label class="title">Nama<span class="required"></span></label>
		</td>
		<td>
			<input class="medium" type="text" name="name" required="required"/></div>
		</td>
	</tr>
	
	
	
	
	<tr>
	
	<div class="element-email" >
		<td>
			<label class="title">Email<span class="required"></span></label>
		</td>
		<td>
			<input class="medium" type="email" name="email" value="" required="required"/></div>
		</td>
	</tr>
	
	<tr>
	<td>
	<div class="element-select" >
			<label class="title">Angkatan<span class="required"></span></label>
		</td>
		<td>
		<div class="element-select" >
		<div style="width:80px;">	
		<span>
		<select name="angkatan" required="required">
			<option value="2010">2010</option><br/>
			<option value="2011">2011</option><br/>
			<option value="2012">2012</option><br/>
			<option value="2013">2013</option><br/>
			<option value="2014">2014</option><br/>
			<option value="2015">2015</option><br/>
			<option value="2016">2016</option><br/>
			<option value="2017">2017</option><br/>
			<option value="2018">2018</option><br/>
			<option value="2019">2019</option><br/>
			<option value="2020">2020</option><br/>
			<option value="2021">2021</option><br/>
			<option value="2022">2022</option><br/>
			<option value="2023">2023</option><br/>
			<option value="2024">2024</option><br/>
			<option value="2025">2025</option><br/>
		</select><i></i>
		</span>
		</div>	
		</div>
		</td>
	</div>
	</td>
	</tr>
	<tr>
		<td>
			<div class="element-input" >
				<script>
	function kab(e) {
	if (!/^[A-Za-z]+$/.test(e.value)) {
      e.value = e.value.substring(0,e.value.length-1);
	}
	}

	</script>
			<label class="title">Kabupaten<span class="required"></span></label>
		</td>
		<td>
			<input class="medium" type="text" id="harga" onkeyup="kab(this)" name="kab" /></div>
		</td>
	</tr>

	<tr>
		<td>
			<div class="element-select" >
			<label class="title">Prodi<span class="required"></span></label>
		</td>
		<td>
			<div class="element-select" >
			<div class="small">
			<span>
			<select name="prodi" required="required">
				<option value="Fisika">Fisika</option><br/>
				<option value="TIK">TIK</option><br/>
				<option value="Matematika">Matematika</option><br/>
				<option value="Kimia">Kimia</option><br/></select><i></i>
			</span>
			</div>
			</div>
			</div>
		</td>
		</tr>
		
		<tr>
		<td>
		<div class="element-textarea" ><label class="title">Isi Kebutuhan Anda :<span class="required">*</span></label>
		</td>
		<td>
		<textarea class="large" name="pesan" cols="20" rows="5" required="required"></textarea>
		</div>
		</td>		
		</tr>
		
		<tr>
		<td>
		</td>
		<td>
			<div class="submit">
				<input type="submit" name="btnsimpan" value="Simpan"/>
				<input type="reset" value="Batal" onclick="return confirm('hapus data yang telah diinput?')">
			
			</div>
		</td>		
		</tr>
	
</table>
</form>
<script type="text/javascript" src="../tambah_mahasiswa_files/formoid1/formoid-default-red.js"></script>


</div>
</div>
</body>
<div id="bottomPagination">Copy Right Labkom STKIP Surya 2013/2014
</div>
</html>
<?php
}
	else {
		header("location:../index.php");
	}
?>