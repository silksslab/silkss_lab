<?php 

include 'koneksi.php';
?>
<?php
include "koneksi.php";
@session_start();
$username= $_SESSION['username'];
if ($username){
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<script type="text/javascript">
	function konfirmasiHapus(Id_Jadwal,Matakuliah)
	{
		
		var Id_Jadwal=Id_Jadwal;
		var Matakuliah=Matakuliah;
		var jawab;
		
		jawab = confirm("Apakah data  '"+Matakuliah+"' akan dihapus ?")
		if(jawab)
		{
			window.location = "hapus_jadwal.php?;
			return false;
		}else{
			alert("Penghapusan data dibatalkan");
		}
	}
	</script>

<link rel="stylesheet" type="text/css" href="../base.css" />
<link rel="stylesheet" type="text/css" href="../content.css" />
<link rel="stylesheet" type="text/css" href="../nav.css" />
<title>Lihat Jadwal | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="../common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>

<div id="siteNav">
	<ul>
	<li id="active"><a href="index.php" >BERANDA</a></li>
	<li><a href="penggunaan_lab.php" class="active daddy main-node">Penggunaan Lab</a>

	<ul class="other-section">
				<li><a >Jadwal</a>
							<ul class="other-section">
							
							<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
							
							</ul>
				</li>
				
				
				<li><a >Komputer</a>
							<ul class="other-section">
							
							<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
							
							</ul>
				</li>
				
				
	</ul>
	</li>

	<li><a href="kinerja.php" class="daddy">Kinerja</a>
				<ul class="other-section">
				<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
				
				</ul>
				</li>
				
	<li><a href="kebutuhan.php" class="daddy">Kebutuhan</a>
	<ul class="other-section">
					
							
							<li><a class="daddy" href="formulir_pemakaian.php" class="no-ch">Formulir Pakai Komputer</a></li>
							<li><a class="daddy" href="formulir_pemesanan.php" class="no-ch">Formulir Pesan Komputer</a></li>
		
	</ul>
	</li>

	<li><a href="info_lab.php" class="daddy">Info lab</a>
			
	</li>

	<li><a href="formulir_kontak.php" class="daddy">Kontak Kami</a>
			
	</li>

	</div>
	<div id='topPagination'>
		<div class="pagination noprt">
			<?php
			echo"Selamat Datang &nbsp; ' <i>".$username." '</i> <a href='../login/logout.php'><u>Keluar</u></a>";
			?>
		<span> </span></a>
		</div>
	</div>
<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Lihat Jadwal</h1>
		
<?php
include 'connect.php';
?>

<table border="1" width="100%" class='tabeldata'>
    <thead>
        <tr>
			
			
			<th width="10%">Hari</th>
            <th width="10%" >Waktu</th>
            <th width="20%">Matakuliah</th>
            <th width="5%">Kelas </th>
            <th width="10%">Pengajar</th>
            <th width="7%">Ruangan</th>
            
        </tr>
    </thead>
    
    <tbody>
    <?php
    $sql = "SELECT * FROM jadwal ORDER BY Id_Jadwal DESC";
    $no  = 1;
    foreach ($dbh->query($sql) as $data) :
    ?>
        <tr>
            
            <td><?php echo $data['Hari'] ?></td>
            <td><?php echo $data['Jam_Masuk'] ?> - <?php echo $data['Jam_Keluar'] ?> </td>
            <td><?php echo $data['Matakuliah'] ?></td>
            <td><?php echo $data['Kelas'] ?></td>
			<td><?php echo $data['Pengajar'] ?></td>
			<td><?php echo $data['Ruangan'] ?></td>
            
        </tr>
    <?php
	
	//die($data['Id_Ruangan']);
    endforeach;
	
    ?>
    </tbody>
	
</table>



</div>
</div>
</div>
<div id="bottomPagination" >Copy Right Labkom STKIP Surya 2013/2014
</div>
</body></html>
<?php
}
	else {
		header("location:../index.php");
	}

?>