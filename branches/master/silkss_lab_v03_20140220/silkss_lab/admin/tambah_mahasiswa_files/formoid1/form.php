<?php

define('EMAIL_FOR_REPORTS', 'yandripono@gmail.com');
define('RECAPTCHA_PRIVATE_KEY', '@privatekey@');
define('FINISH_URI', 'http://');
define('FINISH_ACTION', 'message');
define('FINISH_MESSAGE', 'Thanks for filling out my form!');
define('UPLOAD_ALLOWED_FILE_TYPES', 'doc, docx, xls, csv, txt, rtf, html, zip, jpg, jpeg, png, gif');

require_once str_replace('\\', '/', __DIR__) . '/handler.php';

?>

<?php if (frmd_message()): ?>
<link rel="stylesheet" href="<?=dirname($form_path)?>/formoid-default-red.css" type="text/css" />
<span class="alert alert-success"><?=FINISH_MESSAGE;?></span>
<?php else: ?>
<!-- Start Formoid form-->
<link rel="stylesheet" href="<?=dirname($form_path)?>/formoid-default-red.css" type="text/css" />
<script type="text/javascript" src="<?=dirname($form_path)?>/jquery.min.js"></script>

<form action="add_mahasiswa.php" class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:880px;min-width:150px" method="post">
<table width="80%">
	<div class="title"><h2><center>Menambah Data Mahasiswa</center></h2></div>
	<div class="element-input" >
	
	<!--
	<tr>
		<td>
			<label class="title">Id Mahasiswa<span class="required">*</span>
			</label>
		</td>
		<td>
			<input class="small" type="text" name="idmasuk" required="required"/>
		</td>
	</div>
	</tr>
	-->
	<tr>
		<td>
		<div class="element-input" >
			<label class="title">Nim<span class="required">*</span></label>
		</td>
		<td>
			<input class="small" type="text" name="nim" required="required"/></div>
		</td>
	</tr>
	<tr>
	<div class="element-input" >
		<td>
			<label class="title">Nama<span class="required">*</span></label>
		</td>
		<td>
			<input class="medium" type="text" name="name" required="required"/></div>
		</td>
	</tr>
	<tr>
	<div class="element-input" >
		<td>
			<label class="title">Username<span class="required">*</span></label>
		</td>
		<td>
			<input class="small" type="text" name="username" required="required"/>
	</div>
		</td>
	</tr>
	<tr>
	<div class="element-input" >
		<td>
			<label class="title">Password <span class="required">*</span></label>
		</td>
		<td>
			<input class="small" type="password" name="password" required="required"/>
	</div>
		</td>
	</tr>
	
	<tr>
	
	<div class="element-email" >
		<td>
			<label class="title">Email<span class="required">*</span></label>
		</td>
		<td>
			<input class="medium" type="email" name="email" value="" required="required"/></div>
		</td>
	</tr>
	
	<tr>
	<td>
	<div class="element-select" >
			<label class="title">Angkatan<span class="required">*</span></label>
		</td>
		<td>
		<div class="element-select" >
		<div class="small">	
		<span>
		<select name="angkatan" required="required">
			<option value="2010">2010</option><br/>
			<option value="2011">2011</option><br/>
			<option value="2012">2012</option><br/>
			<option value="2013">2013</option><br/>
			<option value="2014">2014</option><br/>
			<option value="2015">2015</option><br/>
			<option value="2016">2016</option><br/>
			<option value="2017">2017</option><br/>
			<option value="2018">2018</option><br/>
			<option value="2019">2019</option><br/>
			<option value="2020">2020</option><br/>
			<option value="2021">2021</option><br/>
			<option value="2022">2022</option><br/>
			<option value="2023">2023</option><br/>
			<option value="2024">2024</option><br/>
			<option value="2025">2025</option><br/>
		</select><i></i>
		</span>
		</div>	
		</div>
		</td>
	</div>
	</td>
	</tr>
	<tr>
	<div class="element-radio" >
		<td>
			<label class="title">Jenis Kelamin<span class="required">*</span></label>	
		</td>
		<td>
		<div class="column column1"><input type="radio" name="jk" value="L" required="required"/><span>L</span><br/>
									<input type="radio" name="jk" value="P" required="required"/><span>P</span><br/></div><span class="clearfix"></span>
	</div>
		</td>
	</tr>
	<tr>
		<td>
			<div class="element-input" >
			<label class="title">Kabupaten<span class="required">*</span></label>
		</td>
		<td>
			<input class="medium" type="text" name="kab" required="required"/></div>
		</td>
	</tr>
	
	<tr>
		<td>
			<div class="element-input" >
			<label class="title">Provinsi</label>
		</td>
		<td>
			<input class="large" type="text" name="prov" /></div>
		</td>
	</tr>
	
	<tr>
		<td>
			<div class="element-select" >
			<label class="title">Prodi<span class="required">*</span></label>
		</td>
		<td>
			<div class="element-select" >
			<div class="small">
			<span>
			<select name="prodi" required="required">
				<option value="Fisika">Fisika</option><br/>
				<option value="TIK">TIK</option><br/>
				<option value="Matematika">Matematika</option><br/>
				<option value="Kimia">Kimia</option><br/></select><i></i>
			</span>
			</div>
			</div>
			</div>
		</td>
		</tr>
		
		<tr>
		<td>
		</td>
		<td>
			<div class="submit">
				<input type="submit" value="Simpan"/>
			</div>
		</td>
		</tr>
</table>
</form>


<script type="text/javascript" src="<?=dirname($form_path)?>/formoid-default-red.js">
</script>

<!-- Stop Formoid form-->
<?php endif; ?>

<?php frmd_end_form(); ?>