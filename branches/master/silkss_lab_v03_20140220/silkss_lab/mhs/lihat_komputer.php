
<?php 
include 'koneksi.php';
?>
<?php
include "koneksi.php";
@session_start();
$username= $_SESSION['username'];
if ($username){
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="../base.css" />
<link rel="stylesheet" type="text/css" href="../content.css" />
<link rel="stylesheet" type="text/css" href="../nav.css" />
<title>Lihat Komputer | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="../common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>

<div id="siteNav">
	<ul>
	<li id="active"><a href="index.php" >BERANDA</a></li>
	<li><a href="penggunaan_lab.php" class="active daddy main-node" >Penggunaan Lab</a>

	<ul class="other-section">
				<li><a >Jadwal</a>
							<ul class="other-section">
							
							<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
							
							</ul>
				</li>
				
				
				<li><a >Komputer</a>
							<ul class="other-section">
							
							<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
							
							</ul>
				</li>
				
				
	</ul>
	</li>

	<li><a href="kinerja.php" class="daddy">Kinerja</a>
				<ul class="other-section">
				<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
				
				</ul>
				</li>
				
	<li><a href="kebutuhan.php" class="daddy">Kebutuhan</a>
	<ul class="other-section">
					
							
							<li><a class="daddy" href="formulir_pemakaian.php" class="no-ch">Formulir Pakai Komputer</a></li>
							<li><a class="daddy" href="formulir_pemesanan.php" class="no-ch">Formulir Pesan Komputer</a></li>
		
	</ul>
	</li>

	<li><a href="info_lab.php" class="daddy">Info lab</a>
			
	</li>

	<li><a href="formulir_kontak.php" class="daddy">Kontak Kami</a>
			
	</li>

	</div>
	<div id='topPagination'>
		<div class="pagination noprt">
			<?php
			echo"Selamat Datang &nbsp; ' <i>".$username." '</i> <a href='../login/logout.php'><u>Keluar</u></a>";
			?>
		<span> </span></a>
		</div>
	</div>
	
<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Lihat Komputer</h1>
	
<?php 

	$connect=mysqli_connect($host,$userdb,$passdb,$dbname);

		// ================ TAMPILKAN DATANYA =====================//
		echo "<table border='1' class='tabeldata'>
			<tr>
			
			
			<th width='7%'>Kode Komputer</th>
			<th width='10%'>Merk Komputer</th>
			<th width='20%'>Model Komputer</th> 
			<th width='5%'>Status</th> 
			</tr>";
		$sql = "SELECT * FROM `komputer` ORDER BY `Id_Komputer` DESC  ";
		$result=mysqli_query($connect,$sql);
		if ($result==null)//cek 
		  {
		  die('</br>Error: ' . mysqli_error($connect));//jika pencarian data error, tampilkan errornya apa ?
		  }
		// echo " </br> "; //jika pencarian data sukses/berhasil
		$data 	= mysqli_num_rows($result);
		if ($data == 0) {
			echo "<tr><td  colspan='4'> Data Kosong</td></tr>";
		} else {
			$no = 1;
			while ($row = mysqli_fetch_array($result))  {
				
				echo 	"<tr>
						
						<td>$row[1]</td>
						<td>$row[2]</td>
						<td>$row[3]</td>
						<td>$row[4]</td>
						</tr>";
				$no++;
			}
		}
		echo "</table>";
	
mysqli_close($connect);	// tutup koneksi		
?>



</div>
</div>
</div>
<div id="bottomPagination" >Copy Right Labkom STKIP Surya 2013/2014
</div>
</body></html>
<?php
}
	else {
		header("location:../index.php");
	}

?>