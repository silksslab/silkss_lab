
<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="base.css" /><link rel="stylesheet" type="text/css" href="content.css" /><link rel="stylesheet" type="text/css" href="nav.css" /><title>Lihat Matakuliah | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>
<div id="siteNav">
<ul>
<li><a href="index.php" class="daddy main-node">BERANDA</a></li><li><a href="penggunaan_lab.php" class="daddy">Penggunaan Lab</a><ul class="other-section">
<li><a href="jadwal.php" class="daddy">Jadwal</a><ul class="other-section">
<li><a href="buat_jadwal.php" class="no-ch">Buat Jadwal</a></li>
<li><a href="lihat_jadwal.php" class="no-ch">Lihat Jadwal</a></li>
<li><a href="ubah_jadwal.php" class="no-ch">Ubah Jadwal</a></li>
<li><a href="hapus_jadwal.php" class="no-ch">Hapus Jadwal</a></li>
</ul>
</li>
<li><a href="ruangan.php" class="daddy">Ruangan</a><ul class="other-section">
<li><a href="tambah_ruangan.php" class="no-ch">Tambah Ruangan</a></li>
<li><a href="lihat_ruangan.php" class="no-ch">Lihat Ruangan</a></li>
<li><a href="ubah_ruangan.php" class="no-ch">Ubah Ruangan</a></li>
<li><a href="hapus_ruangan.php" class="no-ch">Hapus Ruangan</a></li>
</ul>
</li>
<li><a href="komputer.php" class="daddy">Komputer</a><ul class="other-section">
<li><a href="tambah_komputer.php" class="no-ch">Tambah Komputer</a></li>
<li><a href="lihat_komputer.php" class="no-ch">Lihat Komputer</a></li>
<li><a href="ubah_komputer.php" class="no-ch">Ubah Komputer</a></li>
<li><a href="hapus_komputer.php" class="no-ch">Hapus Komputer</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="kinerja.php" class="daddy">Kinerja</a><ul class="other-section">
<li><a href="kinerja_mahasiswa.php" class="no-ch">Kinerja Mahasiswa</a></li>
<li><a href="kinerja_tutor.php" class="no-ch">Kinerja Tutor</a></li>
<li><a href="kinerja_dosen.php" class="no-ch">Kinerja Dosen</a></li>
<li><a href="kinerja_lab.php" class="no-ch">Kinerja Lab</a></li>
</ul>
</li>
<li class="current-page-parent"><a href="kebutuhan.php" class="current-page-parent daddy">Kebutuhan</a><ul>
<li><a href="mahasiswa.php" class="daddy">Mahasiswa</a><ul class="other-section">
<li><a href="tambah_mahasiswa.php" class="no-ch">Tambah Mahasiswa</a></li>
<li><a href="lihat_mahasiswa.php" class="no-ch">Lihat Mahasiswa</a></li>
<li><a href="ubah_mahasiswa.php" class="no-ch">Ubah Mahasiswa</a></li>
<li><a href="hapus_mahasiswa.php" class="no-ch">Hapus Mahasiswa</a></li>
<li><a href="formulir_pemesanan.php" class="no-ch">Formulir Pemesanan</a></li>
<li><a href="lihat_pemesanan.php" class="no-ch">Lihat Pemesanan</a></li>
<li><a href="mengambil_matakuliah.php" class="no-ch">Mengambil Matakuliah</a></li>
</ul>
</li>
<li><a href="dosen.php" class="daddy">Dosen</a><ul class="other-section">
<li><a href="tambah_dosen.php" class="no-ch">Tambah Dosen</a></li>
<li><a href="lihat_dosen.php" class="no-ch">Lihat Dosen</a></li>
<li><a href="ubah_dosen.php" class="no-ch">Ubah Dosen</a></li>
<li><a href="hapus_dosen.php" class="no-ch">Hapus Dosen</a></li>
<li><a href="formulir_peminjaman_ruangan.php" class="no-ch">Formulir Peminjaman Ruangan</a></li>
<li><a href="lihat_peminjaman1.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li><a href="tutor.php" class="daddy">Tutor</a><ul class="other-section">
<li><a href="tambah_tutor.php" class="no-ch">Tambah Tutor</a></li>
<li><a href="lihat_tutor.php" class="no-ch">Lihat Tutor</a></li>
<li><a href="ubah_tutor.php" class="no-ch">Ubah Tutor</a></li>
<li><a href="hapus_tutor.php" class="no-ch">Hapus Tutor</a></li>
<li><a href="formulir_peminjaman.php" class="no-ch">Formulir Peminjaman</a></li>
<li><a href="lihat_peminjaman2.php" class="no-ch">Lihat Peminjaman</a></li>
</ul>
</li>
<li class="current-page-parent"><a href="matakuliah.php" class="current-page-parent daddy">Matakuliah</a><ul>
<li><a href="tambah__matakuliah.php" class="no-ch">Tambah  Matakuliah</a></li>
<li id="active"><a href="lihat_matakuliah.php" class="active no-ch">Lihat Matakuliah</a></li>
<li><a href="ubahmatakuliah.php" class="no-ch">UbahMatakuliah</a></li>
<li><a href="hapus_matakuliah.php" class="no-ch">Hapus Matakuliah</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="info_lab.php" class="daddy">Info lab</a><ul class="other-section">
<li><a href="buat_berita.php" class="no-ch">Buat Berita</a></li>
<li><a href="lihat_berita.php" class="no-ch">Lihat Berita</a></li>
</ul>
</li>
<li><a href="kontak_kami.php" class="daddy">Kontak Kami</a><ul class="other-section">
<li><a href="formulir_kontak.php" class="no-ch">Formulir Kontak</a></li>
<li><a href="lihat_kotak_masuk.php" class="no-ch">Lihat Kotak Masuk</a></li>
</ul>
</div>
<div id='topPagination'><div class="pagination noprt"><a href="tambah__matakuliah.php" class="prev"><span>&laquo; </span>Previous</a> | <a href="ubahmatakuliah.php" class="next"> Next<span> &raquo;</span></a></div>
</div><div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Lihat Matakuliah</h1>



<?php 

//echo $_SESSION['username'];
// mengambil data yang telah disimpan pada form
//session
// die($_POST['kdruang']);
//memberikan variabel
$kdmk = $_POST["kdmk"];
$namamk= $_POST["namamk"];
$harimk	= $_POST["harimk"];
$jammk = $_POST["jammk"];


//Koneksi Ke Database
$connect=mysqli_connect("localhost","root","","silkss_db");
// Cek Koneksi ke database
	if (mysqli_connect_errno())
		{
		echo "koneksi ke databse error: " . mysqli_connect_error();//jika koneksi error, tampilkan errornya apa?
		}
	//Cek apakah data Sudah ada atau belum
	$sql = "SELECT Kode_Matakuliah FROM `matakuliah` WHERE Kode_Matakuliah=".$kdmk;
	$result=mysqli_query($connect,$sql);
	if ($result==null)//cek 
		  {
		  die('</br>Error: ' . mysqli_error($connect));//jika pencarian data error, tampilkan errornya apa ?
		  }
	echo " </br> Pencarian berhasil"; //jika pencarian data sukses/berhasil
	$data 	= mysqli_num_rows($result);
	if ($data != 0) {
			echo "<tr><td><br><div style=background-color:orange;color:black;font-size:15px;text-align:center;>Data  Sudah Ada</div>   </td></tr>";//jika data sudah ada tampilkan data sudah ada jika belum ada maka lakukan insert di bawah
	} else {
			$sql="INSERT INTO matakuliah (Id_Matakuliah,Nama_Matakuliah,Kode_Matakuliah,Hari,Jam)
		VALUES
		   (default,
			'$namamk',
			'$kdmk',
			'$harimk',
			'$jammk')";
		// perintah untuk menjalankan query
		$result=mysqli_query($connect,$sql);
		if ($result==null)//cek 
		  {
		  die('</br>Error: ' . mysqli_error($connect));//jika penambahan data error, tampilkan errornya apa ?
		  }
		echo " </br> 1 baris bertambah pada tabel 'matakuliah'"; //jika penambahan data sukses/berhasil

			
				// ------- TAMPILKAN DATANYA ---------------===
			
				echo "<table border='1' class='data'>
				<tr>
				<th width='10%'>Id Matakuliah</th>
				<th width='20%'>Nama Matakuliah</th>
				<th width='10%'>Kode Matakuliah</th>
				<th width='20%'>Hari</th>
				<th width='10%'>Jam</th> 
				</tr>";
				//tampilkan data dari tabel matakuliah
			$sql = "SELECT * FROM `matakuliah` ORDER BY `Id_Matakuliah` ASC  ";
		$result=mysqli_query($connect,$sql);
		if ($result==null)//cek 
		  {
		  die('</br>Error: ' . mysqli_error($connect));//jika pencarian data error, tampilkan errornya apa ?
		  }
		echo " </br> Pencarian berhasil"; //jika pencarian data sukses/berhasil
		$data 	= mysqli_num_rows($result);
		if ($data == 0) {
			echo "<tr><td  colspan='4'> Data Kosong</td></tr>";
		} else {
					$no = 1;
					// die("a".$data);
					while ($row = mysqli_fetch_array($result)) {
						
						echo 	"<tr>
								<td>$no</td>
								<td>$row[Nama_Matakuliah]</td>
								<td>$row[Kode_Matakuliah]</td>
								<td>$row[Hari]</td>
								<td>$row[Jam]</td>
								</tr>";
						$no++;
					}
				}
				echo "</table>";
  }
 
mysqli_close($connect);	// tutup koneksi		
?>




</div>
<div id='bottomPagination'><div class="pagination noprt"><a href="tambah__matakuliah.php" class="prev"><span>&laquo; </span>Previous</a> | <a href="ubahmatakuliah.php" class="next"> Next<span> &raquo;</span></a></div>
</div></div>
</div>
</body></html>
