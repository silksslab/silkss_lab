
<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!-- Created using eXe: http://exelearning.org -->
<head>
<link rel="stylesheet" type="text/css" href="base.css" /><link rel="stylesheet" type="text/css" href="content.css" /><link rel="stylesheet" type="text/css" href="nav.css" /><title>Tambah Komputer | SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA </title>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<script type="text/javascript" src="common.js"></script>
</head>
<body>
<div id="content">
<div id="header"  style="background-image: url(stkip_suryalogo.jpg); background-repeat: no-repeat;">
SISTEM INFORMASI LABORATORIUM KOMPUTER STKIP SURYA</div>
<div id="siteNav">
	<ul>
	<li id="active"><a href="index.php" >BERANDA</a></li>
	<li><a href="penggunaan_lab.php" class="daddy">Penggunaan Lab</a>

	<ul class="other-section">
				<li><a href="lihat_jadwal.php" >Jadwal</a>
							
				</li>
				
				
				
				
	</ul>
	</li>
				
	<li><a href="kebutuhan.php" class="daddy">Kebutuhan</a>

	</li>

	<li><a href="info_lab.php" class="daddy">Info lab</a>

	</li>

	<li><a href="formulir_kontak.php" class="daddy">Kontak Kami</a>

	</li>

	</div>
	<div id='topPagination'><div class="pagination noprt"><a href="login/login.php" class="next"> Masuk<span> </span></a></div>
	</div>
	<div id="main">

<div id="main">
<div id="nodeDecoration"><h1 id="nodeTitle">Tulis Komentar</h1>

<a href="lihat_bukutamu.php">Lihat Semua Komentar</a>
<hr>
</hr>
<!-- START FORM BUKU TAMU -->
				
				<!-- Start Formoid form-->
			<link rel="stylesheet" href="tambah_komentar_files/formoid1/formoid-default-red.css" type="text/css" />
			<script type="text/javascript" src="tambah_komentar_files/formoid1/jquery.min.js">
			</script>
			<form action="simpan_buku_tamu.php" class="formoid-default-red" style="background-color:#FFFFFF;font-size:14px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#000000;max-width:480px;min-width:150px" method="post"><div class="title"><h2></h2></div>
				<div class="element-input" ><label class="title">Nama<span class="required">*</span></label>
					<input class="medium" type="text" name="nama" required="Silahkan isi nama anda"/>
				</div>
				
				<div class="element-email" ><label class="title">Email<span class="required">*</span></label>
					<input class="medium" type="email" name="email" value="" required="required"/>
				</div>
				
				<div class="element-textarea" ><label class="title">Komentar<span class="required">*</span></label>
					<textarea class="large" name="komentar" cols="20" rows="5" required="required">
					</textarea>
				</div>

				<div class="submit"><input type="submit" value="Kirim"/>
				</div>
				<div class="submit"><input name="Submit2" type="reset"  value="  Batal  "><br/></td>
					
				</div>
			</form>
			<script type="text/javascript" src="tambah_komentar_files/formoid1/formoid-default-red.js">
			</script>
			
<!-- Stop Formoid form-->

				<!-- End FORM BUKU TAMU -->


</div>
</div>
</div>

</body>
<div id="bottomPagination">Copy Right Labkom STKIP Surya 2013/2014
</div>
</html>
